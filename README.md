# Master Thesis
## Learning the Automatic Preparation of STM (Scanning Tunneling Microscope) Tips

- uses deep reinforcement learning to learn how to prepare a good STM tip
- based on DQN
- STM: 
  - microscope
  - records nanoscale images
  - needs tiny tip of high quality
  - good tip -> good images
- classConvNet: CNN, decides if an image is good (used to compute RL reward)
- convNet: CNN, decides if the new image is better than the previous one (used to compute RL reward)

### simulation
simulation to test the RL framework


### STM
real STM necessary
