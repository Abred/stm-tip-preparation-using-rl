#!/usr/bin/python

import os
import sys
import random
from PIL import Image, ImageFont, ImageDraw
import scipy.ndimage.filters as spf
import scipy.signal as sps
import numpy as np


szImg = 500
bg = 0
peakMax = 3
sigmaMax = 35
radiusMax = int(np.sqrt(121))
peakLimit = 1
sigmaLimit = 4
radiusLimit = 2
ks = 500


# tmp = np.zeros((1000, 1))
# for i in range(1000):
#     tmp[i][0] = int(min(max(np.random.exponential(1.5), 1.0),
#                         peakMax))
# print(np.mean(tmp), tmp.max(), tmp.min(), np.std(tmp), tmp)
# exit()
# dr.rectangle(((0, 0),(szImg-1, szImg-1)), outline="red")

if len(sys.argv) <= 2:
    print("provide args")
    exit(-1)
if not os.path.exists(sys.argv[1]):
    os.makedirs(sys.argv[1])

if len(sys.argv) <= 3:
    prefix = ""
else:
    prefix = sys.argv[3]


stds = []
for i in range(int(sys.argv[2])):
    im = Image.new('F', (szImg, szImg), bg)
    dr = ImageDraw.Draw(im)

    rect = []
    cnt = 3
    while cnt > 0:
        szRect = 100 + random.randint(-25, 25)
        xRect = random.randint(0, szImg-szRect)
        yRect = random.randint(0, szImg-szRect)
        rep = False
        for j in rect:
            if xRect < (j[0]+j[2]) and (xRect+szRect) > j[0] and \
               yRect < (j[1]+j[2]) and (yRect+szRect) > j[1]:
                rep = True
                break

        if rep:
            continue
        rect.append((xRect, yRect, szRect))
        cnt -= 1
        dr.rectangle(((xRect, yRect),
                      (xRect+szRect, yRect+szRect)),
                     fill="white")
        # print(szRect, xRect, yRect)

    # circ = []
    cnt = 3
    while cnt > 0:
        szRect = 100 + random.randint(-25, 25)
        xRect = random.randint(0, szImg-szRect)
        yRect = random.randint(0, szImg-szRect)
        rep = False
        for j in rect:
            if xRect < (j[0]+j[2]) and (xRect+szRect) > j[0] and \
               yRect < (j[1]+j[2]) and (yRect+szRect) > j[1]:
                rep = True
                break

        if rep:
            continue

        rect.append((xRect, yRect, szRect))
        cnt -= 1
        dr.ellipse(((xRect, yRect),
                    (xRect+szRect, yRect+szRect)),
                   fill="white")
        # print(szRect, xRect, yRect)

    if prefix == "bad":
        while True:
            peaks = int(min(max(np.random.exponential(1.5), 1.0),
                            peakMax))
            sigma = int(min(max(np.random.exponential(7.5), 1.0),
                            sigmaMax))
            # sigma = random.randint(1, self.params['sigmaMax'])
            radius = int(min(max(np.random.exponential(4.0), 0.0),
                             radiusMax-1))
            # peaks = int(min(max(np.random.exponential(1.5), 1.0),
            #                 peakMax))
            # peaks = random.randint(1, peakMax)
            # sigma = random.randint(1, sigmaMax)
            # sigma = random.randint(1, 5)
            # radius = random.randint(0, radiusMax-1)
            # radius = random.randint(0, 5)
            # radius = int(max(0, pow(random.randint(0, radiusMax), 2)))
            if (peaks > peakLimit and radius >= radiusLimit) or \
               sigma > sigmaLimit:
                break
    elif prefix == "good":
        while True:
            peaks = int(min(max(np.random.exponential(1.5), 1.0),
                            peakMax))
            # peaks = random.randint(1, peakMax)
            sigma = random.randint(1, sigmaLimit)
            radius = random.randint(0, radiusMax-1)
            # radius = int(max(0, pow(random.randint(0, radiusMax), 2)))
            if (peaks <= peakLimit or radius < radiusLimit) and \
               sigma <= sigmaLimit:
                break
        # peaks = random.randint(1, peakLimit)
        # sigma = random.randint(1, sigmaLimit)
        # radius = random.randint(0, radiusLimit-1)

    # peaks = random.randint(1, peakMax)
    # sigma = random.randint(1, sigmaMax)
    # radius = int(max(0, pow(random.randint(0, radiusMax), 2)))

    rep = True
    kernel = np.zeros((ks, ks))
    if len(sys.argv) > 4:
        peaks = int(sys.argv[4])
        sigma = float(sys.argv[5])
        radius = float(sys.argv[6])
        # radius = radius*radius

    while rep:
        rep = False
        angs = []
        for j in range(peaks):
            angs.append(np.random.rand() * 2 * np.pi)
        for j in range(peaks):
            for k in range(j+1, peaks):
                if (np.pi - abs(abs(angs[j] - angs[k]) - np.pi)) < 2 * np.pi / 5.0:
                    rep = True
        if rep:
            continue
        for j in range(len(angs)):
            print(angs[j]/np.pi*180.0)

        # print(np.sum(kernel))
        for j in range(len(angs)):
            # print(ang*180/np.pi)
            y = int(np.sin(angs[j]) * pow(radius, 2) + ks/2)
            x = int(np.cos(angs[j]) * pow(radius, 2) + ks/2)
            # print(x,y, 1.0/len(angs))
            # if len(angs) == 1:
            #     kernel[y, x] = 1.0
            # el
            if j == 0:
                kernel[y, x] = 1.0
            else:
                if kernel[y, x ] != 0.0:
                    print("peak same loc")
                kernel[y, x] += 0.3334
            print(x, y)
        # print(np.sum(kernel))
        # kernel = kernel/np.sum(kernel)
        kernel = spf.gaussian_filter(kernel, sigma)
        print(kernel[y, x])
        xm = 0.0
        ym = 0.0
        sm = 0.0
        kernel2 = kernel
        # kernel = np.multiply(kernel,kernel)
        
        # kernel2 = np.log(kernel+1)
        # gy, gx = np.gradient(kernel)
        # kernel2 = np.sqrt(gx**2 + gy**2)
        # print(kernel2.shape)

        for y in range(kernel.shape[0]):
            for x in range(kernel.shape[1]):
                ym += float(y+1) * kernel[y, x]
                xm += float(x+1) * kernel[y, x]
                sm += kernel[y, x]
        ym /= sm
        ym -= 1
        xm /= sm
        xm -= 1
        # ym = kernel.shape[0]/2
        # xm = kernel.shape[0]/2

        print(kernel[int(ym), int(xm)])
        dist = 0.0
        # sm = np.sum(kernel)
        for y in range(kernel.shape[0]):
            for x in range(kernel.shape[1]):
                dist += np.sqrt((pow(y - ym, 2) + pow(x - xm, 2))) * kernel2[y, x]/sm
                # dist += np.sqrt((pow(y - ym, 2) + pow(x - xm, 2)) * kernel2[y, x]/sm)
                # dist += (pow(y - ym, 2) + pow(x - xm, 2)) * np.sqrt(kernel2[y, x])/sm
        nz = np.count_nonzero(kernel)
        # dist = np.sqrt(dist)
        dist2 = peaks*(peaks)*(peaks-1)*(radius)*3 + (sigma-1)*1
        print(peaks, sigma, np.sqrt(radius), xm, ym, nz, dist, dist2)

        # print(peaks, sigma, radius, np.std(kernel))
        # stdA = np.sqrt(np.mean(np.abs(kernel - np.mean(kernel))))
        # nz = np.count_nonzero(kernel)
        # sk = np.sum(kernel)
        # mk = sk / nz
        # # print(nz, sk, mk)
        # stdB = np.sqrt(np.sum(np.abs(kernel - mk))/nz)
        # print(peaks, sigma, radius, stdA, stdB)
        # # std = sqrt(mean(abs(x - x.mean())**2)).


        stds.append((peaks, sigma, radius, dist, dist2))


        # print(np.sum(kernel))
    filteredImage = sps.fftconvolve(im, kernel, mode='same')
    # print(filteredImage.max())
    filteredImage = filteredImage/filteredImage.max()*255.0
    # print(peaks, sigma, radius)

    # print(kernel.max(), (kernel*255.0).max())
    # kernel = kernel/kernel.max()*255.0
    # ima = Image.fromarray(kernel)
    # # im.show()
    # fname = prefix + "kernel_" + str(peaks) + "_" + str(sigma) + "_" + str(radius) + "_" + str(i)
    # ima.convert('RGB').save(os.path.join(sys.argv[1], fname + ".png"))


    ima = Image.fromarray(filteredImage)
    # im.show()
    fname = prefix + "image_" + str(peaks) + "_" + str(sigma) + "_" + str(radius) + "_" + str(i)
    ima.convert('RGB').save(os.path.join(sys.argv[1], fname + ".png"))
    # im.convert('RGB').save(os.path.join(sys.argv[1], fname + "Orig.png"))
    # ima = Image.fromarray(img1/255.0)
    np.save(os.path.join(sys.argv[1], fname), ima)

sstds = sorted(stds, key=lambda tp: tp[3])
for s1 in range(len(sstds)):
    s1c = sstds[s1]
    for s2 in range(s1+1, len(sstds)):
        s2c = sstds[s2]
        if s1c[0] <= s2c[0] and s1c[1] <= s2c[1] and s1c[2] <= s2c[2]:
            if s1c[3] >= s2c[3]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        elif s1c[0] >= s2c[0] and s1c[1] >= s2c[1] and s1c[2] >= s2c[2]:
            if s1c[3] <= s2c[3]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        else:
            print("order? {} <-> {}".format(s1c, s2c))

# generate whole space and compare somehow!?
print("")
print("")
sstds = sorted(stds, key=lambda tp: tp[3])
for s1 in range(len(sstds)):
    s1c = sstds[s1]
    for s2 in range(s1+1, len(sstds)):
        s2c = sstds[s2]
        if s1c[0] <= s2c[0] and s1c[1] <= s2c[1] and s1c[2] <= s2c[2]:
            if s1c[3] >= s2c[3]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        elif s1c[0] >= s2c[0] and s1c[1] >= s2c[1] and s1c[2] >= s2c[2]:
            if s1c[3] <= s2c[3]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        else:
            p1 = s1c[0]
            s1 = s1c[1]
            r1 = s1c[2]
            d1a = s1c[3]
            d1b = s1c[4]
            if p1 == 1:
                r1b = 0
            else:
                r1b = r1
            if r1 == 0:
                p1b = 1
            else:
                p1b = p1

            p2 = s2c[0]
            s2 = s2c[1]
            r2 = s2c[2]
            d2a = s2c[3]
            d2b = s2c[4]
            if p2 == 1:
                r2b = 0
            else:
                r2b = r2
            if r2 == 0:
                p2b = 1
            else:
                p2b = p2
            print("order? ({}, {}, {}, {}, {}) <-> ({}, {}, {}, {}, {})  [{}, {}; {}, {}]".format(p1b, s1, r1b, d1a, d1b, p2b, s2, r2b, d2a, d2b, p1, r1, p2, r2))


print("")
print("")
sstds = sorted(stds, key=lambda tp: tp[4])
for s1 in range(len(sstds)):
    s1c = sstds[s1]
    for s2 in range(s1+1, len(sstds)):
        s2c = sstds[s2]
        if s1c[0] <= s2c[0] and s1c[1] <= s2c[1] and s1c[2] <= s2c[2]:
            if s1c[4] >= s2c[4]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        elif s1c[0] >= s2c[0] and s1c[1] >= s2c[1] and s1c[2] >= s2c[2]:
            if s1c[4] <= s2c[4]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        else:
            print("order? {} <-> {}".format(s1c, s2c))


print("")
print("")
sstds = sorted(stds, key=lambda tp: tp[4])
for s1 in range(len(sstds)):
    s1c = sstds[s1]
    for s2 in range(s1+1, len(sstds)):
        s2c = sstds[s2]
        if s1c[0] <= s2c[0] and s1c[1] <= s2c[1] and s1c[2] <= s2c[2]:
            if s1c[4] >= s2c[4]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        elif s1c[0] >= s2c[0] and s1c[1] >= s2c[1] and s1c[2] >= s2c[2]:
            if s1c[4] <= s2c[4]:
                print("wrong order! {} <-> {}".format(s1c, s2c))
            # else:
                # print("correct order! {} <-> {}".format(s1c, s2c))
        else:
            p1 = s1c[0]
            s1 = s1c[1]
            r1 = s1c[2]
            d1a = s1c[3]
            d1b = s1c[4]
            if p1 == 1:
                r1b = 0
            else:
                r1b = r1
            if r1 == 0:
                p1b = 1
            else:
                p1b = p1

            p2 = s2c[0]
            s2 = s2c[1]
            r2 = s2c[2]
            d2a = s2c[3]
            d2b = s2c[4]
            if p2 == 1:
                r2b = 0
            else:
                r2b = r2
            if r2 == 0:
                p2b = 1
            else:
                p2b = p2
            print("order? ({}, {}, {}, {}, {}) <-> ({}, {}, {}, {}, {})  [{}, {}; {}, {}]".format(p1b, s1, r1b, d1a, d1b, p2b, s2, r2b, d2a, d2b, p1, r1, p2, r2))
# sstds = sorted(stds, key=lambda tp: tp[4])
# for s in sstds:
#     print(s)

# sstds = sorted(stds, key=lambda tp: tp[5])
# for s in sstds:
#     print(s)
