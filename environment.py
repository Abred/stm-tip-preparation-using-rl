import abc
import numpy as np


class Environment(object):
    """Abstract class for RL-Environment

    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, params, features=None):
        super(Environment, self).__init__()
        self.params = params
        if features is not None:
            self.currFeatures = features
        else:
            self.currFeatures = []
        self.nextFeatures = []
        self.transitions = None

    def getReward(self):
        if self.isTerminal():
            return (True, self.params['rewardFinal'])

        for f in range(len(self.currFeatures)):
            if self.nextFeatures[f] < self.currFeatures[f]:
                return (False, self.params['rewardPos'])
        return (False, self.params['rewardNeg'])

    @abc.abstractmethod
    def isTerminal(self):
        raise NotImplementedError("Please implement this method")

    @abc.abstractmethod
    def getProbability(self, state, action, nextState):
        raise NotImplementedError("Please implement this method")

    @abc.abstractmethod
    def sampleStep(self, action):
        raise NotImplementedError("Please implement this method")

    @abc.abstractmethod
    def reset(self, features):
        raise NotImplementedError("Please implement this method")

    def takeStep(self):
        self.currFeatures = list(self.nextFeatures)
