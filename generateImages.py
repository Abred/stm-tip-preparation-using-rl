#!/usr/bin/python

import os
import sys
import random
from PIL import Image, ImageFont, ImageDraw
import scipy.ndimage.filters as spf
import numpy as np


szImg = 500
bg = (0, 0, 255)
bg = 0

# dr.rectangle(((0, 0),(szImg-1, szImg-1)), outline="red")

if len(sys.argv) <= 2:
    print("provide args")
    exit(-1)
if not os.path.exists(sys.argv[1]):
    os.makedirs(sys.argv[1])

for i in range(int(sys.argv[2])):
    im = Image.new('F', (szImg, szImg), bg)
    dr = ImageDraw.Draw(im)

    for j in range(3):
        szRect = 100 + random.randint(-25, 25)
        xRect = random.randint(0, szImg-szRect)
        yRect = random.randint(0, szImg-szRect)
        dr.rectangle(((xRect, yRect),
                      (xRect+szRect, yRect+szRect)),
                     fill="white")

    for j in range(3):
        szRect = 100 + random.randint(-25, 25)
        xRect = random.randint(0, szImg-szRect)
        yRect = random.randint(0, szImg-szRect)
        dr.ellipse(((xRect, yRect),
                    (xRect+szRect, yRect+szRect)),
                   fill="white")

    img1 = spf.gaussian_filter(im, sigma=2)
    # img2 = spf.gaussian_filter(im, sigma=0.1)

    # im = scp.gaussian_laplace(im, sigma=2) #, order=(0,1))

    # ima = Image.fromarray(im + img1-img2)
    ima = Image.fromarray(img1)
    # im.show()
    ima.convert('RGB').save(os.path.join(sys.argv[1], "image" + str(i) + ".png"))
    np.save(os.path.join(sys.argv[1], "image" + str(i) + ".png", ima))
