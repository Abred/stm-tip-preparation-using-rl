from keras.applications.vgg16 import preprocess_input
import json
import numpy as np
import os
import shutil
import sys
import time

import preload
import defineVGG
import parseNNArgs
import loadData
import classNN
import runner

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim


class ClassConvNet(classNN.ClassNN):
    Hconv1 = 32
    Hconv2 = 48
    Hconv3 = 48
    HFC = [2048, 2048]
    HFCVGG = [2048, 2048]

    learning_rate = 0.01
    momentum = 0.9

    def __init__(self, sess, out_dir, glStep,
                 trainImages,
                 trainLabels,
                 testImages,
                 testLabels,
                 testFiles,
                 params):
        super(ClassConvNet, self).__init__(sess, glStep, params)
        self.top = params['top']
        self.bnDecay = params['batchnorm-decay']

        if self.useVGG:
            self.state_dim = 224
        elif params['state_dim'] is None:
            self.state_dim = params['cropSz']
        else:
            self.state_dim = params['state_dim']

        if self.useVGG:
            varTmp = tf.get_variable("tmp", shape=[1,1])
            # with tf.variable_scope("VGG") as scope:
                # model = VGG16(weights='imagenet', include_top=False)
            self.vggsaver = tf.train.import_meta_graph(
                    '/home/s7550245/convNet/vgg-model.meta',
                    import_scope="VGG",
                    clear_devices=True)

            _VARSTORE_KEY = ("__variable_store",)
            varstore = ops.get_collection(_VARSTORE_KEY)[0]
            variables = tf.get_collection(
                ops.GraphKeys.GLOBAL_VARIABLES,
                scope="VGG")

            for v in variables:
                    varstore._vars[v.name.split(":")[0]] = v

            with tf.variable_scope("VGG") as scope:
                scope.reuse_variables()
                self.nnTrain = self.defineNNVGG(trainImages)
                self.nnTest = self.defineNNVGG(testImages)
                self.nnTrain = tf.stop_gradient(self.nnTrain)
                self.nnTest = tf.stop_gradient(self.nnTest)


            if self.top is not None:
                with tf.variable_scope("VGG") as scope:
                    self.nnTrain = self.defineNNVGGTop(self.nnTrain)
                    self.nn_params = tf.trainable_variables()
                    scope.reuse_variables()
                    self.nnTest = self.defineNNVGGTop(self.nnTest)
            self.lTrain = trainLabels
            self.defineLoss(trainLabels)
            self.defineTraining()
            self.lTest = testLabels
        else:
            if params['preload']:
                with tf.variable_scope("classConvNet") as scope:
                    self.nnTrain = self.defineNN(trainImages)
                    self.nn_params = tf.trainable_variables()
                    scope.reuse_variables()
                    self.nnTest = self.defineNN(testImages)

                self.lTrain = trainLabels
                self.defineLoss(trainLabels)
                self.defineTraining()
                self.lTest = testLabels
            else:
                self.images = tf.placeholder(
                    tf.float32,
                    shape=(None,
                           self.state_dim,
                           self.state_dim,
                           1),
                    name='input')
                self.nn = self.defineNN(self.images)
                self.nn_params = tf.trainable_variables()
                self.nnTrain = self.nn
                self.nnTest = self.nn

                self.labels = tf.placeholder(tf.float32, [None, 1],
                                             name='labels')
                self.lTrain = self.labels
                self.defineLoss(self.labels)
                self.defineTraining()
                self.lTest = self.labels

        self.fTest = testFiles

        print(set(self.nn_params).symmetric_difference(set(tf.trainable_variables())))
        self.summary_op = tf.summary.merge(self.summaries)
        self.testPredHistoSum = tf.summary.histogram('testPredHisto',
                                                     self.nnTest)

        vd = {}
        _VARSTORE_KEY = ("__variable_store",)
        varstore = ops.get_collection(_VARSTORE_KEY)[0]
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="classConvNet")
        for v in variables:
            print(v.name)
            vn = v.name.split(":")[0]
            vn = vn.replace("classConvNet", "convNet")
            print(vn)
            if "fc" not in vn:
                vd[vn] = v
        self.saver = tf.train.Saver(vd)

    def defineNNVGG(self, images):
        return defineVGG.defineNNVGG(images, self.state_dim, self.top)

    def defineNNVGGTop(self, bottom):
        with tf.variable_scope('top') as scope:
            print(bottom)
            if self.top == 1:
                numPool = 0
                self.HFCVGG = [256, 256]
            if self.top <= 2:
                numPool = 1
                H = 64
                self.HFCVGG = [256, 256]
            elif self.top <= 4:
                numPool = 2
                H = 128
                self.HFCVGG = [512, 512]
            elif self.top <= 7:
                numPool = 3
                H = 256
                self.HFCVGG = [1024, 1024]
            elif self.top <= 10:
                numPool = 4
                H = 512
                self.HFCVGG = [2048, 2048]
            elif self.top <= 13:
                numPool = 5
                H = 512
                self.HFCVGG = [2048, 2048]
            # remSz = int(self.state_dim / 2**numPool)
            net = slim.flatten(bottom)
            # net = tf.reshape(bottom, [-1, remSz*remSz*H],
            #                  name='flatten')

            with slim.arg_scope(
                [slim.fully_connected],
                activation_fn=tf.nn.relu,
                weights_initializer=
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0/255.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True)):
                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'scale': True}):
                    for i in range(len(self.HFCVGG)):
                        net = slim.fully_connected(net, self.HFCVGG[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.keep_prob,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)
            self.summaries += [tf.summary.histogram('output', net)]
            return net

    def defineNN(self, images):
        with tf.variable_scope('inf') as scope:
            net = tf.reshape(images, [-1,
                                      self.state_dim,
                                      self.state_dim,
                                      1], name='deflatteni1')

            net = (net - 127.0) / 255.0
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True)):
                with slim.arg_scope([slim.conv2d], stride=1, padding='SAME'):
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv1,
                                      [3, 3], scope='conv1')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool1')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv2,
                                      [3, 3], scope='conv2')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool2')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv3,
                                      [3, 3], scope='conv3')
                    print(net)

                # remSz = int(self.state_dim / 2**2)
                net = slim.flatten(net)
                # net = tf.reshape(net, [-1, remSz*remSz*self.Hconv3],
                #                  name='flatten')

                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'decay': self.bnDecay,
                                        'scale': True}):
                    for i in range(len(self.HFC)):
                        net = slim.fully_connected(net, self.HFC[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.keep_prob,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)

                self.summaries += [tf.summary.histogram('output', net)]

        return net


if __name__ == "__main__":
    params = parseNNArgs.parse(sys.argv[1:])

    if os.environ['SLURM_JOB_NAME'] == 'zsh':
        params['version'] = 'tmp'
    print(params, params['version'])
    timestamp = str(int(time.time()))
    jobid = os.environ['SLURM_JOBID']
    out_dir = os.path.abspath(os.path.join(
        "/scratch/s7550245/simulateSTM/classConvNet/runs",
        params['version'], jobid + "_" + timestamp))

    print("Number of epochs: ", params['numEpochs'])
    out_dir += "_" + str(params['numEpochs'])

    print("miniBatchSize: ", params['miniBatchSize'])
    out_dir += "_" + str(params['miniBatchSize'])

    print("use subimage: ", params['subImages'])
    if params['subImages']:
        out_dir += "_" + "subImg"
    else:
        out_dir += "_" + "noSubImg"

    print("usevgg", params['useVGG'])
    if params['useVGG']:
        out_dir += "_" + "VGG" + str(params['top'])
        if params['stopGrad']:
            out_dir += "-stopGrad" + str(params['stopGrad'])
    else:
        out_dir += "_" + "noVGG"

    print("dropout", params['dropout'])
    if params['dropout']:
        out_dir += "_" + "dropout" + str(params['dropout'])
    else:
        out_dir += "_" + "noDropout"

    print("sameL", params['sameL'])
    if params['sameL']:
        out_dir += "_" + "sameL"
    else:
        out_dir += "_" + "nosameL"

    if params['distortBrightnessRelative'] or params['distortContrast']:
        params['distorted'] = True
    print("distorted", params['distorted'])
    if params['distorted']:
        delta = params['distortBrightnessRelative']
        factor = params['distortContrast']
        out_dir += "_" + "distorted" + "_Br-" + str(delta) + "_Cntr-" + str(factor)
    else:
        out_dir += "_" + "noDistorted"

    print("batchnorm", params['batchnorm'])
    if params['batchnorm']:
        out_dir += "_" + "batchnorm-" + str(params['batchnorm-decay'])
    else:
        out_dir += "_" + "noBatchnorm"

    print("cropSz", params['cropSz'])
    if params['cropSz']:
        out_dir += "_" + "cropSz" + str(params['cropSz'])

    print("data normalised by: ", params['suffix'])
    out_dir += "_suffix-" + params['suffix']

    print("use whitened images", params['white'])
    if params['white']:
        out_dir += "_" + "white"
    else:
        out_dir += "_" + "noWhite"

    print("weight decay", params['weight-decay'])
    out_dir += "_wd" + str(params['weight-decay'])

    print("learning rate", params['learning-rate'])
    out_dir += "_lr" + str(params['learning-rate'])

    print("momentum", params['momentum'])
    out_dir += "_mom" + str(params['momentum'])

    print("optimizer", params['optimizer'])
    out_dir += "_opt" + params['optimizer']

    print("reading data from: ", params['in_dir'])
    out_dir += params['in_dir'].split("/")[-1]

    if params['resume']:
        print("resuming... {}".format(params['resume']))
        out_dir = params['resume']

    print("Summaries will be written to: {}\n".format(out_dir))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.copy2(sys.argv[0], os.path.join(out_dir, sys.argv[0]))
    shutil.copy2("classConvNetEval.py",
                 os.path.join(out_dir, "classConvNetEval.py"))

    if not params['resume']:
        print("new start... {}".format(out_dir))
        config = json.dumps(params)
        with open(os.path.join(out_dir, "config"), 'w') as f:
            f.write(config)

    params['out_dir'] = out_dir
    if os.environ['SLURM_JOB_NAME'] != 'zsh':
        sys.stdout.flush()
        sys.stdout = open(os.path.join(out_dir, "log"), 'w')

    # with tf.device("/gpu:0"):
    global_step = tf.Variable(1, name='global_stepClass',
                              trainable=False)

    sess = tf.Session()
    # sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    if params['resume']:
        img_train, l_train, f_train, \
            img_test, l_test, f_test = loadData.restoreData(out_dir)
    else:
        img_train, l_train, f_train, \
            img_test, l_test, f_test = loadData.loadData(out_dir,
                                                         params,
                                                         rawImg=True,
                                                         fake=True)

    # imgs = np.concatenate((img_train, img_test))
    # labels = np.concatenate((l_train, l_test))
    # files = np.concatenate((f_train, f_test))

    print("using vgg: {}".format(params['useVGG']))
    channels = 1
    if params['useVGG']:
        channels = 3
        img_train = preprocess_input(np.repeat(img_train, 3, axis=3))
        img_test = preprocess_input(np.repeat(img_test, 3, axis=3))

    print(img_train.shape, l_train.shape)
    print(img_test.shape, l_test.shape)
    print(img_train.dtype, img_test.dtype)

    if params['preload']:
        with tf.variable_scope("preloadClassConvNet") as scope:
            pl = preload.PreloaderSingle(img_train, l_train,
                                         img_test, l_test, f_test)
            images_train, labels_train, \
            images_test, labels_test, files_test = \
                pl.setupPreload(params, rawImg=True)

        rn = ClassConvNet(sess, out_dir, global_step,
                          images_train, labels_train,
                          images_test, labels_test, files_test,
                          params)
        pl.initPreload(sess)
    else:
        rn = ClassConvNet(sess, out_dir, global_step,
                          None, None, None, None, None,
                          params)

    trainImgCnt = img_train.shape[0]
    testImgCnt = img_test.shape[0]

    op = runner.Runner(sess, global_step, params, conv=False)
    init_op = tf.group(tf.global_variables_initializer(),
                       tf.local_variables_initializer())
    sess.run(init_op)

    op.initSingle(out_dir, rn, trainImgCnt, testImgCnt)

    rn.setSess(sess, out_dir)
    if params['preload']:
        op.runSingleQueue()
    else:
        op.runSingleLoop(img_train, l_train, img_test, l_test, transf=True)
