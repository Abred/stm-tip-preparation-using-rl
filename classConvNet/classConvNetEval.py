from keras.applications.vgg16 import preprocess_input
import random
import json
import numpy as np
import os
import shutil
import sys
import time

import preload
import defineVGG
import parseNNArgs
import loadData
import classNN
import runner
import readFakeImgPairs
import transformImage

import scipy.ndimage

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim


class ClassConvNetEval:

    def __init__(self, sess, params, glStep=None):
        try:
            if params['classNN'] is not None:
                if os.path.isdir(params['classNN']):
                    in_dir = params['classNN']
                    model = None
                else:
                    in_dir = os.path.dirname(params['classNN'])
                    model = params['classNN']

            else:
                in_dir = params['in_dir']
                model = params['model']
            print("config file: {}".format(os.path.join(in_dir, "config")))
            with open(os.path.join(in_dir, "config"), 'r') as f:
                config = f.read()
                params = json.loads(config)
                params['in_dir'] = in_dir
                params['model'] = model
        except:
            print("config file not found")
        self.sess = sess
        self.useVGG = params['useVGG']
        self.keep_prob = tf.placeholder(tf.float32, shape=(),
                                        name="keep_prob_pl")
        self.isTraining = tf.placeholder(tf.bool, shape=(),
                                         name="isTraining_pl")

        if self.useVGG:
            self.state_dim = 224
        elif params['state_dim'] is None:
            self.state_dim = params['cropSz']
        else:
            self.state_dim = params['state_dim']

        if self.useVGG:
            scope="VGG"
            self.img_pl = tf.placeholder(
                tf.float32, shape=[None, 50, 50, 3], name='input')
            tmpImg = tf.image.resize_images(self.img_pl, (224, 224))
            # tmpImg = tf.image.grayscale_to_rgb(tmpImg)
            print(tmpImg)
            self.saver = tf.train.import_meta_graph(
                tf.train.latest_checkpoint(in_dir) + ".meta",
                import_scope=scope,
                clear_devices=True,
                input_map={'VGG_1/deflatten': tmpImg})
            self.nn = sess.graph.get_tensor_by_name(
                scope+"/VGG_2/top/out/BiasAdd:0")
        else:
            scope="classConvNet"
            self.img_pl = tf.placeholder(
                tf.float32, shape=[None, 50, 50, 1], name='input')
            self.saver = tf.train.import_meta_graph(
                tf.train.latest_checkpoint(in_dir) + ".meta",
                import_scope=scope,
                clear_devices=True,
                input_map={'classConvNet/inf/deflatteni1': self.img_pl})
            net = sess.graph.get_tensor_by_name(
                scope+"/classConvNet/inf/out/BiasAdd:0")
            self.nn = tf.Print(net, [net], "outputClass:", first_n=20)
        try:
            self.keep_prob = sess.graph.get_tensor_by_name(
                scope+"/keep_prob_pl:0")
        except:
            pass
        try:
            self.isTraining = sess.graph.get_tensor_by_name(
                scope+"/isTraining_pl:0")
        except:
            self.isTraining = sess.graph.get_tensor_by_name(
                scope+"/Placeholder_6:0")

    def runPrediction(self, inputs):
        return self.sess.run(self.nn, feed_dict={
            self.img_pl: inputs,
            self.isTraining: False,
            self.keep_prob: 1.0
        })


if __name__ == "__main__":
    params = parseNNArgs.parse(sys.argv[1:])

    if os.path.isfile(os.path.join(params['in_dir'], "config")):
        with open(os.path.join(params['in_dir'], "config"), 'r') as f:
            tmp = params['in_dir']
            tmp2 = params['validationData']
            tmp3 = params['zoom']
            tmp4 = params['preload']
            model = params['model']
            config = f.read()
            params =  json.loads(config)
            params['in_dir'] = tmp
            params['validationData'] = tmp2
            params['zoom'] = tmp3
            params['preload'] = tmp4
            params['model'] = model

    timestamp = str(int(time.time()))
    jobid = os.environ['SLURM_JOBID']
    out_dir = os.path.abspath(os.path.join(
        "/scratch/s7550245/simulateSTM/classConvNet/runsEval",
        params['version'], jobid + "_" + timestamp))

    print("usevgg", params['useVGG'])
    if params['useVGG']:
        out_dir += "_" + "VGG" + str(params['top'])
        if params['stopGrad']:
            out_dir += "-stopGrad" + str(params['stopGrad'])
    else:
        out_dir += "_" + "noVGG"

    print("dropout", params['dropout'])
    if params['dropout']:
        out_dir += "_" + "dropout" + str(params['dropout'])
    else:
        out_dir += "_" + "noDropout"

    print("batchnorm", params['batchnorm'])
    if params['batchnorm']:
        out_dir += "_" + "batchnorm-" + str(params['batchnorm-decay'])
    else:
        out_dir += "_" + "noBatchnorm"

    print("cropSz", params['cropSz'])
    if params['cropSz']:
        out_dir += "_" + "cropSz" + str(params['cropSz'])

    print("reading data from: ", params['in_dir'])
    out_dir += params['in_dir'].split("/")[-1].split("_")[0]

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.copy2(sys.argv[0], os.path.join(out_dir, sys.argv[0]))

    config = json.dumps(params)
    with open(os.path.join(out_dir, "config"), 'w') as f:
        f.write(config)

    params['out_dir'] = out_dir
    # sys.stdout = open(os.path.join(out_dir, "log"), 'w')

    # with tf.device("/gpu:0"):
    global_step = tf.Variable(1, name='global_stepClass',
                              trainable=False)

    sess = tf.Session()
    # sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    if params['resume']:
        img_train, l_train, f_train, \
            img_test, l_test, f_test = loadData.restoreData(out_dir)
    else:
        img_train, l_train, f_train, \
            img_test, l_test, f_test = loadData.loadData(out_dir,
                                                         params,
                                                         rawImg=True,
                                                         fake=True)

    imgs = np.concatenate((img_train, img_test))
    labels = np.concatenate((l_train, l_test))
    files = np.concatenate((f_train, f_test))

    print("preload", params['preload'])
    if not params['preload']:
        imgs, labels, files = transformImage.transformImages(
            params, imgs, labels)

    # if params['useVGG']:
    #     imgs = scipy.ndimage.zoom(imgs,
    #                               (1,
    #                                224.0/imgs.shape[1],
    #                                224.0/imgs.shape[2],
    #                                1),
    #                               order=1)
    print("using vgg: {}".format(params['useVGG']))
    if params['useVGG']:
        imgs = preprocess_input(np.repeat(imgs, 3, axis=3))

    print(imgs.shape)
    nn = ClassConvNetEval(sess, params, glStep=global_step)

    # nn.saver.restore(sess,
    #                  tf.train.latest_checkpoint(params['in_dir']))
    if params['model'] is not None and not os.path.isdir(params['model']):
        print("restoring ... {}".format(params['model']))
        nn.saver.restore(
            sess,
            params['model'])
    elif params['model'] is not None:
        print("restoring ... {}".format(tf.train.latest_checkpoint(params['model'])))
        nn.saver.restore(
            sess,
            tf.train.latest_checkpoint(params['model']))
    else:
        print("restoring ... {}".format(tf.train.latest_checkpoint(params['in_dir'])))
        nn.saver.restore(
            sess,
            tf.train.latest_checkpoint(params['in_dir']))

    random.seed(params['seed'])
    seed = random.randint(0, 2**31)
    print("random_state: {}".format(seed))

    print("max {}, min {}".format(imgs.max(), imgs.min()))

    res = nn.runPrediction(imgs)
    cor = 0
    fn = 0
    fp = 0
    total = len(res)
    for i in range(total):
        tmp = False
        if res[i] < 0.5:
            if labels[i] == 0:
                cor += 1
                tmp = True
            else:
                fn += 1
        else:
            if labels[i] == 1:
                cor += 1
                tmp = True
            else:
                fp += 1
        print(tmp, res[i], labels[i], files[i])
    print("Accuracy: {} (correct: {}, total: {})".format(
        float(cor)/float(total), cor, total))
    print("False-Positives: {} | False-Negatives: {}".format(
        fp, fn))
