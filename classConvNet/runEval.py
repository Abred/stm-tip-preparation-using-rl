#!/bin/bash

#SBATCH --job-name=eval-classConvNet
#SBATCH -A p_cvldpose
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --time 0-00:10:00
#SBATCH --mem 30G
#SBATCH --mail-type=END,FAIL,TIME_LIMIT_90
#SBATCH --mail-user=s7550245@msx.tu-dresden.de
#SBATCH -o /scratch/s7550245/convNet/log.%j
###SBATCH -c 6
###SBATCH -c 6
###SBATCH --gres=gpu:1
###SBATCH --partition=gpu2


# module load eb
# module load tensorflow


# PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/.local/lib/python2.7/site-packages /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu/ld-2.17.so --library-path /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/bin/python \
PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/1.0.1-Python-2.7.12/lib/python2.7/site-packages/: /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu/ld-2.17.so --library-path /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.12-intel-2016.03-GCC-5.3/bin/python \
		  classConvNetEval.py \
		  `#--validationData "/scratch/s7550245/imagesClass7" `\
		  `#--validationData "/scratch/s7550245/images3RL2" `\
		  `#--validationData "/scratch/s7550245/tmpImgs" `\
		  --validationData "/home/s7550245/simulateSTM/imagesClass6Val" \
		  --zoom \
		  `#--vgg `\
		  `#--noPreload `\
		  `#--validationData /scratch/s7550245/tmp `\
		  `#-i /scratch/s7550245/simulateSTM/classConvNet/runs/2/7548745_1495199060_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.99_optmomentumimagesClass6 `\
		  `#-i /scratch/s7550245/simulateSTM/classConvNet/runs/2/7259416_1494427274_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.99_optmomentumimagesClass5 `\
		  `#--validationData /home/s7550245/simulateSTM/imagesClassVal2 `\
		  `#-i /scratch/s7550245/simulateSTM/classConvNet/runs/1/1492792260_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.99_optmomentum `\
		  -i $1 \
		  --model $2 \
		  `#-i /scratch/s7550245/simulateSTM/classConvNet/runs/2/7693028_1495648194_1000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.9_optmomentumimagesClass7 `\
