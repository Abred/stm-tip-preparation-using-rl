import numpy as np
import os
import shutil
import sys
import time

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim

import math

import tfutils


class DQN:
    numActions = 10  # num actions

    Hconv1 = 32
    Hconv2 = 48
    Hconv3 = 48
    HFC = [2048, 2048]
    # Hconv3 = None
    # HFC = [256, 128]
    HFCSmall = [128, 128, 128]
    HFCVGG = [1024, 1024]

    # tau = 0.0001
    state_dim = 50
    featureCnt = 3
    col_channels = 1

    def __init__(self, sess, out_dir, glStep, params):
        self.sess = sess
        self.params = params
        self.summaries = []
        self.weight_summaries = []
        if params['batchnorm']:
            # self.batchnorm = slim.batch_norm
            self.batchnorm = tfutils.batch_normBUG
        else:
            self.batchnorm = None

        self.dropout = params['dropout']
        self.weightDecay = params['weight-decay']
        self.learning_rate = params['learning-rate']
        self.momentum = params['momentum']
        self.opti = params['optimizer']
        self.bnDecay = params['batchnorm-decay']
        self.useVGG = params['useVGG']
        self.tau = params['tau']

        if self.useVGG:
            self.state_dim = 224
            self.col_channels = 3
            # self.images = tf.placeholder(
            #     tf.float32,
            #     shape=[None,
            #            224,
            #            224,
            #            3],
            #     name='input')

            self.images = tf.placeholder(
                tf.float32,
                shape=[None,
                       50,
                       50,
                       1],
                name='input')
            tmpImg = tf.image.resize_images(self.images, (224, 224))
            # self.images = tf.concat([self.images,
            #                          self.images,
            #                          self.images], axis=3)
            tmpImg = tf.image.grayscale_to_rgb(tmpImg)

            varTmp = tf.get_variable("tmp", shape=[1,1])
            self.vggsaver = tf.train.import_meta_graph(
                '/home/s7550245/vggFcNet/vgg-model.meta',
                import_scope="VGG",
                clear_devices=True,
                input_map={'input_1':tmpImg})

            _VARSTORE_KEY = ("__variable_store",)
            varstore = ops.get_collection(_VARSTORE_KEY)[0]
            variables = tf.get_collection(
                ops.GraphKeys.GLOBAL_VARIABLES,
                scope="VGG")

            # tmp = [n for n in sess.graph.as_graph_def().node]
            # for v in tmp:
            #     if "VGG" in v.name:
            #         print(v)

            for v in variables:
                varstore._vars[v.name.split(":")[0]] = v
                # print(v.name)

            self.top = params['top']
            if self.top == 7:
                self.pretrained = sess.graph.get_tensor_by_name(
                    "VGG/MaxPool_2:0")
            elif self.top == 10:
                self.pretrained = sess.graph.get_tensor_by_name(
                    "VGG/MaxPool_3:0")
            elif self.top == 13:
                self.pretrained = sess.graph.get_tensor_by_name(
                    "VGG/MaxPool_4:0")
            print(self.pretrained)
            self.pretrained = tf.stop_gradient(self.pretrained)

        print("dropout", self.dropout)
        self.global_step = glStep

        if params['numActions'] is not None:
            self.numActions = params['numActions']

        with tf.variable_scope('DQN'):
            self.keep_prob = tf.placeholder(tf.float32)
            self.isTraining = tf.placeholder(tf.bool)

            # DQN Network
            self.setNN()

            self.loss_op = self.define_loss()
            self.train_op = self.defineTraining()

        _VARSTORE_KEY = ("__variable_store",)
        varstore = ops.get_collection(_VARSTORE_KEY)[0]
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="DQN")

        if not params['smallNN'] and \
           (params['useClassNN'] or params['useRankNN']):
            if params['useClassNN']:
                newScope = "classConvNet"
            elif params['useRankNN']:
                newScope = "convNet"
            vd = {}
            for v in variables:
                print(v.name)
                vn = v.name.split(":")[0]
                if (vn.endswith("weights") or \
                    vn.endswith("biases")) and \
                    not "fc" in vn and \
                    not "out" in vn:
                    vn = vn.replace("DQN", newScope)
                    if not "target" in vn:
                        # vn = vn.replace("target/", "")
                        vd[vn] = v
                print(vn)
            self.saver = tf.train.Saver(vd)
        else:
            self.saver = tf.train.Saver()

        for v in variables:
            print(v.name)
            if v.name.endswith("weights:0") or \
               v.name.endswith("biases:0"):
                s = []
                var = v
                mean = tf.reduce_mean(var)
                s.append(tf.summary.scalar(v.name+'mean', mean))
                with tf.name_scope('stddev'):
                    stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
                s.append(tf.summary.scalar(v.name+'stddev', stddev))
                s.append(tf.summary.scalar(v.name+'max', tf.reduce_max(var)))
                s.append(tf.summary.scalar(v.name+'min', tf.reduce_min(var)))
                s.append(tf.summary.histogram(v.name+'histogram', var))

                self.weight_summaries += s

        self.summary_op = tf.summary.merge(self.summaries)
        self.weight_summary_op = tf.summary.merge(self.weight_summaries)
        self.writer = tf.summary.FileWriter(out_dir, sess.graph)

    def setNN(self):
        prevTrainVarCount = len(tf.trainable_variables())

        if self.useVGG:
            defNN = self.defineNNVGG
        else:
            if self.params['smallNN']:
                defNN = self.defineNNSmall
            else:
                defNN = self.defineNN
        self.input_pl, self.nn = defNN()
        self.nn_params = tf.trainable_variables()[prevTrainVarCount:]

        # Target Network
        with tf.variable_scope('target'):
            prevTrainVarCount = len(tf.trainable_variables())
            self.target_input_pl, self.target_nn = \
                defNN(isTargetNN=True)
            self.target_nn_params = \
                tf.trainable_variables()[prevTrainVarCount:]
            self.target_nn_init_op = self.define_init_target_nn_op()
            self.target_nn_update_op = self.define_update_target_nn_op()

    def defineNNVGG(self, isTargetNN=False):
        with tf.variable_scope('inf'):
            print("defining vgg net")
            net = self.pretrained

            with slim.arg_scope(
                [slim.fully_connected],
                activation_fn=tf.nn.relu,
                weights_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                normalizer_fn=self.batchnorm,
                    normalizer_params={
                        'fused': True,
                        'is_training': self.isTraining,
                        'updates_collections': None,
                        'decay': self.bnDecay,
                        'scale': True}
            ):

                if self.top == 1:
                    numPool = 0
                    self.HFCVGG = [256, 256]
                    H = 64
                elif self.top <= 2:
                    numPool = 1
                    H = 64
                    self.HFCVGG = [256, 256]
                elif self.top <= 4:
                    numPool = 2
                    H = 128
                    self.HFCVGG = [512, 512]
                elif self.top <= 7:
                    numPool = 3
                    H = 256
                    self.HFCVGG = [1024, 1024]
                elif self.top <= 10:
                    numPool = 4
                    H = 512
                    self.HFCVGG = [2048, 2048]
                elif self.top <= 13:
                    numPool = 5
                    H = 512
                    self.HFCVGG = [2048, 2048]

                # remSz = int(self.state_dim / 2**numPool)
                net = slim.flatten(net)
                # print(remSz, net,H)
                # net = tf.reshape(net, [-1, remSz*remSz*H],
                #                  name='flatten')

                for i in range(len(self.HFC)):
                    net = slim.fully_connected(net, self.HFCVGG[i],
                                               scope='fc' + str(i))
                    print(net)
                    if self.dropout:
                        net = slim.dropout(net,
                                           keep_prob=self.dropout,
                                           is_training=self.isTraining)
                        print(net)
                net = slim.fully_connected(net, self.numActions,
                                           activation_fn=None,
                                           scope='out')
                print(net)
                if not isTargetNN:
                    self.weight_summaries += [tf.summary.histogram('output', net)]
        return self.images, net

    def defineNN(self, isTargetNN=False):
        with tf.variable_scope('inf'):
            images = tf.placeholder(
                tf.float32,
                shape=[None,
                       self.state_dim,
                       self.state_dim,
                       self.col_channels],
                name='input')

            net = images
            # net = tf.Print(net, [net], "input:", summarize=30,
            #                    first_n=25)
            # net = (net - 127.0) / 255.0
            fact = 1.0
            # fact = 1.0/255.0
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=fact, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=fact, mode='FAN_AVG', uniform=True)):
                with slim.arg_scope([slim.conv2d], stride=1, padding='SAME'):
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv1,
                                      [3, 3], scope='conv1')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool1')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv2,
                                      [3, 3], scope='conv2')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool2')
                    print(net)
                    if self.Hconv3 is not None:
                        net = slim.repeat(net, 3, slim.conv2d, self.Hconv3,
                                          [3, 3], scope='conv3')
                        print(net)

                # remSz = int(self.state_dim / 2**2)
                net = slim.flatten(net)
                # net = tf.reshape(net, [-1, remSz*remSz*self.Hconv2],
                #                  name='flatten')

                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'decay': self.bnDecay,
                                        'scale': True}):
                    for i in range(len(self.HFC)):
                        net = slim.fully_connected(net, self.HFC[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.dropout,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, self.numActions,
                                           activation_fn=None,
                                           scope='out')
                print(net)
                # net = tf.sigmoid(net)
                net = tf.Print(net, [net], "output:", summarize=30,
                               first_n=25)
                if not isTargetNN:
                    self.weight_summaries += [tf.summary.histogram('output', net)]

        return images, net

    def defineNNSmall(self, isTargetNN=False):
        with tf.variable_scope('inf'):
            images = tf.placeholder(
                tf.float32,
                shape=[None, self.featureCnt],
                name='input')
            # net = tf.minimum(99, images)
            # print(net)
            # net = tf.one_hot(net, 100)
            # print(net)
            # net = tf.to_float(net)
            # print(net)
            # net = tf.reshape(net, [-1, 100])
            net = images
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                normalizer_fn=self.batchnorm,
                    normalizer_params={
                        'fused': True,
                        'is_training': self.isTraining,
                        'updates_collections': None,
                        'decay': self.bnDecay,
                        'scale': True}):
                for i in range(len(self.HFCSmall)):
                    net = slim.fully_connected(net, self.HFCSmall[i],
                                               scope='fc' + str(i))
                    if not isTargetNN:
                        self.weight_summaries += [tf.summary.histogram(
                            'fc' + str(i),
                            net)]
                    print(net)
                    if self.dropout:
                        net = slim.dropout(net, self.dropout)
                        print(net)
                net = slim.fully_connected(net, self.numActions,
                                           activation_fn=None,
                                           scope='out')
                print(net)
                # net = tf.sigmoid(net)

                if not isTargetNN:
                    self.weight_summaries += [tf.summary.histogram('output', net)]

        return images, net

    def define_update_target_nn_op(self):
        with tf.variable_scope('update'):
            if self.params['noHardResetDQN']:
                tau = tf.constant(self.tau, name='tau')
                invtau = tf.constant(1.0-self.tau, name='invtau')
                return \
                    [self.target_nn_params[i].assign(
                        tf.multiply(self.nn_params[i], tau) +
                        tf.multiply(self.target_nn_params[i], invtau))
                     for i in range(len(self.target_nn_params))]
            else:
                return \
                    [self.target_nn_params[i].assign(self.nn_params[i])
                     for i in range(len(self.target_nn_params))]

    def define_init_target_nn_op(self):
        with tf.variable_scope('update'):
            return \
                [self.target_nn_params[i].assign(self.nn_params[i])
                 for i in range(len(self.target_nn_params))]

    def define_loss(self):
        with tf.variable_scope('loss2'):
            self.td_targets_pl = tf.placeholder(tf.float32, [None, 1],
                                                name='tdTargets')
            self.action_ids_pl = tf.placeholder(tf.int32, [None, 1],
                                                name='actionIDs')

            index_mask = tf.reshape(tf.one_hot(self.action_ids_pl,
                                               self.numActions),
                                    [-1, self.numActions])
            index_mask = tf.Print(index_mask, [index_mask], "idm ", first_n=10,summarize=100)
            qs = tf.reduce_sum(self.nn * index_mask,
                                    axis=1, keep_dims=True)
            # qs = tf.gather(self.nn, self.action_ids_pl))
            qs = tf.Print(qs, [qs], "aoh ", first_n=10, summarize=100)
            # Huber loss
            self.delta = self.td_targets_pl - qs
            # lossL2 = tf.where(tf.abs(self.delta) < 1.0,
            #                   0.5 * tf.square(self.delta),
            #                   tf.abs(self.delta) - 0.5, name='clipped_error')
            # lossL2 = tf.reduce_mean(lossL2)

            lossL2 = slim.losses.mean_squared_error(
                self.td_targets_pl,
                qs)
            lossL2 = tf.Print(lossL2, [lossL2], "lossL2 ", first_n=25)

            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss',
                                      lossL2)]
            # regs = []
            # for v in self.nn_params:
            #     if "w" in v.name:
            #         # regs.append(tf.abs(v))
            #         regs.append(tf.nn.l2_loss(v))
            # lossReg = tf.add_n(regs) * self.weightDecay
            lossReg = tf.add_n(slim.losses.get_regularization_losses("DQN"))
            lossReg = tf.Print(lossReg, [lossReg], "regLoss ", first_n=25)
            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss_reg',
                                      lossReg)]

            loss = lossL2 + lossReg
            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss_with_reg',
                                      loss)]

        return loss

    def defineTraining(self):
        with tf.variable_scope('train'):
            learning_rate = self.learning_rate
            if self.params['lr-decay']:
                learning_rate = tf.train.exponential_decay(
                    self.learning_rate, self.global_step,
                    50000, 0.9, staircase=False)
                # learning_rate = tf.train.polynomial_decay(
                #     1.0 - self.learning_rate, self.global_step, 10000,
                #     end_learning_rate=self.learning_rate/100.0, power=1.0)
            momentum = self.momentum
            if self.params['mom-decay']:
                momentum = 1.0 - tf.train.exponential_decay(
                    1.0 - self.momentum, self.global_step,
                    50000, 0.9, staircase=False)
                # momentum = 1.0 - tf.train.polynomial_decay(
                #     1.0 - self.momentum, self.global_step, 10000,
                #     end_learning_rate=self.learning_rate/100.0, power=1.0)
            learning_rate = tf.Print(learning_rate, [learning_rate], "learning_rate", first_n=15)
            if self.opti == 'momentum':
                self.optimizer = tf.train.MomentumOptimizer(learning_rate,
                                                       momentum)
            elif self.opti == 'adam':
                self.optimizer = tf.train.AdamOptimizer(learning_rate)
            elif self.opti == 'sgd':
                self.optimizer = tf.train.GradientDescentOptimizer(
                    learning_rate)

            print(self.optimizer)
            return self.optimizer.minimize(self.loss_op,
                                           global_step=self.global_step)

    def run_train(self, inputs, actionIDs, targets):
        step = self.sess.run(self.global_step)
        if self.params['smallNN']:
            wSum = 50000
            lSum = 100
        else:
            wSum = 1000
            lSum = 50
        if (step+1) % wSum == 0:
            out, delta, loss, _, summaries = self.sess.run(
                [self.nn,
                 self.delta,
                 self.loss_op,
                 self.train_op,
                 self.weight_summary_op],
                feed_dict={
                    self.input_pl: inputs,
                    self.action_ids_pl: actionIDs,
                    self.td_targets_pl: targets,
                    self.isTraining: True,
                    self.keep_prob: self.dropout
                })
            self.writer.add_summary(summaries, step)
            self.writer.flush()
        elif (step+1) % lSum == 0:
            out, delta, loss, _, summaries = self.sess.run(
                [self.nn,
                 self.delta,
                 self.loss_op,
                 self.train_op,
                 self.summary_op],
                feed_dict={
                    self.input_pl: inputs,
                    self.action_ids_pl: actionIDs,
                    self.td_targets_pl: targets,
                    self.isTraining: True,
                    self.keep_prob: self.dropout
                })
            self.writer.add_summary(summaries, step)
            self.writer.flush()
        else:
            out, delta, loss, _ = self.sess.run([self.nn,
                                                 self.delta,
                                                 self.loss_op,
                                                 self.train_op],
                                                feed_dict={
                self.input_pl: inputs,
                self.action_ids_pl: actionIDs,
                self.td_targets_pl: targets,
                self.isTraining: True,
                self.keep_prob: self.dropout
            })
        return step, out, delta, loss

    def run_predict(self, inputs):
        return self.sess.run(self.nn, feed_dict={
            self.input_pl: inputs,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_predict_target(self, inputs):
        return self.sess.run(self.target_nn, feed_dict={
            self.target_input_pl: inputs,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_update_target_nn(self):
        self.sess.run(self.target_nn_update_op)
