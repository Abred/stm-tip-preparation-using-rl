#!/bin/bash

#SBATCH --job-name=dqn
##SBATCH -A p_cvldpose
#SBATCH -n 1
##SBATCH -N 1
#SBATCH --time 0-04:00:00
#SBATCH --mem 30G
#SBATCH --mail-type=END,FAIL,TIME_LIMIT_90
#SBATCH --mail-user=s7550245@msx.tu-dresden.de
#SBATCH -o /scratch/s7550245/simulateSTM/dqn/log.%j
###SBATCH -c 12
#SBATCH -c 6
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu2


# module load eb
# module load tensorflow


# PYTHONPATH=/home/s7550245/simulateSTM/convNet:/home/s7550245/pyutil:/home/s7550245/.local/lib/python2.7/site-packages /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu/ld-2.17.so --library-path /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/bin/python \
PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/1.0.1-Python-2.7.12/lib/python2.7/site-packages/: srun /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu/ld-2.17.so --library-path /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.12-intel-2016.03-GCC-5.3/bin/python \
		  `#-m cProfile -o profOut -s 'cumtime' `\
		  dqn.py \
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/3/7664087_1495492223_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0005_mom0.9_optadamimages3RL2 `\
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/ok/7862693_1496329917_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2 `\
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/3/7864705_1496337039_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2 `\
		  --rankNN /scratch/s7550245/simulateSTM/convNet/runs/3/7864269_1496330383_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2 \
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/4/8646679_1499267966_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2/model.ckpt-170002 `\
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/4/8954425_1499730043_2000_32_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.0001_mom0.9_optsgd_images3RL2/model.ckpt-102002 `\
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/4/8954425_1499730043_2000_32_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.0001_mom0.9_optsgd_images3RL2/model.ckpt-1002 `\
		  `#--rankNN /scratch/s7550245/simulateSTM/convNet/runs/3/7664087_1495492223_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0005_mom0.9_optadamimages3RL2 `\
		  `#--classNN /scratch/s7550245/simulateSTM/classConvNet/runs/2/7781067_1495978381_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd1e-05_lr1e-05_mom0.99_optmomentumimagesClass7 `\
		  `#--classNN /scratch/s7550245/simulateSTM/classConvNet/runs/2/7660264_1495475645_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.99_optmomentumimagesClass7 `\
		  --classNN /scratch/s7550245/simulateSTM/classConvNet/runs/3/7836376_1496268934_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentumtmpClassImgs \
		  `#--classNN /scratch/s7550245/simulateSTM/classConvNet/runs/4/8559132_1498835444_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentumtmpClassImgs `\
		  `#--classNN /scratch/s7550245/simulateSTM/classConvNet/runs/3/7799989_1496150233_1000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentumimagesClass7 `\
		  --cropSize 50 \
		  --version 19 \
		  --learning-rate 0.0000001 \
		  --momentum 0.9 \
		  --optimizer momentum \
		  --gamma 0.95 \
		  --randomEps 0 \
		  --stepsTillTerm 100 \
		  --startLearning 10000 \
		  --numEpochs 2000000 \
		  --async \
		  `#--sleep `\
		  --miniBatchSize 32 \
		  `#--dropout 0.5 `\
		  --weight-decay 0.01 \
		  --reward binary \
		  --rewardPos -1 \
		  --rewardNeg -2 \
		  --rewardFinal 10 \
		  `#--penaltyNeg 10 `\
		  --penaltySigma 3 \
		  --penaltyRadius 2 \
		  --peakMax 3 \
		  --peakLimit 1 \
		  --sigmaMax 35 \
		  --sigmaLimit 4 \
		  --radiusMax 11 \
		  --radiusLimit 2 \
		  --numActions 10 \
		  `#--vgg `\
		  `#--xFirstVggLayers 13 `\
		  `#--prioritized `\
		  `#--importanceSampling `\
		  --doubleDQN \
		  --noHardResetDQN \
		  --tau 0.01 \
		  --resetFreq 1000 \
		  `#--batchnorm `\
		  `#--smallNN `\
		  `#--initSigma 50 `\
		  `#--discountReward `\
		  `#--tabular `\
		  --lr-decay \
		  --mom-decay \
		  --annealSteps 50000 \
		  `#--zoom `\
		  --images "/home/s7550245/simulateSTM/images3RL2" \
		  --replaySz 100000 \
		  --storeModel \
		  --storeBuffer \
		  `#--useRankNN `\
		  `#--termAtFull `\
		  `#--limitExploring 75000 `\
		  `#--onlyLearn `\
		  `#-r "/scratch/s7550245/simulateSTM/dqn/runs/13/8701306_1499442323_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.001_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_limitEx20000_replay-100000" `\
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/12/8559150_1498835666_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-binary_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_limitEx100000_replay-100000/replayBuffer.pickle" `\
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/12/8557053_1498828604_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000/replayBuffer.pickle" `\
		  `#-r "/scratch/s7550245/simulateSTM/dqn/runs/13/8603672_1499174841_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000" `\
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/13/8622381_1499244072_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-200000/replayBuffer.pickle" `\
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/13/8645038_1499264599_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.1_lr0.005_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000/replayBuffer.pickle" `\
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/13/8603672_1499174841_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000/replayBuffer.pickle" `\
		  `#--loadModel "/scratch/s7550245/simulateSTM/dqn/runs/13/8603672_1499174841_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000" `\
		  `#-r /scratch/s7550245/simulateSTM/dqn/runs/10/7673036_1495550568_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optadam_noHardResetDQN-0.001_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-10000b `\
		  `#-r /scratch/s7550245/simulateSTM/dqn/runs/11/7978946_1497016242_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr1e-05_mom0.9_optadam_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000 `\
		  `#-r /scratch/s7550245/simulateSTM/dqn/runs/11/7978949_1497016298_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-binary_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-10000` \
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/13/8603672_1499174841_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000/replayBuffer.pickle" `\
		  --transitions "/home/s7550245/simulateSTM/transitions10.npy" \
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/12/8581183_1499083497_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000/replayBuffer.pickle" `\
		  `#--loadReplay "/scratch/s7550245/simulateSTM/dqn/runs/11/7933344_1496826317_ep2000000_randEps0_batch32_stt200_gamma0.95_noDropout_noBatchnorm_wd0.001_lr0.0005_mom0.9_optmomentum_noHardResetDQN-0.001_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000/replayBuffer.pickle" `\
		  `#--loadModel /scratch/s7550245/simulateSTM/dqn/runs/12/8581183_1499083497_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optmomentum_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000 `\
		  `#-r /scratch/s7550245/simulateSTM/dqn/runs/12/8475477_1498520670_ep2000000_randEps0_batch32_stt100_gamma0.95_noDropout_noBatchnorm_wd0.01_lr0.0001_mom0.9_optadam_noHardResetDQN-0.01_doubleDQN_reward-net_-1.0_-2.0_10.0_3.0_2.0_numActions-10_async_replay-100000b `\
