#!/bin/bash

timestamp=$(date +%s)

mkdir -p tempFiles/${timestamp}
cp dqn.py tempFiles/${timestamp}
cp dqnQNN.py tempFiles/${timestamp}
cp qTab.py tempFiles/${timestamp}
cp run.sh tempFiles/${timestamp}
cp ../stmTip.py tempFiles/${timestamp}
cp ../environment.py  tempFiles/${timestamp}
cp ../peakSigRadEnv.py tempFiles/${timestamp}
cp ../replay_buffer.py tempFiles/${timestamp}
cp ../sumTree.py tempFiles/${timestamp}

echo $timestamp

cd tempFiles/${timestamp}
if [ "x1" == "x${1}" ]
then
	./run.sh
else
	sbatch --signal=SIGUSR1@1200 $1 ./run.sh
fi
