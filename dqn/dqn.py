import signal
import cProfile
from keras.applications.vgg16 import preprocess_input

import parseNNArgs

import traceback
import threading
import pickle
import shutil
import glob
import os
import random
import sys
import time
import json
import math

import numpy as np
import scipy.ndimage

from qTab import QTab
from dqnQNN import DQN
from replay_buffer import ReplayBuffer
from sumTree import SumTree
from stmTip import STMTip
from peakSigRadEnv import PeakSigRadEnv
import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python import debug as tf_debug

def printT(s):
    sys.stdout.write(s + '\n')

def signal_handler(signal, frame):
    printT('Time is almost up!')
    sys.stdout.flush()
    rl.stopLearning = True
    rl.stopExploring = True
    time.sleep(10)

    printT("start exiting... ({})".format(time.ctime()))
    sys.stdout.flush()
    rl.logStuff()
    sys.stdout.flush()
    printT("end exiting ... bye ({})".format(time.ctime()))
    sys.stdout.flush()
    sys.exit(0)



class dqnRunner():
    def __init__(self, sess, params, out_dir=None):
        signal.signal(signal.SIGUSR1, signal_handler)

        self.params = params
        self.sess = sess
        self.lock = threading.Lock()
        self.annealSteps = params['annealSteps']
        self.numActions = params['numActions']
        self.featureCnt = 3
        self.tmpCnt = 0
        self.ct = 0
        self.ctTotal = 0
        self.ctTotal2 = 0
        self.ctIntv = 20

        printT("tensorflow version: {}".format(tf.__version__))
        if params['resume']:
            printT("resuming... {}".format(params['resume']))
            self.out_dir = params['resume']
        else:
            self.out_dir = out_dir

        printT("Summaries will be written to: {}\n".format(self.out_dir))
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)
            shutil.copy2("run.sh", os.path.join(self.out_dir, "run.sh"))
            shutil.copy2(sys.argv[0], os.path.join(self.out_dir, sys.argv[0]))
            shutil.copy2("qTab.py", os.path.join(self.out_dir, "qTab.py"))
            shutil.copy2("replay_buffer.py",
                         os.path.join(self.out_dir, "replay_buffer.py"))
            shutil.copy2("stmTip.py", os.path.join(self.out_dir, "stmTip.py"))
            shutil.copy2("sumTree.py",os.path.join(self.out_dir, "sumTree.py"))
            shutil.copy2("dqnQNN.py", os.path.join(self.out_dir, "dqnQNN.py"))
            shutil.copy2("environment.py",
                         os.path.join(self.out_dir, "environment.py"))
            shutil.copy2("peakSigRadEnv.py",
                         os.path.join(self.out_dir, "PeakSigRadEnv.py"))

        if not params['resume']:
            printT("new start... {}".format(self.out_dir))
            config = json.dumps(params)
            with open(os.path.join(self.out_dir, "config"), 'w') as f:
                f.write(config)

        if os.environ['SLURM_JOB_NAME'] != 'zsh':
            sys.stdout.flush()
            sys.stdout = open(os.path.join(self.out_dir, "log"), 'w')
            for k in self.params:
                printT("{}: {}".format(k, self.params[k]))

        if self.params['reward'] == 'net':
            if self.params['rankNN'] is not None:
                try:
                    self.rewardRankNet = ConvNet(self.sess, params)
                except:
                    self.rewardRankNet = ConvNetEval(self.sess, params)
            if self.params['classNN'] is not None:
                try:
                    self.rewardClassNet = ClassConvNet(self.sess, params)
                except:
                    self.rewardClassNet = ClassConvNetEval(self.sess, params)
        self.global_step = None
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES)
        for v in variables:
            if "global_step" in v.name:
                self.global_step = v
        if self.global_step is None:
            self.global_step = tf.Variable(0, name='global_step',
                                           trainable=False)
        self.resetGlStep = tf.assign(self.global_step, 0)

        if self.params['tabular']:
            if self.params['reward'] == 'term':
                self.q = QTab(self.out_dir, self.params, actionCnt=11)
            else:
                self.q = QTab(self.out_dir, self.params)
        else:
            self.q = DQN(self.sess, self.out_dir,
                         self.global_step, self.params)

        # self.simulator = STMTip(params)
        self.simulator = PeakSigRadEnv(params)
        self.sampleCntr = np.zeros((self.simulator.peakMax,
                                    self.simulator.sigmaMax,
                                    self.simulator.radiusMax))
        self.stateSteps = np.zeros((self.simulator.peakMax,
                                    self.simulator.sigmaMax,
                                    self.simulator.radiusMax))
        if self.params['replaySz'] is None:
            self.replayBufferSize = 1000000
        else:
            self.replayBufferSize = self.params['replaySz']
        if self.params['prioritized']:
            self.replay = SumTree(self.replayBufferSize,
                                  self.params['miniBatchSize'],
                                  self.annealSteps*20)
            printT("using SumTree")
        else:
            self.replay = ReplayBuffer(self.replayBufferSize)
            printT("using linear Buffer")
        self.imgs = glob.glob(os.path.join(self.params['images'], "*.npy"))



        if not params['resume'] or \
           int(params['resume'].split('/')[-1].split('_')[1]) > 1497190099:
           self.action_step = tf.Variable(0, name='action_step',
                                          trainable=False, dtype=tf.int32)
           self.increment_ac_step_op = tf.assign(self.action_step,
                                                 self.action_step+1)
           self.episode_step = tf.Variable(0, name='episode_step',
                                           trainable=False, dtype=tf.int32)
           self.increment_ep_step_op = tf.assign(self.episode_step,
                                                 self.episode_step+1)

        self.saver = tf.train.Saver()
        if not (not params['resume'] or \
           int(params['resume'].split('/')[-1].split('_')[1]) > 1497190099):
           self.action_step = tf.Variable(0, name='action_step',
                                          trainable=False, dtype=tf.int32)
           self.increment_ac_step_op = tf.assign(self.action_step,
                                                 self.action_step+1)
           self.episode_step = tf.Variable(0, name='episode_step',
                                           trainable=False, dtype=tf.int32)
           self.increment_ep_step_op = tf.assign(self.episode_step,
                                                 self.episode_step+1)
        self.resetEpStep = tf.assign(self.action_step, 0)
        self.resetAcStep = tf.assign(self.episode_step, 0)


    def run(self):
        episode_reward = tf.Variable(0., name="episodeReward")
        sum1a = tf.summary.scalar("Reward", episode_reward)
        episode_disc_reward = tf.Variable(0., name="episodeDiscReward")
        sum1b = tf.summary.scalar("Reward_discounted", episode_disc_reward)
        eval_reward = tf.Variable(0., name="evalReward")
        eval_reward_op = tf.summary.scalar("Eval-Reward", eval_reward)
        eval_disc_reward = tf.Variable(0., name="evalDiscReward")
        eval_disc_reward_op = tf.summary.scalar("Eval-Reward_discounted",
                                                eval_disc_reward)
        eval_sum_vars = [eval_reward, eval_disc_reward]
        eval_sum_op = tf.summary.merge([eval_reward_op, eval_disc_reward_op])
        episode_ave_max_q = tf.Variable(0., name='epsideAvgMaxQ')
        sum2 = tf.summary.scalar("Qmax_Value", episode_ave_max_q)

        peakFinal = tf.Variable(0., name="peakFinal")
        sum3 = tf.summary.scalar("PeakFinal", peakFinal)
        peakDiff = tf.Variable(0., name="peakDiff")
        sum4 = tf.summary.scalar("PeakDiff", peakDiff)

        sigmaFinal = tf.Variable(0., name="sigmaFinal")
        sum5 = tf.summary.scalar("SigmaFinal", sigmaFinal)
        sigmaDiff = tf.Variable(0., name="sigmaDiff")
        sum6 = tf.summary.scalar("SigmaDiff", sigmaDiff)

        radiusFinal = tf.Variable(0., name="radiusFinal")
        sum7 = tf.summary.scalar("RadiusFinal", radiusFinal)
        radiusDiff = tf.Variable(0., name="radiusDiff")
        sum8 = tf.summary.scalar("RadiusDiff", radiusDiff)

        stepCount = tf.Variable(0., name="stepCount")
        sum9 = tf.summary.scalar("StepCount", stepCount)

        ctAcc = tf.Variable(0., name="correctTermAcc")
        ctAccSum = tf.summary.scalar("correctTermAcc", ctAcc)
        ctAccTotal = tf.Variable(0., name="correctTermAccTotal")
        ctAccSumTotal = tf.summary.scalar("correctTermAccTotal", ctAccTotal)
        ctAccTotal2 = tf.Variable(0., name="correctTermAccTotal2")
        ctAccSumTotal2 = tf.summary.scalar("correctTermAccTotal2", ctAccTotal2)
        ctAccSum_ops = tf.summary.merge([ctAccSum,
                                         ctAccSumTotal, ctAccSumTotal2])
        e2 = 0

        self.histoTest = tf.placeholder(tf.int32,
                                        shape=[None, 1],
                                        name='histoTest')
        self.sum6 = tf.summary.histogram("histoTest", self.histoTest)

        summary_vars = [episode_reward, episode_disc_reward, episode_ave_max_q,
                        peakFinal, peakDiff,
                        sigmaFinal, sigmaDiff,
                        radiusFinal, radiusDiff,
                        stepCount]
        summary_ops = tf.summary.merge([sum1a, sum1b, sum2, sum3, sum4, sum5,
                                        sum6, sum7, sum8, sum9])
        self.writer = tf.summary.FileWriter(self.out_dir+"/train",
                                            self.sess.graph)

        self.state_summaries_vars = []
        self.state_summaries_ops = []
        for i in range(self.simulator.peakMax):
            for j in range(self.simulator.sigmaMax):
                for k in range(self.simulator.radiusMax):
                    n = "state_" + str(i) + "_" + str(j) + "_" + str(k)
                    # print(n)
                    varI = tf.Variable(0., name=n)
                    self.state_summaries_vars.append(varI)
                    sumI = tf.summary.scalar(n, varI)
                    self.state_summaries_ops.append(sumI)
        # self.state_summaries_ops = tf.summary.merge(self.state_summaries_ops)



        self.sess.run(tf.initialize_all_variables())
        # self.sess.run(self.q.target_nn_init_op)
        self.sess.graph.finalize()

        if self.params['useVGG'] and not self.params['resume']:
            self.q.vggsaver.restore(self.sess,
                                    '/home/s7550245/convNet/vgg-model')
            printT("VGG restored.")

        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="DQN")
        for v in variables:
            if v.name.endswith("conv1_1/weights:0"):
                print(v.name, self.sess.run(v))

        # variables = tf.get_collection(
        #     ops.GraphKeys.GLOBAL_VARIABLES,
        #     scope="classConvNet")
        # for v in variables:
        #     print(self.sess.run(v))

        if self.params['useClassNN']:
            print("restoring dqn net from classNN: {}".format(
                params['classNN']))
            if "model" in params['classNN']:
                self.q.saver.restore(
                    self.sess,
                    params['classNN'])
            else:
                self.q.saver.restore(
                    self.sess,
                    tf.train.latest_checkpoint(params['classNN']))
        elif self.params['useRankNN']:
            print("restoring dqn net from rankNN: {}".format(
                params['rankNN']))
            if "model" in params['rankNN']:
                self.q.saver.restore(
                    self.sess,
                    params['rankNN'])
            else:
                self.q.saver.restore(
                    self.sess,
                    tf.train.latest_checkpoint(params['rankNN']))

        self.sess.run(self.q.target_nn_init_op)
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="DQN")
        for v in variables:
            if v.name.endswith("conv1_1/weights:0"):
                print(v.name, self.sess.run(v))

        if self.params['reward'] == 'net':
            if self.params['rankNN'] is not None:
                print("restoring reward rank net from rankNN: {}".format(
                    params['rankNN']))
                if "model" in params['rankNN']:
                    self.rewardRankNet.saver.restore(
                        self.sess,
                        params['rankNN'])
                else:
                    self.rewardRankNet.saver.restore(
                        self.sess,
                        tf.train.latest_checkpoint(params['rankNN']))
                variables = tf.get_collection(
                    ops.GraphKeys.GLOBAL_VARIABLES,
                    scope="convNet")
                for v in variables:
                    if v.name.endswith("conv1_1/weights:0"):
                        print(self.sess.run(v))

            if self.params['classNN'] is not None:
                print("restoring reward class net from classNN: {}".format(
                    params['classNN']))
                if "model" in params['classNN']:
                    self.rewardClassNet.saver.restore(
                        self.sess,
                        params['classNN'])
                else:
                    self.rewardClassNet.saver.restore(
                        self.sess,
                        tf.train.latest_checkpoint(params['classNN']))
                # variables = tf.get_collection(
                #     ops.GraphKeys.GLOBAL_VARIABLES,
                #     scope="classConvNet")
                # for v in variables:
                #     print(self.sess.run(v))
            self.correctPosRank = 0
            self.incorrectPosRank = 0
            self.correctNegRank = 0
            self.incorrectNegRank = 0

            self.correctPosRank2 = 0
            self.incorrectPosRank2 = 0
            self.correctNegRank2 = 0
            self.incorrectNegRank2 = 0

            self.correctPosRank3 = 0
            self.incorrectPosRank3 = 0
            self.correctNegRank3 = 0
            self.incorrectNegRank3 = 0

            self.correctPosRank4 = 0
            self.incorrectPosRank4 = 0
            self.correctNegRank4 = 0
            self.incorrectNegRank4 = 0

            self.correctPosRankTotal = 0
            self.incorrectPosRankTotal = 0
            self.correctNegRankTotal = 0
            self.incorrectNegRankTotal = 0

            self.correctPosRankTotal2 = 0
            self.incorrectPosRankTotal2 = 0
            self.correctNegRankTotal2 = 0
            self.incorrectNegRankTotal2 = 0

            self.correctPosRankTotal3 = 0
            self.incorrectPosRankTotal3 = 0
            self.correctNegRankTotal3 = 0
            self.incorrectNegRankTotal3 = 0

            self.correctPosRankTotal4 = 0
            self.incorrectPosRankTotal4 = 0
            self.correctNegRankTotal4 = 0
            self.incorrectNegRankTotal4 = 0

            self.rankPosCount = 0
            self.rankNegCount = 0

            self.rankPosCount2 = 0
            self.rankNegCount2 = 0

            self.rankPosCount3 = 0
            self.rankNegCount3 = 0

            self.rankPosCount4 = 0
            self.rankNegCount4 = 0

            self.rankPosCountTotal = 0
            self.rankNegCountTotal = 0

            self.rankPosCountTotal2 = 0
            self.rankNegCountTotal2 = 0

            self.rankPosCountTotal3 = 0
            self.rankNegCountTotal3 = 0

            self.rankPosCountTotal4 = 0
            self.rankNegCountTotal4 = 0

            self.correctPosClass = 0
            self.incorrectPosClass = 0
            self.correctNegClass = 0
            self.incorrectNegClass = 0

            self.correctPosClass2 = 0
            self.incorrectPosClass2 = 0
            self.correctNegClass2 = 0
            self.incorrectNegClass2 = 0

            self.correctPosClassTotal = 0
            self.incorrectPosClassTotal = 0
            self.correctNegClassTotal = 0
            self.incorrectNegClassTotal = 0

            self.correctPosClassTotal2 = 0
            self.incorrectPosClassTotal2 = 0
            self.correctNegClassTotal2 = 0
            self.incorrectNegClassTotal2 = 0

            self.classPosCount = 0
            self.classNegCount = 0

            self.classPosCount2 = 0
            self.classNegCount2 = 0

            self.classPosCountTotal = 0
            self.classNegCountTotal = 0

            self.classPosCountTotal2 = 0
            self.classNegCountTotal2 = 0

        if params['loadModel']:
            self.saver.restore(sess, tf.train.latest_checkpoint(
                self.params['loadModel']))
            printT("Model {} restored.".format(self.params['loadModel']))

        if params['loadReplay'] is not None:
            self.replay.load(params['loadReplay'])
            printT("Buffer {} restored.".format(self.params['loadReplay']))

        if params['resume']:
            # variables = tf.get_collection(
            #     ops.GraphKeys.GLOBAL_VARIABLES,
            #     scope="DQN")
            # for v in variables:
            #     print(v.name, self.sess.run(v))
            # print(variables[0].name, self.sess.run(variables[0]))
            self.saver.restore(sess, tf.train.latest_checkpoint(self.out_dir))
            printT("Model {} restored.".format(
                tf.train.latest_checkpoint(self.out_dir)))
            # variables = tf.get_collection(
            #     ops.GraphKeys.GLOBAL_VARIABLES,
            #     scope="DQN")
            # for v in variables:
            #     print(v.name, self.sess.run(v))
            # print(variables[0].name, self.sess.run(variables[0]))
            self.replay.load(os.path.join(self.out_dir, "replayBuffer.pickle"))
            printT("Buffer {} restored.".format(self.out_dir))
            # self.logBuffer()
        else:
            self.sess.run(self.resetGlStep)

        # variables = tf.get_collection(
            #     ops.GraphKeys.GLOBAL_VARIABLES,
            #     scope="convNet")
            # for v in variables:
            #     print(v.name, self.sess.run(v))

        self.maxEpisodes = self.params['numEpochs']
        self.miniBatchSize = self.params['miniBatchSize']
        self.gamma = self.params['gamma']
        self.startLearning = self.params['startLearning']
        if self.params['tabular'] and self.params['sleep']:
            self.startLearning = min(self.startLearning*1000,
                                     self.replayBufferSize)
        elif self.params['smallNN'] and self.params['sleep']:
            self.startLearning = min(self.startLearning*10,
                                     self.replayBufferSize)
        if os.environ['SLURM_JOB_NAME'] == 'zsh' and not self.params['sleep']:
            self.startLearning = 1

        self.startEpsilon = 1.0
        # self.endEpsilon = 0.1
        self.endEpsilon = 0.0
        self.epsilon = self.startEpsilon
        self.evalEp = False

        self.learning = True
        self.pauseLearning = False
        self.pauseExploring = False
        self.stopLearning = False
        self.stopExploring = False

        # self.logStuff()
        # exit()

        # if self.params['onlyLearn']:
        #     self.learnWrap()
        #     return
        # elif self.params['async']:
        if self.params['async']:
            t = threading.Thread(target=self.learnWrap)
            t.daemon = True
            t.start()

        acs = sess.run(self.action_step)
        printT("start action step: {}".format(acs))
        # acs = 100000
        ac = acs
        evalEpReward = 0
        evalEpDiscReward = 0
        if self.params['tabular']:
            evalIntv = 50000
            evalCnt = 10000
        elif self.params['smallNN']:
            evalIntv = 10000
            evalCnt = 5000
        else:
            evalIntv = 1000
            evalCnt = 100
        evalOc = 0

        if self.params['onlyLearn']:
            sess.run(self.resetEpStep)
            sess.run(self.resetAcStep)
        fs = sess.run(self.episode_step)
        printT("start Episode: {}".format(fs))
        # fs = 25248
        for e in range(fs, self.maxEpisodes):
            while self.stopExploring:
                time.sleep(1)
            if self.params['onlyLearn'] or \
               (self.params['limitExploring'] is not None \
                and self.replay.size() >= self.params['limitExploring']):
                self.pauseExploring = True
                self.evalEp = False
            elif e % (evalIntv+evalCnt) >= evalIntv:
                self.evalEp = True
                if e % (evalIntv+evalCnt) == evalIntv:
                    printT("Start Eval Episodes!")
                    evalOc += 1
            else:
                self.evalEp = False

            sess.run(self.increment_ep_step_op)

            if (not self.evalEp):
                # rawState = self.reset()
                rawState = self.reset(sigma=self.params['initSigma'],
                                      radius=self.params['initRadius'],
                                      numPeaks=self.params['initPeaks'])
            else:
                rawState = self.reset(sigma=25,
                                      radius=6,
                                      numPeaks=1)

            if self.params['useVGG']:
                if not self.params['zoom']:
                    rawStateScaled = scipy.ndimage.zoom(rawState,
                                                        (1,
                                                         224.0/500.0,
                                                         224.0/500.0,
                                                         1),
                                                        order=1)
                else:
                    rawStateScaled = rawState
            elif self.params['smallNN']:
                rawStateScaled = rawState
            elif self.params['zoom']:
                rawStateScaled = rawState
            else:
                rawStateScaled = scipy.ndimage.zoom(rawState,
                                                    (1, 0.1, 0.1, 1),
                                                    order=1)

            nextState = self.simulator.getNextState(rawStateScaled)
            nextFeatures = list(self.simulator.currFeatures)
            featuresStart = nextFeatures
            terminal = False
            ep_reward = 0
            ep_disc_reward = 0
            ep_ave_max_q = 0

            step = 0
            while not terminal:
                if self.params['async']:
                    if not t.isAlive():
                        printT("alive {}".format(t.isAlive()))
                        printT("Exception in user code:")
                        printT('-'*60)
                        traceback.print_exc(file=sys.stdout)
                        printT('-'*60)
                        sys.stdout.flush()
                        t.join(timeout=None)
                        os._exit(-1)

                state = nextState
                self.simulator.takeStep()

                if self.params['useVGG']:
                    if self.params['zoom']:
                        stateScaled = scipy.ndimage.zoom(state,
                                                         (1,
                                                          224.0/500.0,
                                                          224.0/500.0,
                                                          1),
                                                         order=1)
                        stateScaled.shape = (1, 224, 224, 1)
                        stateScaled = preprocess_input(np.repeat(
                            stateScaled, 3,
                            axis=3))
                    else:
                        stateScaled = state
                        stateScaled.shape = (1, 224, 224, 1)
                elif self.params['smallNN']:
                    stateScaled = np.array(
                        [float(int(self.simulator.currFeatures[0])),
                         float(int(self.simulator.currFeatures[1])),
                         float(int(self.simulator.currFeatures[2]))])
                    stateScaled.shape = (1, self.featureCnt)
                elif self.params['zoom']:
                    stateScaled = scipy.ndimage.zoom(state,
                                                     (1, 0.1, 0.1, 1),
                                                     order=1)
                else:
                    stateScaled = state
                    stateScaled.shape = (1, 50, 50, 1)
                    # stateScaled = scipy.ndimage.zoom(state,
                    #                                  (1, 0.1, 0.1, 1),
                    #                                  order=1)

                step += 1
                if not self.evalEp:
                    sess.run(self.increment_ac_step_op)
                    ac += 1
                tmp_step = min(ac, self.annealSteps)
                self.epsilon = (self.startEpsilon - self.endEpsilon) * \
                               (1 - tmp_step / self.annealSteps) + \
                               self.endEpsilon
                # self.epsilon = self.endEpsilon
                # self.epsilon = 1.0
                if (not self.pauseExploring) and \
                   (not self.evalEp) and \
                   (e < self.params['randomEps'] or
                    np.random.rand() < self.epsilon):
                    if self.params['reward'] == 'term':
                        action = random.randint(0, self.numActions)
                    else:
                        action = random.randrange(0, self.numActions)
                    # printT("\nStep: {} Next action (e-greedy {}): {}".format(
                    #     ac,
                    #     self.epsilon,
                    #     action))
                else:
                    action = self.getActionsIDs(
                        stateScaled,
                        features=self.simulator.currFeatures)
                    # printT("\nStep:{} Next action: {}".format(ac, action))

                # terminal, newState, newFeatures = \
                #     self.simulator.act(
                #         rawStateScaled, action-4,
                #         factor=0.7)
                self.simulator.sampleStep(action-4)
                if self.simulator.currFeatures == self.simulator.nextFeatures:
                    nextState = np.copy(state)
                else:
                    nextState = self.simulator.getNextState(rawStateScaled)
                # printT(str(self.simulator.nextFeatures[0]))
                if self.params['useVGG']:
                    if self.params['zoom']:
                        nextStateScaled = scipy.ndimage.zoom(nextState,
                                                             (1,
                                                              224.0/500.0,
                                                              224.0/500.0,
                                                              1),
                                                             order=1)
                        nextStateScaled.shape = (1, 224, 224, 1)
                        nextStateScaled = preprocess_input(np.repeat(
                            stateScaled, 3,
                            axis=3))
                    else:
                        nextStateScaled = nextState
                elif self.params['smallNN']:
                    nextStateScaled = np.array(
                        [float(int(self.simulator.nextFeatures[0])),
                         float(int(self.simulator.nextFeatures[1])),
                         float(int(self.simulator.nextFeatures[2]))])
                    nextStateScaled.shape = (1, self.featureCnt)
                elif self.params['zoom']:
                    nextStateScaled = scipy.ndimage.zoom(nextState,
                                                         (1, 0.1, 0.1, 1),
                                                         order=1)
                else:
                    nextStateScaled = nextState
                    nextStateScaled.shape = (1, 50, 50, 1)
                    # newStateScaled = scipy.ndimage.zoom(newState,
                    #                                     (1, 0.1, 0.1, 1),
                    #                                     order=1)
                # !!!
                # from transiton file
                # net reward
                # if self.params['reward'] == 'binary' or self.evalEp:
                if self.params['reward'] == 'binary':
                    terminal, reward = self.simulator.getReward(action)
                elif self.params['reward'] == 'net':
                # printT("{}".format(reward))
                    terminal, reward = self.getRewardNet(stateScaled,
                                                         nextStateScaled,
                                                         action, state, e,
                                                         step)
                # printT("{}\n".format(reward))
                if self.params['reward'] == 'term' and \
                   action == self.numActions:
                    terminal = True

                if step == self.params['stepsTillTerm']:
                    terminal = True

                # reward, terminal = self.getReward(stateScaled, newStateScaled,
                #                                   terminal, action,
                #                                   features, newFeatures)
                ep_disc_reward += pow(self.gamma, step-1) * reward
                ep_reward += reward

                if not self.evalEp and not self.pauseExploring:
                    self.insertSamples(np.copy(stateScaled),
                                       action, reward, terminal,
                                       np.copy(nextStateScaled),
                                       self.simulator.currFeatures,
                                       self.simulator.nextFeatures)

                # self.simulator.takeStep()

                if not self.params['async']:
                    # printT("learn..")
                    self.learn()

                sys.stdout.flush()
                if not self.pauseExploring and \
                   not self.evalEp and \
                   self.params['sleep'] and \
                   self.params['async'] and \
                   (self.replay.size() >= self.startLearning) and \
                   (self.replay.size() >= self.miniBatchSize):
                    time.sleep(2)

            featuresEnd = self.simulator.currFeatures
            if self.params['stepsTillTerm'] == 1:
                featuresEnd = self.simulator.nextFeatures
            if self.evalEp:
                evalEpReward += ep_reward
                evalEpDiscReward += ep_disc_reward
                if e % (evalIntv+evalCnt) == (evalIntv+evalCnt-1):
                    summary_str = self.sess.run(eval_sum_op, feed_dict={
                        eval_sum_vars[0]: evalEpReward/float(evalCnt),
                        eval_sum_vars[1]: evalEpDiscReward/float(evalCnt)
                    })
                    self.writer.add_summary(summary_str, evalOc-1)
                    evalEpReward = 0.0
                    evalEpDiscReward = 0.0
                    printT("avg disc reward: {}, avg reward: {} <<<<----------------------------------".format(evalEpDiscReward/float(evalCnt), evalEpReward/float(evalCnt)))

                printT('Time: {} | Reward: {} | Discounted Reward: {} | Eval-Episode {} | correct termination {}'.
                       format(time.ctime(), ep_reward, ep_disc_reward, e,
                              self.simulator.isTerminal()))
            else:
                if (not self.params['tabular'] and not self.params['smallNN']) or \
                   e % 100:
                    et = e - (evalOc * evalCnt)
                    printT("step count: {}".format(step))
                    summary_str = self.sess.run(summary_ops, feed_dict={
                        summary_vars[0]: max(-100, ep_reward),
                        summary_vars[1]: ep_disc_reward,
                        summary_vars[2]: ep_ave_max_q / float(step),
                        summary_vars[3]: featuresEnd[0],
                        summary_vars[4]: featuresEnd[0] - featuresStart[0],
                        summary_vars[5]: featuresEnd[1],
                        summary_vars[6]: featuresEnd[1] - featuresStart[1],
                        summary_vars[7]: featuresEnd[2],
                        summary_vars[8]: featuresEnd[2] - featuresStart[2],
                        summary_vars[9]: step
                    })
                    self.writer.add_summary(summary_str, et)
                    self.writer.flush()
                    printT('Time: {} | Reward: {} | Discounted Reward: {} | Episode {} | Buffersize: {} | correct termination {}'.
                           format(time.ctime(), ep_reward, ep_disc_reward, e,
                                  self.replay.size(),
                                  self.simulator.isTerminal()))
                    printT("Peak: {}, Sigma: {}, Radius: {}\n".format(
                        (featuresStart[0], featuresEnd[0],
                         featuresEnd[0]-featuresStart[0]),
                        (featuresStart[1], featuresEnd[1],
                         featuresEnd[1]-featuresStart[1]),
                        (featuresStart[2], featuresEnd[2],
                         featuresEnd[2]-featuresStart[2])))
                    if step != self.params['stepsTillTerm'] or \
                       self.simulator.isTerminal():
                        e2 += 1
                    if self.simulator.isTerminal():
                        self.ct += 1
                        self.ctTotal += 1
                        self.ctTotal2 += 1
                    if (et+1) % self.ctIntv == 0:
                        printT("Termination accuracy: {} (current)".format(
                            float(self.ct)/float(self.ctIntv)))
                        printT("Termination accuracy: {} (total, {}, {})".format(
                            float(self.ctTotal)/float(et+1), self.ctTotal,et+1))
                        printT("Termination accuracy: {} (total2, {}, {})".format(
                            float(self.ctTotal2)/float(e2), self.ctTotal2, e2))
                        summary_str = self.sess.run(ctAccSum_ops,
                                                    feed_dict={
                            ctAcc: float(self.ct)/float(self.ctIntv),
                            ctAccTotal: float(self.ctTotal)/float(et+1),
                            ctAccTotal2: float(self.ctTotal2)/float(e2)
                                                    })
                        self.ct = 0
                        self.writer.add_summary(summary_str, et)
                    self.writer.flush()

            modelStoreIntv = 500
            # if self.params['zoom']:
            #     modelStoreIntv = 100
            # if self.params['tabular']:
            #     modelStoreIntv = 50000
            if self.params['storeModel'] and ((e+1) % modelStoreIntv) == 0:
                self.logModel()
            # if self.params['storeBuffer'] and ((e+1) % modelStoreIntv) == 0:
            #     self.logBuffer()

            statsIntv = 100
            if self.params['tabular'] and not self.params['sleep']:
                statsIntv = 10000
            if ((e+1) % statsIntv) == 0:
                if self.params['reward'] == 'net':
                    self.logRewardAccuracy()

            sys.stdout.flush()

        self.learning = False
        # time.sleep(10)
        # if ((e+1) % statsIntv) != 0:
        #     self.logSampleCntr()

        sys.stdout.flush()

    def learnWrap(self):
        try:
            self.learn()
            # cProfile.runctx('self.learn()', globals(), locals(), filename=os.path.join(self.out_dir, "profOutLearn"))
        except:
            printT("learn wrap failed")
            printT("Exception in user code:")
            printT('-'*60)
            traceback.print_exc(file=sys.stdout)
            printT('-'*60)
            sys.stdout.flush()
            os._exit(-1)

    def learn(self):
        y_batch = np.zeros((self.miniBatchSize, 1))

        tmp = np.zeros((self.miniBatchSize, self.numActions))
        while self.learning:
            if self.stopLearning:
                time.sleep(5.0)
                continue
            if self.replay.size() < self.startLearning or \
               self.replay.size() < self.miniBatchSize or \
               self.evalEp:
                if self.params['async']:
                    time.sleep(5.0)
                    continue
                else:
                    return
            sys.stdout.flush()

            # printT("learn2..")
            if self.params['prioritized']:
                self.lock.acquire()
                ids, w_batch, s_batch, a_batch, r_batch, t_batch,\
                    ns_batch, sig_batch, sig2_batch, ps_batch = \
                    self.replay.sample_batch(self.miniBatchSize)
                # printT("ids {}".format(ids))
                # printT("w_batch {}".format(w_batch))
                self.lock.release()
            else:
                s_batch, a_batch, r_batch, t_batch, ns_batch, sig_batch, sig2_batch = \
                    self.replay.sample_batch(self.miniBatchSize)
                # if self.params['useVGG']:
                #     if self.params['zoom']:
                #         s_batch = scipy.ndimage.zoom(s_batch,
                #                                      (1,
                #                                       224.0/50.0,
                #                                       224.0/50.0,
                #                                       1),
                #                                      order=1)
                #         ns_batch = scipy.ndimage.zoom(ns_batch,
                #                                       (1,
                #                                        224.0/50.0,
                #                                        224.0/50.0,
                #                                        1),
                #                                       order=1)

            # printT("s_batch {}".format(s_batch))
            # printT("a_batch {}".format(a_batch))
            # printT("r_batch {}".format(r_batch))
            # printT("t_batch {}".format(t_batch))
            # printT("ns_batch {}".format(ns_batch))

            # print(sig_batch, sig_batch.shape)
            for b in range(self.miniBatchSize):
                # printT("{} {} {} {}".format(int(sig_batch[b, 0]),
                #                             int(sig_batch[b, 1]),
                #                             int(sig_batch[b, 2]),
                #                             self.sampleCntr.shape))
                self.sampleCntr[int(sig_batch[b, 0]),
                                int(sig_batch[b, 1]),
                                int(sig_batch[b, 2])] += 1

            if self.params['smallNN']:
                s_batch.shape = (s_batch.shape[0], self.featureCnt)
                ns_batch.shape = (ns_batch.shape[0], self.featureCnt)

            if self.params['doubleDQN'] and \
               not self.params['tabular']:
                qValsNewState = self.estimate_ddqn(ns_batch, p=False, tmp=tmp)
            else:
                qValsNewState = self.predict_target_nn(ns_batch)
            # y_batch = np.zeros((self.miniBatchSize, 1))
            if self.params['importanceSampling']:
                wMax = np.max(w_batch)
                # print(w_batch)
                # print(wMax)
                for i in range(self.miniBatchSize):
                    if t_batch[i]:
                        y_batch[i] = w_batch[i] / wMax * r_batch[i]
                    else:
                        y_batch[i] = w_batch[i] / wMax * \
                            (r_batch[i] + self.gamma * qValsNewState[i])
            else:
                for i in range(self.miniBatchSize):
                    # if ns_batch[i] <= 5:
                    #     qValsNewState[i] = 10.0
                    if t_batch[i]:
                        y_batch[i] = r_batch[i]
                    else:
                        # printT(r_batch, qValsNewState)
                        y_batch[i] = r_batch[i] + self.gamma * qValsNewState[i]

            # for b in range(self.miniBatchSize):
            #     print(sig_batch[b], a_batch[b], r_batch[b],
            #           sig2_batch[b], qValsNewState[b], y_batch[b])

            gS, qs, delta = self.update(s_batch, a_batch, y_batch)
            # res1, res2 = self.rewardRankNet.runPrediction(s_batch, ns_batch)
            # res = self.rewardClassNet.runPrediction(s_batch)
            # resb = self.rewardClassNet.runPrediction(ns_batch)
            # printT("{} {} {} {}".format(res1, res2, res, resb))
            # try:
            #     printT("learning rate: {}".format(self.sess.run(self.q.optimizer._lr)))
            # except:
            #     printT("learning rate: {}".format(self.sess.run(self.q.optimizer._learning_rate)))
            if self.params['prioritized']:
                self.lock.acquire()
                for i in range(self.miniBatchSize):
                    self.replay.update(ids[i], abs(delta[i]))
                self.lock.release()

            # for b in range(self.miniBatchSize):
            #     printT("{} {} {} {} {} {} {} {}".format(
            #         sig_batch[b], sig2_batch[b],
            #         y_batch[b], ps_batch[b], delta[b], self.replay.epsilon,
            #         self.replay.alpha,
            #         (abs(delta[b]) + self.replay.epsilon) ** self.replay.alpha))

            # ep_ave_max_q += np.amax(qs)

            if self.params['noHardResetDQN']:
                self.update_targets()
            elif (gS+1) % self.params['resetFreq'] == 0:
                self.update_targets()

            if self.params['tabular']:
                intv = 100000
            elif self.params['smallNN']:
                intv = 100000
            else:
                intv = 50000
            if (gS+1) % intv == 0:
                if (gS+1) % (intv*10) == 0 and \
                   os.environ['SLURM_JOB_NAME'] != 'zsh':
                    shutil.copy2(os.path.join(self.out_dir, "log"),
                                 os.path.join(self.out_dir, "log-" +
                                              str(gS)))
                if self.params['prioritized']:
                    for i in range(self.miniBatchSize):
                        printT("{}, {}, {}, {}, {}, {}".format(
                            sig_batch[i], sig2_batch[i],
                            a_batch[i][0], y_batch[i][0],
                            r_batch[i][0], qValsNewState[i]))
                else:
                    for i in range(self.miniBatchSize):
                        printT("{}, {}, {}, {}, {}, {}".format(
                            sig_batch[i], sig2_batch[i],
                            a_batch[i][0], y_batch[i][0],
                            r_batch[i][0], qValsNewState[i]))
                self.logDQN()


            if not self.params['async']:
                return

    def getActionsIDs(self, state, features=None):
        qs = self.q.run_predict(state)
        # printT("{}: {}, ({})".format(features, qs, state))
        mx = np.amax(qs, axis=1)[0]
        if self.params['reward'] == 'term':
            print("not implemented yet")
            exit(-2)
            if mx < (100 - min(features, 100)) * 0.1:
                return self.numActions
        return np.argmax(qs, axis=1)[0]

    def getRewardNet(self, state, nextState, action, stateb, ep, step):
        # printT("{} {}".format(state.max(), nextState.max()))
        res1, res2 = self.rewardRankNet.runPrediction(state, nextState)
        res = self.rewardClassNet.runPrediction(state)
        resb = self.rewardClassNet.runPrediction(nextState)
        classThreshold = 0.5
        # np.save("/scratch/s7550245/tmp/file" + str(self.tmpCnt) + ".npy", state)
        # self.tmpCnt += 1
        # printT("reward (rank): {} - {} = {}".format(res1, res2,
        #                                             res1 - res2))
        # printT("reward (class): {} ({})".format(res, resb))
        term, rew = self.simulator.getReward(action)
        # printT("reward (true): {}, {}".format(rew, term))
        # printT("reward (true) result: {} {} ({}, {})".format(
        #     self.simulator.transitions[
        #         self.simulator.currFeatures[0],
        #         self.simulator.currFeatures[1],
        #         self.simulator.currFeatures[2],
        #         self.simulator.nextFeatures[0],
        #         self.simulator.nextFeatures[1],
        #         self.simulator.nextFeatures[2],
        #         action, 2],
        #     self.simulator.transitions[
        #         self.simulator.currFeatures[0],
        #         self.simulator.currFeatures[1],
        #         self.simulator.currFeatures[2],
        #         self.simulator.nextFeatures[0],
        #         self.simulator.nextFeatures[1],
        #         self.simulator.nextFeatures[2],
        #         action, 3],
        #     self.simulator.currFeatures, self.simulator.nextFeatures))
        eq = self.simulator.currFeatures == self.simulator.nextFeatures
        # printT("{} == {} = {}\n".format(self.simulator.currFeatures, self.simulator.nextFeatures, eq))

        # printT("\nTruth pos/net, not equal, prediction ?")
        if rew == self.params['rewardPos'] and not eq:
            if res2 > res1:
                self.correctPosRank3 += 1
            else:
                self.incorrectPosRank3 += 1
            self.rankPosCount3 += 1
        if rew == self.params['rewardNeg'] and not eq:
            if res2 <= res1:
                self.correctNegRank3 += 1
            else:
                self.incorrectNegRank3 += 1
            self.rankNegCount3 += 1

        # printT("\nPrediction pos/neg, not equal, truth ?")
        if res2 > res1 and not eq and not rew == self.params['rewardFinal']:
            if rew == self.params['rewardPos']:
                self.correctPosRank4 += 1
            else:
                self.incorrectPosRank4 += 1
            self.rankPosCount4 += 1
        if res2 <= res1 and not eq and not rew == self.params['rewardFinal']:
            if rew == self.params['rewardNeg']:
                self.correctNegRank4 += 1
            else:
                self.incorrectNegRank4 += 1
            self.rankNegCount4 += 1

        # printT("\nTruth pos/neg, prediction ?")
        if rew == self.params['rewardPos']:
            if res2 > res1:
                self.correctPosRank += 1
            else:
                self.incorrectPosRank += 1
            self.rankPosCount += 1
        if rew == self.params['rewardNeg']:
            if res2 <= res1:
                self.correctNegRank += 1
            else:
                self.incorrectNegRank += 1
            self.rankNegCount += 1

        # printT("\nPrediction pos/neg, truth ?")
        if res2 > res1 and not rew == self.params['rewardFinal']:
            if rew == self.params['rewardPos']:
                self.correctPosRank2 += 1
            else:
                self.incorrectPosRank2 += 1
            self.rankPosCount2 += 1
        if res2 <= res1 and not rew == self.params['rewardFinal']:
            if rew == self.params['rewardNeg']:
                self.correctNegRank2 += 1
            else:
                self.incorrectNegRank2 += 1
            self.rankNegCount2 += 1


        # printT("\nTruth good/bad, prediction ?")
        if rew == self.params['rewardFinal']:
            if res >= classThreshold:
                self.correctPosClass += 1
            else:
                self.incorrectPosClass += 1
            self.classPosCount += 1
        else:
            if res < classThreshold:
                self.correctNegClass += 1
            else:
                self.incorrectNegClass += 1
            self.classNegCount += 1

        # printT("\nPrediction good/bad, truth ?")
        if res >= classThreshold:
            if rew == self.params['rewardFinal']:
                self.correctPosClass2 += 1
            else:
                self.incorrectPosClass2 += 1
                # np.save(os.path.join(self.out_dir,
                #                      "image_"+ \
                #                      str(ep)+ "_" + \
                #                      str(step) + "_" + \
                #                      str(self.simulator.currFeatures[0]) + "_" + \
                #                      str(self.simulator.currFeatures[1]) + "_" + \
                #                      str(self.simulator.currFeatures[2]) + "_" + \
                #                      str(res)),
                #         state)
                # np.save(os.path.join(self.out_dir,
                #                      "imageB_"+ \
                #                      str(ep)+ "_" + \
                #                      str(step) + "_" + \
                #                      str(self.simulator.currFeatures[0]) + "_" + \
                #                      str(self.simulator.currFeatures[1]) + "_" + \
                #                      str(self.simulator.currFeatures[2]) + "_" + \
                #                      str(res)),
                #         stateb)
            self.classPosCount2 += 1
        else:
            if rew != self.params['rewardFinal']:
                self.correctNegClass2 += 1
            else:
                self.incorrectNegClass2 += 1
            self.classNegCount2 += 1

        if res1 < res2:
            reward = self.params['rewardPos']
        elif res1 >= res2:
            reward = self.params['rewardNeg']
        terminal = False

        if res > classThreshold:
            terminal = True
            reward = self.params['rewardFinal']
            # printT("reward (true): {}, {}".format(rew, term))
            # printT("reward (true) result: {} {} ({}, {})".format(
            #     self.simulator.transitions[
            #         self.simulator.currFeatures[0],
            #         self.simulator.currFeatures[1],
            #         self.simulator.currFeatures[2],
            #         self.simulator.nextFeatures[0],
            #         self.simulator.nextFeatures[1],
            #         self.simulator.nextFeatures[2],
            #         action, 2],
            #     self.simulator.transitions[
            #         self.simulator.currFeatures[0],
            #         self.simulator.currFeatures[1],
            #         self.simulator.currFeatures[2],
            #         self.simulator.nextFeatures[0],
            #         self.simulator.nextFeatures[1],
            #         self.simulator.nextFeatures[2],
            #         action, 3],
            # self.simulator.currFeatures, self.simulator.nextFeatures))
            # printT("reward (class): {} ({})".format(res, resb))

        # reward = float(resb - res)
        # printT("reward (diff): {} - {} = {}".format(res, resb, resb-res))
        # terminal = False
        return terminal, reward

    # def getReward(self, state, newState, terminal, action,
    #               features, newFeatures):
    #     if self.params['reward'] == 'single':
    #         print("not implemented yet")
    #         exit(-3)
    #         if terminal:
    #             if features < 5.0:
    #                 reward = 1.0
    #             else:
    #                 reward = -1.0
    #         else:
    #             reward = 0.0
    #         printT("reward (single): {}".format(reward))
    #     elif self.params['reward'] == 'net':
    #         # print("not implemented yet")
    #         # exit(-4)
            
    #         res1, res2 = self.rewardRankNet.runPrediction(state, newState)
    #         reward = res1 - res2
    #         # printT("reward (net): {} - {} = {}".format(res1, res2,
    #         #                                            reward))
    #         # printT("sigma: {} newSigma: {}".format(sigma, newSigma))
    #         if newFeatures[0] < features[0] or \
    #            newFeatures[1] < features[1] or \
    #            newFeatures[2] < features[2]:
    #             if res2 > res1:
    #                 self.correctPosRank += 1
    #                 # printT("correct pos")
    #             else:
    #                 self.incorrectPosRank += 1
    #                 # printT("incorrect pos")
    #             self.rankPosCount += 1
    #         if newFeatures[0] >= features[0] or \
    #            newFeatures[1] >= features[1] or \
    #            newFeatures[2] >= features[2]:
    #             if res2 <= res1:
    #                 self.correctNegRank += 1
    #                 # printT("correct neg")
    #             else:
    #                 self.incorrectNegRank += 1
    #                 # printT("incorrect neg")
    #             self.rankNegCount += 1
    #         if res1 < res2:
    #             reward = self.params['rewardPos']
    #         elif res1 >= res2:
    #             reward = self.params['rewardNeg']

    #         res = self.rewardClassNet.runPrediction(state)
    #         # if sigma <= 5.0:
    #         if res > 0.5:
    #             terminal = True
    #             reward = self.params['rewardFinal']
    #         if (features[0] < self.params['peakLimit'] or
    #             features[2] < self.params['radiusLimit']) and \
    #            features[1] < self.params['sigmaLimit']:
    #         # if features[0] < self.params['peakLimit'] and \
    #         #    features[1] < self.params['sigmaLimit'] and \
    #         #    features[2] < self.params['radiusLimit']:
    #             if res >= 0.5:
    #                 self.correctPosClass += 1
    #             else:
    #                 self.incorrectPosClass += 1
    #             self.classPosCount += 1
    #         else:
    #             if res < 0.5:
    #                 self.correctNegClass += 1
    #             else:
    #                 self.incorrectNegClass += 1
    #             self.classNegCount += 1
    #     elif self.params['reward'] == 'state':
    #         print("not implemented yet")
    #         exit(-5)

    #         reward = newSigma * (-1.0) * 0.1
    #         printT("reward (state): {}".format(reward))
    #     elif self.params['reward'] == 'binary':
    #         # print(features)
    #         # print(newFeatures)
    #         if newFeatures[0] < features[0] or \
    #            newFeatures[1] < features[1] or \
    #            newFeatures[2] < features[2]:
    #             reward = self.params['rewardPos']
    #         else:
    #         # elif newFeatures[0] >= features[0] or \
    #         #      newFeatures[1] >= features[1] or \
    #         #      newFeatures[2] >= features[2]:
    #             reward = self.params['rewardNeg']

    #         if (features[0] < self.params['peakLimit'] or
    #             features[2] < self.params['radiusLimit']) and \
    #            features[1] < self.params['sigmaLimit']:
    #             terminal = True
    #             reward = self.params['rewardFinal']
    #         # printT("reward (binary): {}".format(reward))
    #     elif self.params['reward'] == 'term':
    #         print("not implemented yet")
    #         exit(-6)

    #         if sigma <= 5:
    #             reward = 10
    #             terminal = True
    #         elif action == 10:
    #             reward = (100 - min(sigma, 100)) * 0.1
    #             terminal = True
    #         else:
    #             reward = 0
    #     else:
    #         print("not implemented yet")
    #         exit(-7)

    #         reward = (sigma - newSigma) * 0.1
    #         if sigma <= 5:
    #             terminal = True
    #             # reward = (5.0-sigma) * 0.1
    #             reward = 10.0
    #         printT("reward (none): {}".format(reward))

    #     return reward, terminal

    def update(self, states, actionIDs, targets):
        step, out, delta, loss = self.q.run_train(states, actionIDs, targets)
        if (not self.params['smallNN'] and (step % 10 == 0)) or (step % 100 == 0):
            printT("--------------------------------------->> time: {}, step: {}, loss: {}".format(time.ctime(), step, loss))

        if step % 100 == 0:
            time.sleep(0.001)
        if np.isnan(loss):
            printT("ABORT: NaN")
            sys.stdout.flush()
            os._exit(-1)
        return step, out, delta

    def update_targets(self):
        self.q.run_update_target_nn()

    def estimate_ddqn(self, states, p=False, tmp=None):
        qs = self.q.run_predict(states)
        if p:
            printT(states)
            printT("qs: {}".format(qs))
        maxA = np.argmax(qs, axis=1)
        if p:
            printT("maxa: {}".format(maxA))
        qs = self.q.run_predict_target(states)
        if p:
            printT("qstarget: {}".format(qs))
        # tmp = np.zeros(qs.shape)
        tmp.fill(0)
        tmp[np.arange(maxA.size), maxA] = 1
        tmp = tmp * qs
        if p:
            printT("tmp: {}".format(tmp))
        tmp = np.sum(tmp, axis=1)
        return tmp

    def predict_target_nn(self, states):
        qs = self.q.run_predict_target(states)
        return np.max(qs, axis=1)

    def predict_nn(self, states):
        qs = self.q.run_predict(states)
        return np.max(qs, axis=1)

    def reset(self, rand=True, sigma=None, radius=None, numPeaks=None):
        if rand:
            self.c = random.randrange(0, len(self.imgs))
        img = np.load(self.imgs[self.c])
        # img = img/img.max()*255.0
        # printT("\nLoading image {} ({}, {})".format(self.imgs[self.c],
        #                                             img.max(),
        #                                             img.min()))
        img.shape = (1, img.shape[0], img.shape[1], 1)
        if sigma is None or radius is None or numPeaks is None:
            self.simulator.reset(features=None)
        else:
            self.simulator.reset(features=[numPeaks, sigma, radius])
        return img

    def insertSamples(self, stateScaled, action, reward, terminal,
                      newStateScaled, features, newFeatures):
        if not self.params['smallNN']:
            stateScaled.shape = (stateScaled.shape[1],
                                 stateScaled.shape[2],
                                 stateScaled.shape[3])
            newStateScaled.shape = (newStateScaled.shape[1],
                                    newStateScaled.shape[2],
                                    newStateScaled.shape[3])

        # if self.params['useVGG'] and self.params['zoom']:
        #     stateScaled = scipy.ndimage.zoom(stateScaled,
        #                                      (50.0/224.0,
        #                                       50.0/224.0,
        #                                       1),
        #                                      order=1)
        #     newStateScaled = scipy.ndimage.zoom(newStateScaled,
        #                                         (50.0/224.0,
        #                                          50.0/224.0,
        #                                          1),
        #                                         order=1)
        self.lock.acquire()
        if self.params['prioritized']:
            self.replay.add(None,
                            (stateScaled, features, action, reward, terminal,
                             newStateScaled, newFeatures))
            if not self.params['smallNN']:
                self.replay.add(
                    None,
                    (np.ascontiguousarray(np.rot90(stateScaled, 2)),
                     features, action, reward, terminal,
                     np.ascontiguousarray(np.rot90(newStateScaled, 2)),
                     newFeatures))
                self.replay.add(
                    None,
                    (np.ascontiguousarray(np.fliplr(stateScaled)),
                     features, action, reward, terminal,
                     np.ascontiguousarray(np.fliplr(newStateScaled)),
                     newFeatures))
                self.replay.add(
                    None,
                    (np.ascontiguousarray(np.flipud(stateScaled)),
                     features, action, reward, terminal,
                     np.ascontiguousarray(np.flipud(newStateScaled)),
                     newFeatures))
        else:
            self.replay.add(stateScaled, features, action, reward, terminal,
                            newStateScaled, newFeatures)
            if not self.params['smallNN']:
                self.replay.add(
                    np.ascontiguousarray(np.rot90(stateScaled, 2)),
                    features, action, reward, terminal,
                    np.ascontiguousarray(np.rot90(newStateScaled, 2)),
                    newFeatures)
                self.replay.add(
                    np.ascontiguousarray(np.fliplr(stateScaled)),
                    features, action, reward, terminal,
                    np.ascontiguousarray(np.fliplr(newStateScaled)),
                    newFeatures)
                self.replay.add(
                    np.ascontiguousarray(np.flipud(stateScaled)),
                    features, action, reward, terminal,
                    np.ascontiguousarray(np.flipud(newStateScaled)),
                    newFeatures)
        self.lock.release()

        if self.pauseExploring == False and \
           self.replay.size() == self.replayBufferSize:
            if self.params['termAtFull']:
                printT("Buffer FULL!")
                self.logStuff()
                self.pauseExploring = True
                # exit()
        elif self.pauseExploring == False and \
             self.params['limitExploring'] is not None and \
             self.replay.size() >= self.params['limitExploring']:
            if self.params['termAtFull']:
                printT("Buffer FULL!")
                self.logStuff()
                self.pauseExploring = True

    def logDQN(self):
        if self.params['tabular']:
            self.q.print_stateCnt()

        s = np.zeros((1, self.featureCnt))
        self.logSampleCntr()
        if self.params['smallNN']:
            if self.params['prioritized']:
                printT("replay: alpha {}, beta {} (sumP {}, total {})".format(
                    self.replay.alpha,
                    self.replay.beta,
                    self.replay.sumP,
                    self.replay.total()))
            # s = np.zeros((1, 1))
            for i in range(self.simulator.peakMax):
                for j in range(self.simulator.sigmaMax):
                    for k in range(self.simulator.radiusMax):
                        s[0, 0] = i
                        s[0, 1] = j
                        s[0, 2] = k
                        qs = self.q.run_predict(s)
                        # printT(qs)
                        if self.params['reward'] == 'term' and \
                           not self.params['tabular']:
                            qs = np.append(qs,
                                           (100 - min(s, 100)) * 0.1,
                                           axis=1)
                        aID = np.argmax(qs, axis=1)[0]
                        printT("state: {}, actionID: {}".format(
                            [i, j, k], aID))
                        for l in range(len(qs[0])):
                            if l == aID:
                                printT("{}: {} <-- {}".format(l, qs[0][l], l))
                            else:
                                printT("{}: {}".format(l, qs[0][l]))
                        sid = i * self.simulator.sigmaMax * \
                              self.simulator.radiusMax + \
                              j * self.simulator.radiusMax + k
                        summary_str = self.sess.run(
                            self.state_summaries_ops[sid],
                            feed_dict={
                                self.state_summaries_vars[sid]:
                                qs[0][aID]
                            })
                        self.stateSteps[i, j, k] += 1
                        self.writer.add_summary(
                            summary_str,
                            self.stateSteps[i, j, k])
        else:
            if True:
                printT("states: time-start: {}".format(time.ctime()))
                for i in range(self.simulator.peakMax):
                    for j in range(self.simulator.sigmaMax):
                        for k in range(self.simulator.radiusMax):
                            c = random.randrange(0, len(self.imgs))
                            img = np.load(self.imgs[c])
                            img.shape = (1, img.shape[0],
                                         img.shape[1], 1)
                            if self.params['useVGG']:
                                if not self.params['zoom']:
                                    img = scipy.ndimage.zoom(
                                        img,
                                        (1,
                                         224.0/500.0,
                                         224.0/500.0,
                                         1),
                                        order=1)
                            else:
                                if not self.params['zoom']:
                                    img = scipy.ndimage.zoom(
                                        img,
                                        (1, 0.1, 0.1, 1),
                                        order=1)
                            img = self.simulator.transformState(
                                img, i, j, k)
                            if self.params['useVGG']:
                                if self.params['zoom']:
                                    img = scipy.ndimage.zoom(
                                        img,
                                        (1,
                                         224.0/500.0,
                                         224.0/500.0,
                                         1),
                                        order=1)
                            else:
                                if self.params['zoom']:
                                    img = scipy.ndimage.zoom(
                                        img,
                                        (1, 0.1, 0.1, 1),
                                        order=1)
                            qs = self.q.run_predict(img)
                            aID = np.argmax(qs, axis=1)[0]
                            printT("state: {}, actionID: {}".format(
                                [i, j, k], aID))
                            for l in range(len(qs[0])):
                                if l == aID:
                                    printT("{}: {} <-- {}".format(l, qs[0][l], l))
                                else:
                                    printT("{}: {}".format(l, qs[0][l]))
                            sid = i * self.simulator.sigmaMax * \
                                  self.simulator.radiusMax + \
                                  j * self.simulator.radiusMax + k
                            summary_str = self.sess.run(
                                self.state_summaries_ops[sid],
                                feed_dict={
                                    self.state_summaries_vars[sid]:
                                    qs[0][aID]
                                })
                            self.stateSteps[i, j, k] += 1
                            self.writer.add_summary(
                                summary_str,
                                self.stateSteps[i, j, k])
                printT("states: time-end: {}".format(time.ctime()))
            else:
                numBatches = 10
                for b in range(numBatches):
                    if self.params['prioritized']:
                        self.lock.acquire()
                        ids, w_batch, s_batch, a_batch,\
                            r_batch, t_batch, ns_batch, \
                            sig_batch, sig2_batch, p_batch = \
                            self.replay.sample_batch(self.miniBatchSize)
                        self.lock.release()
                    else:
                        s_batch, a_batch, r_batch, t_batch,\
                            ns_batch, sig_batch, sig2_batch = \
                            self.replay.sample_batch(self.miniBatchSize)
                    qs = self.q.run_predict(s_batch)
                    if self.params['reward'] == 'term' and \
                       not self.params['tabular']:
                        qs = np.append(
                            qs,
                            (100 - np.minimum(sig_batch, 100)) * 0.1,
                            axis=1)
                    aID = np.argmax(qs, axis=1)
                    for i in range(self.miniBatchSize):
                        state = sig_batch[i]
                        printT("state: {}, actionID: {}".format(
                            state, aID[i]))
                        for j in range(len(qs[i])):
                            if j == aID[i]:
                                printT("{}: {} <-- {}".format(
                                    j, qs[i][j], j))
                            else:
                                printT("{}: {}".format(
                                    j, qs[i][j]))
                        sid = state[0] * self.simulator.sigmaMax * \
                              self.simulator.radiusMax + \
                              state[1] * self.simulator.radiusMax + \
                              state[2]
                        sid = int(sid)
                        summary_str = self.sess.run(
                            self.state_summaries_ops[sid],
                            feed_dict={
                                self.state_summaries_vars[sid]:
                                qs[i][aID[i]]
                            })
                        self.stateSteps[int(state[0]),
                                        int(state[1]),
                                        int(state[2])] += 1
                        self.writer.add_summary(
                            summary_str,
                            self.stateSteps[int(state[0]),
                                            int(state[1]),
                                            int(state[2])])

    def logModel(self):
        printT("Saving model... (Time: {})".format(time.ctime()))
        save_path = self.saver.save(self.sess,
                                    self.out_dir + "/model.ckpt",
                                    global_step=self.global_step)
        printT("Model saved in file: {} (Time: {}), dumping buffer ...".format(save_path, time.ctime()))

    def logBuffer(self):
        printT("Dumping buffer... (Time: {})".format(time.ctime()))
        self.replay.dump(os.path.join(self.out_dir,
                                      "replayBuffer.pickle"))
        printT("Buffer dumped (Time: {})".format(time.ctime()))

    def logSampleCntr(self):
        for i in range(self.simulator.peakMax):
            for j in range(self.simulator.sigmaMax):
                for k in range(self.simulator.radiusMax):
                    printT("state: {}, sampled: {}".format(
                        (i, j, k), self.sampleCntr[i, j, k]))
        if self.params['tabular']:
            self.q.print_stateCnt()

    def logRewardAccuracy(self):
        self.correctPosRankTotal += self.correctPosRank
        self.incorrectPosRankTotal += self.incorrectPosRank
        self.correctNegRankTotal += self.correctNegRank
        self.incorrectNegRankTotal += self.incorrectNegRank
        self.rankPosCountTotal += self.rankPosCount
        self.rankNegCountTotal += self.rankNegCount
        printT("\nTruth pos/neg, prediction ?")
        printT("ranking relative: {} pos, {} neg".format(
            float(self.correctPosRank) /
            float(max(1, self.rankPosCount)),
            float(self.correctNegRank) /
            float(max(1, self.rankNegCount))))
        printT("ranking absolute: {} correct pos, {} total pos".format(
            self.correctPosRank, self.rankPosCount))
        printT("ranking absolute: {} correct neg, {} total neg".format(
            self.correctNegRank, self.rankNegCount))
        printT("ranking relative overall: {} pos, {} neg".format(
            float(self.correctPosRankTotal) /
            float(max(1, self.rankPosCountTotal)),
            float(self.correctNegRankTotal) /
            float(max(1, self.rankNegCountTotal))))
        printT("ranking absolute overall: {} correct pos, {} total pos".format(
            self.correctPosRankTotal, self.rankPosCountTotal))
        printT("ranking absolute overall: {} correct neg, {} total neg".format(
            self.correctNegRankTotal, self.rankNegCountTotal))
        self.correctPosRank = 0
        self.incorrectPosRank = 0
        self.correctNegRank = 0
        self.incorrectNegRank = 0
        self.rankPosCount = 0
        self.rankNegCount = 0

        self.correctPosRankTotal3 += self.correctPosRank3
        self.incorrectPosRankTotal3 += self.incorrectPosRank3
        self.correctNegRankTotal3 += self.correctNegRank3
        self.incorrectNegRankTotal3 += self.incorrectNegRank3
        self.rankPosCountTotal3 += self.rankPosCount3
        self.rankNegCountTotal3 += self.rankNegCount3
        printT("\nTruth pos/net, not equal, prediction ?")
        printT("ranking3 relative: {} pos, {} neg".format(
            float(self.correctPosRank3) /
            float(max(1, self.rankPosCount3)),
            float(self.correctNegRank3) /
            float(max(1, self.rankNegCount3))))
        printT("ranking3 absolute: {} correct pos, {} total pos".format(
            self.correctPosRank3, self.rankPosCount3))
        printT("ranking3 absolute: {} correct neg, {} total neg".format(
            self.correctNegRank3, self.rankNegCount3))
        printT("ranking3 relative overall: {} pos, {} neg".format(
            float(self.correctPosRankTotal3) /
            float(max(1, self.rankPosCountTotal3)),
            float(self.correctNegRankTotal3) /
            float(max(1, self.rankNegCountTotal3))))
        printT("ranking3 absolute overall: {} correct pos, {} total pos".format(
            self.correctPosRankTotal3, self.rankPosCountTotal3))
        printT("ranking3 absolute overall: {} correct neg, {} total neg".format(
            self.correctNegRankTotal3, self.rankNegCountTotal3))
        self.correctPosRank3 = 0
        self.incorrectPosRank3 = 0
        self.correctNegRank3 = 0
        self.incorrectNegRank3 = 0
        self.rankPosCount3 = 0
        self.rankNegCount3 = 0

        self.correctPosClassTotal += self.correctPosClass
        self.incorrectPosClassTotal += self.incorrectPosClass
        self.correctNegClassTotal += self.correctNegClass
        self.incorrectNegClassTotal += self.incorrectNegClass
        self.classPosCountTotal += self.classPosCount
        self.classNegCountTotal += self.classNegCount
        printT("\nTruth good/bad, prediction ?")
        printT("classify relative: {} pos, {} neg".format(
            float(self.correctPosClass) /
            float(max(1, self.classPosCount)),
            float(self.correctNegClass) /
            float(max(1, self.classNegCount))))
        printT("classify absolute: {} correct pos, {} total pos".format(
            self.correctPosClass, self.classPosCount))
        printT("classify absolute: {} correct neg, {} total neg".format(
            self.correctNegClass, self.classNegCount))
        printT("classify relative overall: {} pos, {} neg".format(
            float(self.correctPosClassTotal) /
            float(max(1, self.classPosCountTotal)),
            float(self.correctNegClassTotal) /
            float(max(1, self.classNegCountTotal))))
        printT("classify absolute overall: {} correct pos, {} total pos".format(
            self.correctPosClassTotal, self.classPosCountTotal))
        printT("classify absolute overall: {} correct neg, {} total neg".format(
            self.correctNegClassTotal, self.classNegCountTotal))
        self.correctPosClass = 0
        self.incorrectPosClass = 0
        self.correctNegClass = 0
        self.incorrectNegClass = 0
        self.classPosCount = 0
        self.classNegCount = 0

        self.correctPosRankTotal2 += self.correctPosRank2
        self.incorrectPosRankTotal2 += self.incorrectPosRank2
        self.correctNegRankTotal2 += self.correctNegRank2
        self.incorrectNegRankTotal2 += self.incorrectNegRank2
        self.rankPosCountTotal2 += self.rankPosCount2
        self.rankNegCountTotal2 += self.rankNegCount2
        printT("\nPrediction pos/neg, truth ?")
        printT("ranking2 relative: {} pos, {} neg".format(
            float(self.correctPosRank2) /
            float(max(1, self.rankPosCount2)),
            float(self.correctNegRank2) /
            float(max(1, self.rankNegCount2))))
        printT("ranking2 absolute: {} correct pos, {} total pos".format(
            self.correctPosRank2, self.rankPosCount2))
        printT("ranking2 absolute: {} correct neg, {} total neg".format(
            self.correctNegRank2, self.rankNegCount2))
        printT("ranking2 relative overall: {} pos, {} neg".format(
            float(self.correctPosRankTotal2) /
            float(max(1, self.rankPosCountTotal2)),
            float(self.correctNegRankTotal2) /
            float(max(1, self.rankNegCountTotal2))))
        printT("ranking2 absolute overall: {} correct pos, {} total pos".format(
            self.correctPosRankTotal2, self.rankPosCountTotal2))
        printT("ranking2 absolute overall: {} correct neg, {} total neg".format(
            self.correctNegRankTotal2, self.rankNegCountTotal2))
        self.correctPosRank2 = 0
        self.incorrectPosRank2 = 0
        self.correctNegRank2 = 0
        self.incorrectNegRank2 = 0
        self.rankPosCount2 = 0
        self.rankNegCount2 = 0

        self.correctPosRankTotal4 += self.correctPosRank4
        self.incorrectPosRankTotal4 += self.incorrectPosRank4
        self.correctNegRankTotal4 += self.correctNegRank4
        self.incorrectNegRankTotal4 += self.incorrectNegRank4
        self.rankPosCountTotal4 += self.rankPosCount4
        self.rankNegCountTotal4 += self.rankNegCount4
        printT("\nPrediction pos/neg, not equal, truth ?")
        printT("ranking4 relative: {} pos, {} neg".format(
            float(self.correctPosRank4) /
            float(max(1, self.rankPosCount4)),
            float(self.correctNegRank4) /
            float(max(1, self.rankNegCount4))))
        printT("ranking4 absolute: {} correct pos, {} total pos".format(
            self.correctPosRank4, self.rankPosCount4))
        printT("ranking4 absolute: {} correct neg, {} total neg".format(
            self.correctNegRank4, self.rankNegCount4))
        printT("ranking4 relative overall: {} pos, {} neg".format(
            float(self.correctPosRankTotal4) /
            float(max(1, self.rankPosCountTotal4)),
            float(self.correctNegRankTotal4) /
            float(max(1, self.rankNegCountTotal4))))
        printT("ranking4 absolute overall: {} correct pos, {} total pos".format(
            self.correctPosRankTotal4, self.rankPosCountTotal4))
        printT("ranking4 absolute overall: {} correct neg, {} total neg".format(
            self.correctNegRankTotal4, self.rankNegCountTotal4))
        self.correctPosRank4 = 0
        self.incorrectPosRank4 = 0
        self.correctNegRank4 = 0
        self.incorrectNegRank4 = 0
        self.rankPosCount4 = 0
        self.rankNegCount4 = 0

        self.correctPosClassTotal2 += self.correctPosClass2
        self.incorrectPosClassTotal2 += self.incorrectPosClass2
        self.correctNegClassTotal2 += self.correctNegClass2
        self.incorrectNegClassTotal2 += self.incorrectNegClass2
        self.classPosCountTotal2 += self.classPosCount2
        self.classNegCountTotal2 += self.classNegCount2
        printT("\nPrediction good/bad, truth ?")
        printT("classify2 relative: {} pos, {} neg".format(
            float(self.correctPosClass2) /
            float(max(1, self.classPosCount2)),
            float(self.correctNegClass2) /
            float(max(1, self.classNegCount2))))
        printT("classify2 absolute: {} correct pos, {} total pos".format(
            self.correctPosClass2, self.classPosCount2))
        printT("classify2 absolute: {} correct neg, {} total neg".format(
            self.correctNegClass2, self.classNegCount2))
        printT("classify2 relative overall: {} pos, {} neg".format(
            float(self.correctPosClassTotal2) /
            float(max(1, self.classPosCountTotal2)),
            float(self.correctNegClassTotal2) /
            float(max(1, self.classNegCountTotal2))))
        printT("classify2 absolute overall: {} correct pos, {} total pos".format(
            self.correctPosClassTotal2, self.classPosCountTotal2))
        printT("classify2 absolute overall: {} correct neg, {} total neg".format(
            self.correctNegClassTotal2, self.classNegCountTotal2))
        self.correctPosClass2 = 0
        self.incorrectPosClass2 = 0
        self.correctNegClass2 = 0
        self.incorrectNegClass2 = 0
        self.classPosCount2 = 0
        self.classNegCount2 = 0

    def logStuff(self):
        self.logDQN()
        self.logModel()
        self.logBuffer()
        if not self.params['smallNN']:
            self.logRewardAccuracy()
        # self.logSampleCntr()  -> in logDQN


params = parseNNArgs.parse(sys.argv[1:])

if params['resume'] or params['loadModel']:
    try:
        if params['resume']:
            fl = os.path.join(params['resume'], "config")
        elif params['loadModel']:
            fl = os.path.join(params['loadModel'], "config")
        with open(fl, 'r') as f:
            r = params['resume']
            o = params['onlyLearn']
            m = params['loadModel']
            rp = params['loadReplay']
            config = f.read()
            params = json.loads(config)
            params['resume'] = r
            params['onlyLearn'] = o
            params['loadModel'] = m
            params['loadReplay'] = rp
            if 'replaySz' not in params:
                params['replaySz'] = 500000
    except:
        print("resume: config file not found")

if params['onlyLearn'] and \
   not params['loadReplay'] and \
   not params['loadModel']:
    print("invalid parameters! onlyLearn only avaiable in combination with loadReplay and loadModel")
    exit(-232)

if os.environ['SLURM_JOB_NAME'] == 'zsh':
    params['version'] = 'tmp'
print(params, params['version'])
timestamp = str(int(time.time()))
jobid = os.environ['SLURM_JOBID']
out_dir = os.path.abspath(os.path.join(
    "/scratch/s7550245/simulateSTM/dqn/runs",
    params['version'], jobid + "_" + timestamp))

print("Number of epochs: ", params['numEpochs'])
out_dir += "_ep" + str(params['numEpochs'])

print("Number of random epochs: ", params['randomEps'])
out_dir += "_randEps" + str(params['randomEps'])

print("miniBatchSize: ", params['miniBatchSize'])
out_dir += "_batch" + str(params['miniBatchSize'])

print("stepstillterm: ", params['stepsTillTerm'])
out_dir += "_stt" + str(params['stepsTillTerm'])

print("usevgg", params['useVGG'])
if params['useVGG']:
    out_dir += "_" + "VGG" + str(params['top'])

print("gamma: ", params['gamma'])
out_dir += "_gamma" + str(params['gamma'])

if params['discountReward']:
    out_dir += "_discReward"

print("dropout", params['dropout'])
if params['dropout']:
    out_dir += "_" + "dropout" + str(params['dropout'])
else:
    out_dir += "_" + "noDropout"

print("batchnorm", params['batchnorm'])
if params['batchnorm']:
    out_dir += "_" + "batchnorm-" + str(params['batchnorm-decay'])
else:
    out_dir += "_" + "noBatchnorm"

print("weight decay", params['weight-decay'])
out_dir += "_wd" + str(params['weight-decay'])

print("learning rate", params['learning-rate'])
out_dir += "_lr" + str(params['learning-rate'])

print("momentum", params['momentum'])
out_dir += "_mom" + str(params['momentum'])

print("optimizer", params['optimizer'])
out_dir += "_opt" + params['optimizer']

print("noHardResetDQN", params['noHardResetDQN'])
if params['noHardResetDQN']:
    out_dir += "_" + "noHardResetDQN-" + str(params['tau'])
else:
    out_dir += "_" + "HardResetDQN-" + str(params['resetFreq'])

print("doubleDQN", params['doubleDQN'])
if params['doubleDQN']:
    out_dir += "_" + "doubleDQN"
else:
    out_dir += "_" + "noDoubleDQN"

print("reward", params['reward'])
if params['reward']:
    out_dir += "_" + "reward-" + params['reward']
    if params['rewardPos'] is not None:
        out_dir += "_" + str(params['rewardPos'])
    if params['rewardNeg'] is not None:
        out_dir += "_" + str(params['rewardNeg'])
    if params['rewardFinal'] is not None:
        out_dir += "_" + str(params['rewardFinal'])
    if params['penaltyNeg'] is not None:
        out_dir += "_" + str(params['penaltyNeg'])
    if params['penaltySigma'] is not None:
        out_dir += "_" + str(params['penaltySigma'])
    if params['penaltyRadius'] is not None:
        out_dir += "_" + str(params['penaltyRadius'])

print("numActions", params['numActions'])
if params['numActions']:
    out_dir += "_" + "numActions-" + str(params['numActions'])

if params['prioritized']:
    out_dir += "_" + "prio"

if params['async']:
    out_dir += "_" + "async"

if params['importanceSampling']:
    out_dir += "_" + "impSmpl"

if params['sleep']:
    out_dir += "_" + "sleep"

if params['limitExploring']:
    out_dir += "_" + "limitEx" + str(params['limitExploring'])

if params['smallNN']:
    out_dir += "_" + "smallNN"

if params['tabular']:
    out_dir += "_" + "tabular"

if params['initPeaks']:
    out_dir += "_" + "sig" + str(params['initPeaks'])

if params['initSigma']:
    out_dir += "_" + "sig" + str(params['initSigma'])

if params['initRadius']:
    out_dir += "_" + "sig" + str(params['initRadius'])

if params['replaySz']:
    out_dir += "_" + "replay-" + str(params['replaySz'])

if params['zoom']:
    out_dir += "_" + "zoom"


if params['rankNN']:
    if "model" not in params['rankNN']:
        sys.path.insert(1, params['rankNN'])
    else:
        sys.path.insert(1, os.path.dirname(params['rankNN']))
print(sys.path)
try:
    from convNetEval import ConvNet
except:
    from convNetEval import ConvNetEval

if params['classNN']:
    if "model" not in params['classNN']:
        sys.path.insert(1, params['classNN'])
    else:
        sys.path.insert(1, os.path.dirname(params['classNN']))
print(sys.path)
try:
    from classConvNetEval import ClassConvNet
except:
    from classConvNetEval import ClassConvNetEval

with tf.Session() as sess:
    # sess = tf_debug.LocalCLIDebugWrapperSession(sess)

    rl = dqnRunner(sess, params, out_dir=out_dir)
    rl.run()
