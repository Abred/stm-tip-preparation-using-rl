import numpy as np
import os
import shutil
import sys
import time
import threading

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim

import math

import tfutils


def printT(s):
    sys.stdout.write(s + '\n')


class QTab:
    def __init__(self, out_dir, params,
                 stateCnt=100, actionCnt=10):
        self.out_dir = out_dir
        self.params = params
        self.lock = threading.Lock()
        self.peakMax = params['peakMax']
        self.sigmaMax = params['sigmaMax']
        self.penaltySigma = params['penaltySigma']
        self.radiusMax = params['radiusMax']
        self.penaltyRadius = params['penaltyRadius']
        self.qVals = np.zeros((self.peakMax, self.sigmaMax, self.radiusMax,
                               actionCnt))
        print(self.qVals.shape)
        # for s in range(stateCnt):
        #     for a in range(actionCnt):
        #         self.qVals[s, a] = -99.0

        self.stateCntr = np.zeros((self.peakMax, self.sigmaMax, self.radiusMax,
                                   actionCnt))
        self.stateCnt = stateCnt
        self.actionCnt = actionCnt
        self.step = 0
        self.intv = 1
        self.data = np.zeros((1, self.peakMax, self.sigmaMax, self.radiusMax,
                              actionCnt))

    def run_predict(self, state):
        # printT(str(state))
        t = state.astype(int)
        # printT(str(t[:, 0]))
        # printT(str(t[:, 1]))
        # printT(str(t[:, 2]))
        self.lock.acquire()
        tmp = self.qVals[t[:, 0], t[:, 1], t[:, 2]]
        # printT(str(tmp))
        self.lock.release()
        return tmp

    def run_predict_target(self, state):
        # printT(str(state))
        t = state.astype(int)
        # printT(str(t[:, 0]))
        # printT(str(t[:, 1]))
        # printT(str(t[:, 2]))
        self.lock.acquire()
        tmp = self.qVals[t[:, 0], t[:, 1], t[:, 2]]
        # printT(str(tmp))
        self.lock.release()
        return tmp

    def run_train(self, states, actionIDs, targets):
        # print("train...")
        if (self.step % self.intv) == 0:
            printT("step {} invt {}".format(self.step, self.intv))
            self.lock.acquire()
            self.qVals.shape = (1,
                                self.qVals.shape[0],
                                self.qVals.shape[1],
                                self.qVals.shape[2],
                                self.qVals.shape[3])
            printT("{}".format(self.data.shape))
            self.data = np.append(self.data, self.qVals, axis=0)
            printT("{}".format(self.data.shape))
            # time.sleep(2)
            self.qVals.shape = (self.qVals.shape[1],
                                self.qVals.shape[2],
                                self.qVals.shape[3],
                                self.qVals.shape[4])
            self.lock.release()
            if self.step < 10:
                self.intv = 1
            elif self.step < 100:
                self.intv = 10
            elif self.step < 1000:
                self.intv = 100
            elif self.step < 10000:
                self.intv = 1000
            else:
            # elif self.step < 100000:
                self.intv = 1000
        self.step += 1
        delta = np.zeros((self.params['miniBatchSize'], 1))
        out = np.zeros((self.params['miniBatchSize'], 1))
        mse = 0
        # print(states, actionIDs)
        # printT(str(self.qVals.shape))
        for b in range(self.params['miniBatchSize']):
            peak = int(states[b][0])
            sigma = int(states[b][1])
            radius = int(states[b][2])
            action = int(actionIDs[b])
            # printT(str(states))
            delta[b] = targets[b] - self.qVals[peak, sigma, radius, action]
            mse += delta[b] * delta[b]
            # self.qVals[state, action] = \
            #     self.qVals[state, action] + \
            #     self.params['learning-rate'] * delta[b]
            alpha = 1.0 / (1 + math.pow(
                self.stateCntr[peak, sigma, radius, action], 1.0))
            # if self.step > 50000:
            #     alpha = 0.0
            self.stateCntr[peak, sigma, radius, action] += 1.0
            # printT("state {} action {} old: {} delta {}".format(state, action, self.qVals[state, action], delta[b]))
            self.lock.acquire()
            self.qVals[peak, sigma, radius, action] = \
                self.qVals[peak, sigma, radius, action] + \
                alpha * delta[b]
            out = self.qVals[peak, sigma, radius, action]
            self.lock.release()
            # printT("state {} action {} new: {}".format(state, action, self.qVals[state, action]))
        loss = mse / float(self.params['miniBatchSize'])
        return self.step, out, delta, loss

    def run_update_target_nn(self):
        return

    def print_stateCnt(self):
        printT("{}".format(self.data.shape))
        np.save(os.path.join(self.out_dir, "qvals.npy"), self.data)
        for i in range(self.peakMax):
            for j in range(self.sigmaMax):
                for k in range(self.radiusMax):
                    for a in range(self.actionCnt):
                        printT("state {}, action {}: sampled: {}".format(
                            (i, j, k), a, self.stateCntr[i, j, k, a]))
