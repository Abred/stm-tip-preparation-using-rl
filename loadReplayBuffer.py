import pickle
import numpy as np
import sys
import os
import gc

with open(sys.argv[1], 'rb') as f:
    gc.disable()
    # buf = pickle.load(f, encoding='latin1')
    buf = pickle.load(f)
    gc.enable()
buf = list(buf)

cnt = 0
for e in buf:
    isTerm = e[4]
    print(e[1], e[2], e[3], e[4], e[6])
    if (e[1][0] < 1 or e[1][2] < 2) and e[1][1] < 4:
        infix = "good"
    else:
        infix = "bad"
    if isTerm:
        fn = "image_" + infix + "_" + str(cnt) + \
             "_" + str(e[1][0]) + "_" + str(e[1][1]) + \
             "_" + str(e[1][2]) + ".npy"
        print(fn)
        np.save(os.path.join(sys.argv[2], fn), e[0])
    cnt += 1
