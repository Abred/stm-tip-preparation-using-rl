import scipy.ndimage.filters as spf
from scipy.special import expit
import scipy.signal as sps
import math
import sys
import numpy as np
from environment import Environment


def sigmoid(x, factor=1.0):
    "Numerically-stable sigmoid function."
    if x >= 0:
        z = math.exp(-x * factor)
        return 1 / (1 + z)
    else:
        # if x is less than zero then z will be small, denom can't be
        # zero because it's 1+z.
        z = math.exp(x * factor)
        return z / (1 + z)


def printT(s):
    sys.stdout.write(s + '\n')


class PeakSigRadEnv(Environment):
    """Implementation of Environment base class
    3 Features:
    peaks: number of peaks of STM tip
    sigma: blurr induced by size of STM tip
    radius: location of peaks
    """
    def __init__(self, params):
        super(PeakSigRadEnv, self).__init__(params)
        self.sigmoidFactor = 0.7
        self.ks = 500
        self.peakMax = params['peakMax']
        self.sigmaMax = params['sigmaMax']
        self.penaltySigma = params['penaltySigma']
        self.radiusMax = params['radiusMax']
        self.penaltyRadius = params['penaltyRadius']
        self.transitions = np.load(params['transitions'])

        self.reset()
        # self.computeTransitions()

    def act(self, action):
        self.sampleStep(action)
        self.takeStep(action)

    def isTerminal(self):
        if (self.currFeatures[0] < self.params['peakLimit'] or
            self.currFeatures[2] < self.params['radiusLimit']) and \
           self.currFeatures[1] < self.params['sigmaLimit']:
            return True
        else:
            return False

    def getProbability(self, features, action, nextFeatures):
        return self.transitions[
            features[0], features[1], features[2],
            nextFeatures[0], nextFeatures[1], nextFeatures[2],
            action]

    def sampleStep(self, action):
        rnd = np.random.rand()
        if self.isTerminal():
            self.nextFeatures = [0, 0, 0]
            return
        # score = 1.0 - sigmoid(action, factor=self.sigmoidFactor)
        ts = 0.0
        p2 = 0
        s2 = 0
        r2 = 0
        # print(action, rnd, ts)
        ts += self.transitions[self.currFeatures[0],
                               self.currFeatures[1],
                               self.currFeatures[2],
                               p2, s2, r2, action+4, 0]
        while ts < rnd:
            # ts += 0.1
            r2 += 1
            if r2 >= self.radiusMax:
                s2 += 1
                if s2 >= self.sigmaMax:
                    p2 += 1
                    if p2 >= self.peakMax:
                        # if rnd < 0.99:
                        printT("limit, should I get here? {}".format(
                            (self.currFeatures, p2, s2, r2, action, ts, rnd)))
                        r2 = self.radiusMax-1
                        s2 = self.sigmaMax-1
                        p2 = self.peakMax-1
                        break
                    s2 = 0
                r2 = 0
            ts += self.transitions[self.currFeatures[0],
                                   self.currFeatures[1],
                                   self.currFeatures[2],
                                   p2, s2, r2, action+4, 0]
            # printT("ts {}".format(ts))

        self.nextFeatures = [p2, s2, r2]
        # printT("new features {}".format(
        #                   (self.currFeatures, p2, s2, r2, action, ts, rnd)))

    def getNextState(self, state):
        if self.params['smallNN']:
            return self.nextFeatures
        return self.transformState(state,
                                   self.nextFeatures[0],
                                   self.nextFeatures[1],
                                   self.nextFeatures[2])

    def transformState(self, state, peaks, sigma, radius):
        state.shape = (state.shape[1], state.shape[2])
        if not self.params['zoom']:
            kernel = np.zeros((self.ks/10, self.ks/10))
        else:
            kernel = np.zeros((self.ks, self.ks))
        rep = True
        while rep:
            rep = False
            angs = []
            for i in range(peaks+1):
                angs.append(np.random.rand() * 2 * np.pi)
            for i in range(peaks+1):
                for j in range(i+1, peaks+1):
                    if (np.pi - abs(abs(angs[i] - angs[j]) - np.pi)) < 2 * np.pi / 5.0:
                        rep = True
            if rep:
                continue
        for j in range(len(angs)):
            r = pow(radius, 2)
            if not self.params['zoom']:
                r = r/10.0

            y = int(np.sin(angs[j]) * r + kernel.shape[0]/2)
            x = int(np.cos(angs[j]) * r + kernel.shape[0]/2)
            # print(x, y)
            if j == 0:
                kernel[y, x] = 1.0
            else:
                if kernel[y, x] != 0.0:
                    print("peak same loc, {}".format((peaks, sigma, radius)))
                else:
                    # kernel[y, x] += 0.334/(len(angs)-1)
                    kernel[y, x] += 0.334
            # kernel[y, x] = 1.0
        if not self.params['zoom']:
            kernel = spf.gaussian_filter(kernel, (sigma+1)/10.0)
        else:
            kernel = spf.gaussian_filter(kernel, (sigma+1))
        # filteredState = spf.convolve(state, kernel, mode='constant')
        filteredState = sps.fftconvolve(state, kernel, mode='same')
        filteredState = filteredState/filteredState.max()*255.0
        state.shape = (1, state.shape[0], state.shape[1], 1)
        filteredState.shape = (1,
                               filteredState.shape[0],
                               filteredState.shape[1],
                               1)
        return filteredState

    def reset(self, features=None):
        if features is None:
            numPeaks = int(min(max(np.random.exponential(1.5), 1.0),
                               self.peakMax))-1
            sigma = np.random.randint(0, self.sigmaMax)
            radius = np.random.randint(0, self.radiusMax)
            self.currFeatures = [numPeaks, sigma, radius]
        else:
            self.currFeatures = features
        self.nextFeatures = self.currFeatures

    def computeTransitions(self):
        print("use transition file!\n\n\n")
        self.transitions = np.zeros((self.params['peakMax'],
                                     self.params['sigmaMax'],
                                     self.params['radiusMax'],
                                     self.params['peakMax'],
                                     self.params['sigmaMax'],
                                     self.params['radiusMax'],
                                     self.params['numActions'],
                                     2), dtype=np.float32)
        for i in range(self.params['peakMax']):
            for j in range(self.params['sigmaMax']):
                for k in range(self.params['radiusMax']):
                    if ((i < self.params['peakLimit'] or
                         k < self.params['radiusLimit']) and
                        j < self.params['sigmaLimit']):
                        continue
                    for a in range(self.params['numActions']):
                        score = 1.0 - sigmoid(a, self.sigmoidFactor)
                        prob = 0.0
                        rew = 0.0

                        i2 = max(0, i-1)
                        j2 = int(j*score)
                        k2 = int(k*score)
                        prob = (1-score) * score * 0.3333334
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = int(j*score)
                        k2 = k;
                        prob = (1-score) * score * 0.3333333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = j
                        k2 = int(k*score)
                        prob = (1-score) * score * 0.3333333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        i2 = i
                        j2 = int(j*score)
                        k2 = int(k*score)
                        prob = score * score * 0.3333334
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = int(j*score)
                        k2 = k
                        prob = score * score * 0.3333333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = j
                        k2 = int(k*score)
                        prob = score * score * 0.3333333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        i2 = min(self.peakMax-1, i+1)
                        j2 = int(min(j+self.penaltySigma, self.sigmaMax-1))
                        k2 = int(min(k+self.penaltyRadius, self.radiusMax-1))
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = int(min(j+self.penaltySigma, self.sigmaMax-1))
                        k2 = k
                        prob = (1-score) * (1-score) * 0.3333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = j
                        k2 = int(min(k+self.penaltyRadius, self.radiusMax-1))
                        prob = (1-score) * (1-score) * 0.3333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        i2 = i
                        j2 = int(min(j+self.penaltySigma, self.sigmaMax-1))
                        k2 = int(min(k+self.penaltyRadius, self.radiusMax-1))
                        prob = score * (1-score) * 0.3334
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = int(min(j+self.penaltySigma, self.sigmaMax-1))
                        k2 = k
                        prob = score * (1-score) * 0.3333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

                        j2 = j
                        k2 = int(min(k+self.penaltyRadius, self.radiusMax-1))
                        prob = score * (1-score) * 0.3333
                        self.setTransition(i, j, k, i2, j2, k2, a, prob)

    def setTransition(self, i, j, k, i2, j2, k2, a, prob):
        rew = self.params['rewardPos'] \
              if i2 < i or j2 < j or k2 < k \
              else self.params['rewardNeg']
        self.transitions[i][j][k][i2][j2][k2][a][0] = prob
        self.transitions[i][j][k][i2][j2][k2][a][1] = rew

    def getReward(self, action):
        rew = self.transitions[
            self.currFeatures[0],
            self.currFeatures[1],
            self.currFeatures[2],
            self.nextFeatures[0],
            self.nextFeatures[1],
            self.nextFeatures[2],
            action, 1]
        term = self.isTerminal()

        # brokenOracle = False
        # if brokenOracle:
        #     rnd = np.random.rand()
        #     if rnd < 0.15:
        #         if rew == self.params['rewardPos']:
        #             rew = -2
        #         elif rew == self.params['rewardNeg']:
        #             rew = -1

        #         if term == True:
        #             term = False
        #         elif term == False:
        #             term = True
        if term:
            return (True, self.params['rewardFinal'])
        else:
            return (False, rew)
