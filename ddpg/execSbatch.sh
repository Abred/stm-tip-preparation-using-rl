#!/bin/bash

timestamp=$(date +%s)
echo $timestamp

mkdir -p tempFiles/${timestamp}
cp ddpg.py tempFiles/${timestamp}
cp ddpgActor.py tempFiles/${timestamp}
cp ddpgCritic.py tempFiles/${timestamp}
cp run.sh tempFiles/${timestamp}
cp ../stmTip.py tempFiles/${timestamp}
cp ../replay_buffer.py tempFiles/${timestamp}
cp ../sumTree.py tempFiles/${timestamp}

cd tempFiles/${timestamp}
if [ "x" != "x${1}" ]
then
	./run.sh
else
	sbatch ./run.sh
fi
