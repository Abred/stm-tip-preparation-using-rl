from keras.applications.vgg16 import preprocess_input

import parseNNArgs

import traceback
import math
import png
import threading
import pickle
import shutil
import glob
import os
import random
import sys
import time
import json

import numpy as np
import scipy.ndimage

from ddpgActor import Actor
from replay_buffer import ReplayBuffer
from sumTree import SumTree
from ddpgCritic import Critic
from stmTip import STMTip
from convNetEval import ConvNet
import tensorflow as tf
from tensorflow.python.framework import ops

def printT(s):
    sys.stdout.write(s + '\n')


class ddpgRunner():
    def __init__(self, sess, params, out_dir=None):
        self.params = params
        self.sess = sess
        self.lock = threading.Lock()
        self.annealSteps = params['annealSteps']
        self.cnt=0

        printT("tensorflow version: {}".format(tf.__version__))
        if params['resume']:
            printT("resuming... {}".format(params['resume']))
            self.out_dir = params['resume']
        else:
            self.out_dir = out_dir

        printT("Summaries will be written to: {}\n".format(self.out_dir))
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)
            shutil.copy2(sys.argv[0], os.path.join(self.out_dir, sys.argv[0]))
            shutil.copy2("ddpgActor.py", os.path.join(self.out_dir, "ddpgActor.py"))
            shutil.copy2("ddpgCritic.py", os.path.join(self.out_dir, "ddpgCritic.py"))
            shutil.copy2("replay_buffer.py",
                         os.path.join(self.out_dir, "replay_buffer.py"))
            shutil.copy2("stmTip.py", os.path.join(self.out_dir, "stmTip.py"))
            shutil.copy2("sumTree.py",os.path.join(self.out_dir, "sumTree.py"))

        if not params['resume']:
            printT("new start... {}".format(self.out_dir))
            config = json.dumps(params)
            with open(os.path.join(self.out_dir, "config"), 'w') as f:
                f.write(config)

        if os.environ['SLURM_JOB_NAME'] != 'zsh':
            sys.stdout.flush()
            sys.stdout = open(os.path.join(self.out_dir, "log"), 'w')

        if self.params['reward'] == 'net':
            self.rewardNet = ConvNet(self.sess, params)

        self.global_step = None
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES)
        for v in variables:
            if "global_step" in v.name:
                self.global_step = v
        if self.global_step is None:
            self.global_step = tf.Variable(0, name='global_step',
                                           trainable=False)
        if params['useVGG']:
            self.loadVGG()
            self.actor = Actor(self.sess, self.out_dir, self.params,
                               inputs=self.images)
            self.critic = Critic(self.sess, self.out_dir,
                                 self.global_step, self.params,
                                 inputs=self.images)
        else:
            self.actor = Actor(self.sess, self.out_dir, self.params)
            self.critic = Critic(self.sess, self.out_dir,
                                 self.global_step, self.params)

        self.sampleCntr = np.zeros((100, 1))
        self.stateSteps = np.zeros((100, 1))

        self.simulator = STMTip(params)
        replayBufferSize = 500000
        if self.params['prioritized']:
            self.replay = SumTree(replayBufferSize,
                                  self.params['miniBatchSize'],
                                  self.annealSteps*20)
            printT("using SumTree")
        else:
            self.replay = ReplayBuffer(replayBufferSize)
            printT("using linear Buffer")
        self.imgs = glob.glob("/home/s7550245/simulateSTM/images2/*.npy")

        self.sess.run(tf.assign(self.global_step, 0))

        if self.params['useVGG'] and not self.params['resume']:
            self.vggsaver.restore(self.sess,
                                    '/home/s7550245/convNet/vgg-model')
            printT("VGG restored.")

        self.saver = tf.train.Saver()
        if params['resume']:
            self.saver.restore(sess, tf.train.latest_checkpoint(self.out_dir))
            self.replay.load(os.path.join(self.out_dir, "replayBuffer.pickle"))
            printT("Model restored.")


    def run(self):
        episode_reward = tf.Variable(0., name="episodeReward")
        sum1a = tf.summary.scalar("Reward", episode_reward)
        episode_disc_reward = tf.Variable(0., name="episodeDiscReward")
        sum1b = tf.summary.scalar("Reward_discounted", episode_disc_reward)
        eval_reward = tf.Variable(0., name="evalReward")
        eval_reward_op = tf.summary.scalar("Eval-Reward", eval_reward)
        eval_disc_reward = tf.Variable(0., name="evalDiscReward")
        eval_disc_reward_op = tf.summary.scalar("Eval-Reward_discounted",
                                                eval_disc_reward)
        eval_sum_vars = [eval_reward, eval_disc_reward]
        eval_sum_op = tf.summary.merge([eval_reward_op, eval_disc_reward_op])
        episode_ave_max_q = tf.Variable(0., name='epsideAvgMaxQ')
        sum2 = tf.summary.scalar("Qmax_Value", episode_ave_max_q)
        sigmaFinal = tf.Variable(0., name="sigmaFinal")
        sum3 = tf.summary.scalar("SigmaFinal", sigmaFinal)
        sigmaDiff = tf.Variable(0., name="sigmaDiff")
        sum4 = tf.summary.scalar("SigmaDiff", sigmaDiff)
        stepCount = tf.Variable(0., name="stepCount")
        sum5 = tf.summary.scalar("StepCount", stepCount)

        self.histoTest = tf.placeholder(tf.int32,
                                        shape=[None, 1],
                                        name='histoTest')
        self.sum6 = tf.summary.histogram("histoTest", self.histoTest)

        summary_vars = [episode_reward, episode_disc_reward, episode_ave_max_q,
                        sigmaFinal, sigmaDiff, stepCount]
        summary_ops = tf.summary.merge([sum1a, sum1b, sum2, sum3, sum4, sum5])
        self.writer = tf.summary.FileWriter(self.out_dir+"/train",
                                            self.sess.graph)
        action_step = tf.Variable(0, name='action_step',
                                      trainable=False, dtype=tf.int32)
        increment_ac_step_op = tf.assign(action_step, action_step+1)

        self.state_summaries_vars = []
        self.state_summaries_ops = []
        for i in range(100):
            varI = tf.Variable(0., name="state_" + str(i))
            self.state_summaries_vars.append(varI)
            sumI = tf.summary.scalar("state_" + str(i), varI)
            self.state_summaries_ops.append(sumI)
        # self.state_summaries_ops = tf.summary.merge(self.state_summaries_ops)

        self.sess.run(tf.initialize_all_variables())
        self.sess.graph.finalize()

        self.maxEpisodes = self.params['numEpochs']
        self.miniBatchSize = self.params['miniBatchSize']
        self.gamma = self.params['gamma']
        self.startLearning = self.params['startLearning']
        if os.environ['SLURM_JOB_NAME'] == 'zsh':
            self.startLearning = 1

        self.startEpsilon = 1.0
        self.endEpsilon = 0.01
        self.epsilon = self.startEpsilon

        if self.params['async']:
            t = threading.Thread(target=self.learnWrap)
            t.daemon = True
            t.start()

        acs = sess.run(action_step)
        ac = acs
        evalEpReward = 0
        evalEpDiscReward = 0
        evalIntv = 10000
        evalCnt = 10000
        for e in range(self.maxEpisodes):
            if e % (evalIntv+evalCnt) >= evalIntv:
                self.evalEp = True
                printT("Eval Episode!")
                if e % (evalIntv+evalCnt) == evalIntv:
                    printT("Start Eval Episodes!")
                    self.evalSigma = random.randint(0, 99)
            else:
                self.evalEp = False

            if (not self.evalEp):
                # rawState = self.reset()
                rawState = self.reset(sigma=self.params['initSigma'])
            else:
                rawState = self.reset(sigma=75)

            newState = self.simulator.getRes(rawState)
            newSigma = self.simulator.sigma
            sigmaStart = newSigma
            terminal = False
            ep_reward = 0
            ep_disc_reward = 0
            ep_ave_max_q = 0

            step = 0
            while not terminal:
                if self.params['async']:
                    if not t.isAlive():
                        printT("alive {}".format(t.isAlive()))
                        printT("Exception in user code:")
                        printT('-'*60)
                        traceback.print_exc(file=sys.stdout)
                        printT('-'*60)
                        sys.stdout.flush()
                        t.join(timeout=None)
                        os._exit(-1)

                state = newState
                sigma = newSigma

                if self.params['useVGG']:
                    stateScaled = scipy.ndimage.zoom(state,
                                                     (1,
                                                      224.0/500.0,
                                                      224.0/500.0,
                                                      1),
                                                     order=1)
                    stateScaled = preprocess_input(np.repeat(
                        stateScaled*256.0, 3,
                        axis=3))
                elif self.params['smallNN']:
                    stateScaled = np.array(float(int(sigma)))
                    stateScaled.shape = (1, 1)
                else:
                    stateScaled = scipy.ndimage.zoom(state,
                                                     (1, 0.1, 0.1, 1),
                                                     order=1)
                step += 1
                sess.run(increment_ac_step_op)
                ac += 1
                tmp_step = min(ac, self.annealSteps)
                self.epsilon = (self.startEpsilon - self.endEpsilon) * \
                               (1 - tmp_step / self.annealSteps) + \
                               self.endEpsilon
                if (not self.evalEp) and \
                   (e < self.params['randomEps'] or \
                    np.random.rand() < self.epsilon):
                    action = (np.random.rand() * 10.0) - 5.0
                    # printT("\nStep: {} Next action (e-greedy {}): {}".format(
                    #     ac,
                    #     self.epsilon,
                    #     action))
                else:
                    action = self.getActions(stateScaled)
                    # printT("\nStep:{} Next action: {}".format(ac, action))

                terminal, newState, newSigma = \
                    self.simulator.act(rawState, action,
                                       factor=0.7,
                                       penalty=self.params['penaltyNeg'])

                if step == self.params['stepsTillTerm']:
                    terminal = True

                if self.params['useVGG']:
                    newStateScaled = scipy.ndimage.zoom(newState,
                                                        (1,
                                                         224.0/500.0,
                                                         224.0/500.0,
                                                         1),
                                                        order=1)
                    newStateScaled = preprocess_input(
                        np.repeat(newStateScaled*256.0, 3,
                                  axis=3))
                elif self.params['smallNN']:
                    newStateScaled = np.array(newSigma)
                    newStateScaled.shape = (1, 1)
                else:
                    newStateScaled = scipy.ndimage.zoom(newState,
                                                        (1, 0.1, 0.1, 1),
                                                        order=1)
                reward, terminal = self.getReward(stateScaled, newStateScaled,
                                                  terminal, action,
                                                  sigma, newSigma)

                ep_disc_reward += pow(self.gamma, step-1) * reward
                ep_reward += reward

                if not self.evalEp:
                    self.insertSamples(stateScaled, action, reward, terminal,
                                       newStateScaled, sigma, newSigma)

                if not self.params['async']:
                    self.learn()

                sys.stdout.flush()
                if self.params['sleep'] and \
                   self.params['async'] and \
                   (self.replay.size() > self.startLearning) and \
                   (self.replay.size() > self.miniBatchSize):
                    time.sleep(5)

            sigmaEnd = sigma
            if self.params['stepsTillTerm'] == 1:
                sigmaEnd = newSigma
            if self.evalEp:
                evalEpReward += ep_reward
                evalEpDiscReward += ep_disc_reward
                if e % (evalIntv+evalCnt) == (evalIntv+evalCnt-1):
                    summary_str = self.sess.run(eval_sum_op, feed_dict={
                        eval_sum_vars[0]: evalEpReward/float(evalCnt),
                        eval_sum_vars[1]: evalEpDiscReward/float(evalCnt)
                    })
                    self.writer.add_summary(summary_str,
                                            int((e+1)/(evalIntv+evalCnt)))
                    evalEpReward = 0.0
                    evalEpDiscReward = 0.0
                    printT("avg disc reward: {}, avg reward: {} <<<<----------------------------------".format(ep_disc_reward, ep_reward))

            printT("step count: {}".format(step))
            summary_str = self.sess.run(summary_ops, feed_dict={
                summary_vars[0]: ep_reward,
                summary_vars[1]: ep_disc_reward,
                summary_vars[2]: ep_ave_max_q / float(step),
                summary_vars[3]: sigmaEnd,
                summary_vars[4]: sigmaEnd - sigmaStart,
                summary_vars[5]: step
            })
            self.writer.add_summary(summary_str, e)
            self.writer.flush()
            # printT('| Reward: {}, | Episode {}, | Qmax: {}\n\n'.
            #       format(ep_reward, e,
            #              ep_ave_max_q / float(step)))
            printT('Time: {} | Reward: {} | Discounted Reward: {} | Episode {}\n'.
                  format(time.ctime(), ep_reward, ep_disc_reward, e))
            modelStoreIntv = 250
            if self.params['tabular']:
                modelStoreIntv = 50000
            if ((e+1) % modelStoreIntv) == 0:
                save_path = self.saver.save(self.sess,
                                            self.out_dir + "/model.ckpt",
                                            global_step=self.global_step)
                self.replay.dump(os.path.join(self.out_dir,
                                              "replayBuffer.pickle"))
                printT("Model saved in file: {}".format(save_path))

            if ((e+1) % 1000) == 0:
                for i in range(100):
                    printT("state: {}, sampled: {}".format(i, self.sampleCntr[i]))
                if self.params['tabular']:
                    self.q.print_stateCnt()
            sys.stdout.flush()

        if ((e+1) % 1000) != 0:
            for i in range(100):
                printT("state: {}, sampled: {}".format(i, self.sampleCntr[i]))
            if self.params['tabular']:
                self.q.print_stateCnt()
        sys.stdout.flush()

    def learnWrap(self):
        try:
            self.learn()
        except:
            printT("learn wrap failed")
            printT("Exception in user code:")
            printT('-'*60)
            traceback.print_exc(file=sys.stdout)
            printT('-'*60)
            sys.stdout.flush()
            os._exit(-1)

    def learn(self):
        while True:
            if self.replay.size() < self.startLearning or \
               self.replay.size() < self.miniBatchSize or \
               self.evalEp:
                if self.params['async']:
                    time.sleep(5.0)
                    continue
                else:
                    return

            self.lock.acquire()
            if self.params['prioritized']:
                ids, w_batch, s_batch, a_batch, r_batch, t_batch,\
                    ns_batch, sig_batch, sig2_batch, ps_batch = \
                    self.replay.sample_batch(self.miniBatchSize)
                # printT(ids)
            else:
                s_batch, a_batch, r_batch, t_batch, ns_batch, sig_batch, sig2_batch = \
                    self.replay.sample_batch(self.miniBatchSize)
            self.lock.release()

            for b in range(self.miniBatchSize):
                self.sampleCntr[int(sig_batch[b])] += 1

            if self.params['smallNN']:
                s_batch.shape = (s_batch.shape[0], 1)
                ns_batch.shape = (ns_batch.shape[0], 1)

            qValsNewState = self.predict_target_nn(ns_batch)
            y_batch = np.zeros((self.miniBatchSize, 1))
            if self.params['importanceSampling']:
                wMax = np.max(w_batch)
                for i in range(self.miniBatchSize):
                    if t_batch[i]:
                        y_batch[i] = w_batch[i] / wMax * r_batch[i]
                    else:
                        y_batch[i] = w_batch[i] / wMax * \
                            (r_batch[i] + self.gamma * qValsNewState[i])
            else:
                for i in range(self.miniBatchSize):
                    if t_batch[i]:
                        y_batch[i] = r_batch[i]
                    else:
                        y_batch[i] = r_batch[i] + self.gamma * qValsNewState[i]

            gS, qs, delta = self.update(s_batch, a_batch, y_batch)
            if self.params['prioritized']:
                self.lock.acquire()
                for i in range(self.miniBatchSize):
                    self.replay.update(ids[i], abs(delta[i]))
                self.lock.release()

            # ep_ave_max_q += np.amax(qs)

            self.update_targets()

            if self.params['smallNN']:
                intv = 1000
            else:
                intv = 100
            if (gS+1) % intv == 0:
                if (gS+1) % (intv*10) == 0:
                    if os.environ['SLURM_JOB_NAME'] != 'zsh':
                        shutil.copy2(os.path.join(self.out_dir, "log"),
                                     os.path.join(self.out_dir, "log-" +
                                                  str(gS)))
                    # else:
                    #     shutil.copy2(os.path.join(
                    #         "/scratch/s7550245/simulateSTM/dqn/",
                    #         "log." + os.environ['SLURM_JOBID']),
                    #         os.path.join(self.out_dir, "log-" +
                    #                      str(int(time.time()))))

                # qValsNewState = self.estimate_ddqn(ns_batch,p=True)
                if self.params['prioritized']:
                    for i in range(self.miniBatchSize):
                        printT("{}, {}, {}, {}, {}, {}".format(
                            sig_batch[i][0], sig2_batch[i][0],
                            a_batch[i][0], y_batch[i][0],
                            r_batch[i][0], qValsNewState[i]))
                else:
                    for i in range(self.miniBatchSize):
                        printT("{}, {}, {}, {}, {}, {}".format(
                            sig_batch[i][0], sig2_batch[i][0],
                            a_batch[i][0], y_batch[i][0],
                            r_batch[i][0], qValsNewState[i]))
                # histo = []
                if self.params['smallNN']:
                    if self.params['prioritized']:
                        printT("replay: alpha {}, beta {} (sumP {}, total {})".format(
                            self.replay.alpha,
                            self.replay.beta,
                            self.replay.sumP,
                            self.replay.total()))
                    s = np.zeros((1, 1))
                    a = np.zeros((1, 1))
                    for i in range(100):
                        s[[0]] = i
                        aMax = self.actor.run_predict(s)
                        qs = self.critic.run_predict(s, aMax)
                        printT("state: {}, actionID: {}, value: {}".format(
                            i, aMax[0][0], qs[0][0]))

                        for i in range(11):
                            a[[0]] = float(i) - 5
                            qs = self.critic.run_predict(s, a)
                            if i-5 == int(aMax):
                                printT("{}: {} <-- {}".format(i-5, qs[0][0], i-5))
                            else:
                                printT("{}: {}".format(i-5, qs[0][0]))
                        summary_str = self.sess.run(
                            self.state_summaries_ops[int(s)],
                            feed_dict={
                                self.state_summaries_vars[int(s)]:
                                qs[0][0]
                            })
                        self.stateSteps[int(s), 0] += 1
                        self.writer.add_summary(summary_str,
                                                self.stateSteps[int(s), 0])
                # else:
                #     numBatches = 5
                #     for b in range(numBatches):
                #         self.lock.acquire()
                #         if self.params['prioritized']:
                #             ids, w_batch, s_batch, a_batch,\
                #                 r_batch, t_batch, ns_batch, \
                #                 sig_batch, sig2_batch = \
                #                 self.replay.sample_batch(self.miniBatchSize)
                #         else:
                #             s_batch, a_batch, r_batch, t_batch,\
                #                 ns_batch, sig_batch, sig2_batch = \
                #                 self.replay.sample_batch(self.miniBatchSize)
                #         self.lock.release()
                #         qs = self.predict_nn(s_batch)
                #         # if self.params['reward'] == 'term':
                #         #     qs = np.append(
                #         #         qs,
                #         #         (100 - np.minimum(sig_batch, 100)) * 0.1,
                #         #         axis=1)
                #         aID = np.argmax(qs, axis=1)
                #         for i in range(self.miniBatchSize):
                #             state = sig_batch[i][0]
                #             print("state: {}, actionID: {}".format(
                #                 state, aID[i]))
                #             for j in range(len(qs[i])):
                #                 if j == aID[i]:
                #                     print("{}: {} <-- {}".format(
                #                         j, qs[i][j], j))
                #                 else:
                #                     print("{}: {}".format(
                #                         j, qs[i][j]))
                #             summary_str = self.sess.run(
                #                 self.state_summaries_ops[int(state)],
                #                 feed_dict={
                #                     self.state_summaries_vars[int(state)]:
                #                     qs[i][aID[i]]
                #                 })
                #             self.writer.add_summary(summary_str)


                #     for a in range(aID):
                #         histo.append(i)
                # histo = np.array(histo)
                # histo.shape = (histo.shape[0], 1)
                # summary_str = self.sess.run(self.sum6, feed_dict={
                #     self.histoTest: histo})
                # self.writer.add_summary(summary_str, int(gS/100.0))
                # self.writer.flush()


            if not self.params['async']:
                return

    def getActions(self, state):
        a = self.actor.run_predict(state)
        return a

    def getReward(self, state, newState, terminal, action, sigma, newSigma):
        if self.params['reward'] == 'single':
            if terminal:
                if sigma < 5.0:
                    reward = 1.0
                else:
                    reward = -1.0
            else:
                reward = 0.0
            printT("reward (single): {}".format(reward))
        elif self.params['reward'] == 'net':
            res1, res2 = self.rewardNet.runPrediction(state, newState)
            reward = res1 - res2
            printT("reward (net): {} - {} = {}".format(res1, res2,
                                                       reward))
        elif self.params['reward'] == 'state':
            reward = newSigma * (-1.0) * 0.1
            printT("reward (state): {}".format(reward))
        elif self.params['reward'] == 'binary':
            if newSigma < sigma:
                reward = self.params['rewardPos']
            elif newSigma >= sigma:
                reward = self.params['rewardNeg']
            # else:
            #     reward = 0.0

            if sigma <= 5:
                terminal = True
                reward = self.params['rewardFinal']
            # printT("reward (binary): {}".format(reward))
        elif self.params['reward'] == 'term':
            if action == 10 or sigma <= 5:
                reward = (100 - min(sigma, 100)) * 0.1
                terminal = True
            else:
                reward = 0
        else:
            reward = (sigma - newSigma) * 0.1
            if sigma <= 5:
                terminal = True
                # reward = (5.0-sigma) * 0.1
                reward = 10.0
            printT("reward (none): {}".format(reward))

        return reward, terminal

    def update(self, states, actions, targets):
        step, out, delta = self.critic.run_train(states, actions, targets)
        ac = self.actor.run_predict(states)

        a_grad = self.critic.run_get_action_gradients(states, ac)
        self.actor.run_train(states, a_grad, step)
        return step, out, delta

    def update_targets(self):
        self.critic.run_update_target_nn()
        self.actor.run_update_target_nn()

    def predict_target_nn(self, state):
        a = self.actor.run_predict_target(state)
        return self.critic.run_predict_target(state, a)

    def predict_nn(self, state):
        a = self.actor.run_predict(state)
        return self.critic.run_predict(state, a)

    def reset(self, rand=True, sigma=None):
        if rand:
            self.c = random.randrange(0, len(self.imgs))
        img = np.load(self.imgs[self.c])
        # printT("\nLoading image {}".format(self.imgs[self.c]))
        img.shape = (1, img.shape[0], img.shape[1], 1)
        self.simulator.reset(sigma=sigma)
        return img

    def insertSamples(self, stateScaled, action, reward, terminal,
                      newStateScaled, sigma, newSigma):
        if not self.params['smallNN']:
            stateScaled.shape = (stateScaled.shape[1],
                                 stateScaled.shape[2],
                                 stateScaled.shape[3])
            newStateScaled.shape = (newStateScaled.shape[1],
                                    newStateScaled.shape[2],
                                    newStateScaled.shape[3])

        self.lock.acquire()
        if self.params['prioritized']:
            self.replay.add(None,
                            (stateScaled, sigma, action, reward, terminal,
                             newStateScaled, newSigma))
            if not self.params['smallNN']:
                self.replay.add(
                    None,
                    (np.ascontiguousarray(np.rot90(stateScaled, 2)),
                     sigma, action, reward, terminal,
                     np.ascontiguousarray(np.rot90(newStateScaled, 2)),
                     newSigma))
                self.replay.add(
                    None,
                    (np.ascontiguousarray(np.fliplr(stateScaled)),
                     sigma, action, reward, terminal,
                     np.ascontiguousarray(np.fliplr(newStateScaled)),
                     newSigma))
                self.replay.add(
                    None,
                    (np.ascontiguousarray(np.flipud(stateScaled)),
                     sigma, action, reward, terminal,
                     np.ascontiguousarray(np.flipud(newStateScaled)),
                     newSigma))
        else:
            self.replay.add(stateScaled, sigma, action, reward, terminal,
                            newStateScaled, newSigma)
            if not self.params['smallNN']:
                self.replay.add(
                    np.ascontiguousarray(np.rot90(stateScaled, 2)),
                    sigma, action, reward, terminal,
                    np.ascontiguousarray(np.rot90(newStateScaled, 2)),
                    newSigma)
                self.replay.add(
                    np.ascontiguousarray(np.fliplr(stateScaled)),
                    sigma, action, reward, terminal,
                    np.ascontiguousarray(np.fliplr(newStateScaled)),
                    newSigma)
                self.replay.add(
                    np.ascontiguousarray(np.flipud(stateScaled)),
                    sigma, action, reward, terminal,
                    np.ascontiguousarray(np.flipud(newStateScaled)),
                    newSigma)
        self.lock.release()

    def loadVGG(self):
        self.params['state_dim'] = 224
        self.params['col_channels'] = 3
        self.images = tf.placeholder(
            tf.float32,
            shape=[None,
                   224,
                   224,
                   3],
            name='input')

        # varTmp = tf.get_variable("tmp", shape=[1,1])
        self.vggsaver = tf.train.import_meta_graph(
            '/home/s7550245/vggFcNet/vgg-model.meta',
            import_scope="VGG",
            clear_devices=True,
            input_map={'input_1':self.images})

        _VARSTORE_KEY = ("__variable_store",)
        varstore = ops.get_collection(_VARSTORE_KEY)[0]
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="VGG")

        for v in variables:
            varstore._vars[v.name.split(":")[0]] = v


params = parseNNArgs.parse(sys.argv[1:])

print(params, params['version'])
timestamp = str(int(time.time()))
jobid = os.environ['SLURM_JOBID']
out_dir = os.path.abspath(os.path.join(
    "/scratch/s7550245/simulateSTM/ddpg/runs",
    params['version'], jobid + "_" + timestamp))

print("Number of epochs: ", params['numEpochs'])
out_dir += "_ep" + str(params['numEpochs'])

print("Number of random epochs: ", params['randomEps'])
out_dir += "_randEps" + str(params['randomEps'])

print("miniBatchSize: ", params['miniBatchSize'])
out_dir += "_batch" + str(params['miniBatchSize'])

print("stepstillterm: ", params['stepsTillTerm'])
out_dir += "_stt" + str(params['stepsTillTerm'])

print("usevgg", params['useVGG'])
if params['useVGG']:
    out_dir += "_" + "VGG" + str(params['top'])

print("gamma: ", params['gamma'])
out_dir += "_gamma" + str(params['gamma'])

if params['discountReward']:
    out_dir += "_discReward"

print("dropout", params['dropout'])
if params['dropout']:
    out_dir += "_" + "dropout" + str(params['dropout'])
else:
    out_dir += "_" + "noDropout"

print("tau", params['tau'])
if params['tau']:
    out_dir += "_" + "tau-" + str(params['tau'])

print("batchnorm", params['batchnorm'])
if params['batchnorm']:
    out_dir += "_" + "batchnorm-" + str(params['batchnorm-decay'])
else:
    out_dir += "_" + "noBatchnorm"

print("weight decayActor", params['weight-decayActor'])
out_dir += "_wdA" + str(params['weight-decayActor'])
print("weight decayCritic", params['weight-decayCritic'])
out_dir += "_wdC" + str(params['weight-decayCritic'])

print("learning rateActor", params['learning-rateActor'])
out_dir += "_lrA" + str(params['learning-rateActor'])
print("learning rateCritic", params['learning-rateCritic'])
out_dir += "_lrC" + str(params['learning-rateCritic'])

print("momentumActor", params['momentumActor'])
out_dir += "_momA" + str(params['momentumActor'])
print("momentumCritic", params['momentumCritic'])
out_dir += "_momC" + str(params['momentumCritic'])

print("optimizerActor", params['optimizerActor'])
out_dir += "_optA" + params['optimizerActor']
print("optimizerCritic", params['optimizerCritic'])
out_dir += "_optC" + params['optimizerCritic']

print("reward", params['reward'])
if params['reward']:
    out_dir += "_" + "reward-" + params['reward']
    if params['rewardPos'] is not None:
        out_dir += "_" + str(params['rewardPos'])
    if params['rewardNeg'] is not None:
        out_dir += "_" + str(params['rewardNeg'])
    if params['rewardFinal'] is not None:
        out_dir += "_" + str(params['rewardFinal'])
    if params['penaltyNeg'] is not None:
        out_dir += "_" + str(params['penaltyNeg'])

print("numActions", params['numActions'])
if params['numActions']:
    out_dir += "_" + "numActions-" + params['numActions']

if params['prioritized']:
    out_dir += "_" + "prio"

if params['async']:
    out_dir += "_" + "async"

if params['importanceSampling']:
    out_dir += "_" + "impSmpl"

if params['sleep']:
    out_dir += "_" + "sleep"

if params['smallNN']:
    out_dir += "_" + "smallNN"

if params['tabular']:
    out_dir += "_" + "tabular"

if params['initSigma']:
    out_dir += "_" + "sig" + str(params['initSigma'])

with tf.Session() as sess:
    rl = ddpgRunner(sess, params, out_dir=out_dir)
    rl.run()
