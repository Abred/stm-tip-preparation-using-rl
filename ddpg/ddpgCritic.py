import numpy as np
import os
import shutil
import sys
import time

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim

import tfutils


class Critic:
    O = 1

    Hconv1 = 64
    Hconv2 = 64
    HFC = [256, 256]
    HFCSmall = [128, 128, 128]
    HFCVGG = [2048, 2048]

    tau = 0.001
    state_dim = 50
    col_channels = 1
    actions_dim = 1

    def __init__(self, sess, out_dir, glStep, params,
                 inputs=None):
        self.sess = sess
        self.params = params
        self.summaries = []
        self.weight_summaries = []
        self.out_dir = out_dir
        self.mbs = params['miniBatchSize']
        if params['batchnorm']:
            # self.batchnorm = slim.batch_norm
            self.batchnorm = tfutils.batch_normBUG
        else:
            self.batchnorm = None

        self.dropout = params['dropout']
        self.weightDecay = params['weight-decayCritic']
        self.learning_rate = params['learning-rateCritic']
        self.momentum = params['momentumCritic']
        self.opti = params['optimizerCritic']
        self.bnDecay = params['batchnorm-decay']
        self.useVGG = params['useVGG']
        self.tau = params['tau']

        if params['state_dim'] is not None:
            self.state_dim = params['state_dim']
        if params['col_channels'] is not None:
            self.col_channels = params['col_channels']

        if params['useVGG']:
            self.top = params['top']
            if self.top == 7:
                self.pretrained = sess.graph.get_tensor_by_name(
                    "VGG/MaxPool_2:0")
            elif self.top == 10:
                self.pretrained = sess.graph.get_tensor_by_name(
                    "VGG/MaxPool_3:0")
            elif self.top == 13:
                self.pretrained = sess.graph.get_tensor_by_name(
                    "VGG/MaxPool_4:0")
            print(self.pretrained)
            self.pretrained = tf.stop_gradient(self.pretrained)
            self.images = inputs

        print("dropout critic", self.dropout)

        self.global_step = glStep

        with tf.variable_scope('Critic'):
            self.keep_prob = tf.placeholder(tf.float32)
            self.isTraining = tf.placeholder(tf.bool)

            # Critic Network
            self.setNN()

            self.loss_op = self.define_loss()
            self.train_op = self.defineTraining()

            self.action_grads = self.define_action_grad()
            self.grads = self.define_grads()


        _VARSTORE_KEY = ("__variable_store",)
        varstore = ops.get_collection(_VARSTORE_KEY)[0]
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="Critic")

        for v in variables:
            print(v.name)
            if v.name.endswith("weights:0") or \
               v.name.endswith("biases:0"):
                s = []
                var = v
                mean = tf.reduce_mean(var)
                s.append(tf.summary.scalar(v.name+'mean', mean))
                with tf.name_scope('stddev'):
                    stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
                s.append(tf.summary.scalar(v.name+'stddev', stddev))
                s.append(tf.summary.scalar(v.name+'max', tf.reduce_max(var)))
                s.append(tf.summary.scalar(v.name+'min', tf.reduce_min(var)))
                s.append(tf.summary.histogram(v.name+'histogram', var))

                self.weight_summaries += s
        self.summary_op = tf.summary.merge(self.summaries)
        self.weight_summary_op = tf.summary.merge(self.weight_summaries)
        self.writer = tf.summary.FileWriter(out_dir, sess.graph)

    def setNN(self):
        prevTrainVarCount = len(tf.trainable_variables())

        if self.useVGG:
            defNN = self.defineNNVGG
        else:
            if self.params['smallNN']:
                defNN = self.defineNNSmall
            else:
                defNN = self.defineNN

        self.input_pl, self.actions_pl, self.nn = defNN()
        self.nn_params = tf.trainable_variables()[prevTrainVarCount:]

        # Target Network
        with tf.variable_scope('target'):
            prevTrainVarCount = len(tf.trainable_variables())
            self.target_input_pl, self.target_actions_pl, self.target_nn =\
                defNN(isTargetNN=True)
            self.target_nn_params = \
                tf.trainable_variables()[prevTrainVarCount:]
            with tf.variable_scope('init'):
                for i in range(len(self.nn_params)):
                    tf.Variable.assign(
                        self.target_nn_params[i],
                        self.nn_params[i].initialized_value())
            self.target_nn_update_op = self.define_update_target_nn_op()

    def defineNNVGG(self, isTargetNN=False):
        with tf.variable_scope('inf'):
            print("defining vgg net")
            net = self.pretrained

            actions = tf.placeholder(tf.float32,
                                     shape=[None, self.actions_dim],
                                     name='ActorActions')

            with slim.arg_scope(
                [slim.fully_connected],
                activation_fn=tf.nn.relu,
                weights_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                normalizer_fn=self.batchnorm,
                normalizer_params={
                    'fused': True,
                    'is_training': self.isTraining,
                    'updates_collections': None,
                    'decay': self.bnDecay,
                    'scale': True}
            ):

                if self.top == 1:
                    numPool = 0
                    self.HFCVGG = [256, 256]
                    H = 64
                elif self.top <= 2:
                    numPool = 1
                    H = 64
                    self.HFCVGG = [256, 256]
                elif self.top <= 4:
                    numPool = 2
                    H = 128
                    self.HFCVGG = [512, 512]
                elif self.top <= 7:
                    numPool = 3
                    H = 256
                    self.HFCVGG = [1024, 1024]
                elif self.top <= 10:
                    numPool = 4
                    H = 512
                    self.HFCVGG = [2048, 2048]
                elif self.top <= 13:
                    numPool = 5
                    H = 512
                    self.HFCVGG = [2048, 2048]

                remSz = int(self.state_dim / 2**numPool)
                print(remSz, net,H)
                net = tf.concat(
                    [tf.reshape(net, [-1, remSz*remSz*H],
                                name='flatten'),
                     actions], 1)

                for i in range(len(self.HFC)):
                    net = slim.fully_connected(net, 256,
                                               scope='fc' + str(i))
                    print(net)
                    if self.dropout:
                        net = slim.dropout(net,
                                           keep_prob=self.dropout,
                                           is_training=self.isTraining)
                        print(net)
                net = slim.fully_connected(net, self.O, activation_fn=None,
                                           scope='out')
                print(net)

                if not isTargetNN:
                    self.weight_summaries += [tf.summary.histogram('output', net)]
        return self.images, actions, net

    def defineNN(self, isTargetNN=False):
        with tf.variable_scope('inf'):
            images = tf.placeholder(
                tf.float32,
                shape=[None,
                       self.state_dim,
                       self.state_dim,
                       self.col_channels],
                name='input')
            actions = tf.placeholder(tf.float32,
                                     shape=[None, self.actions_dim],
                                     name='ActorActions')

            net = images
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True)):
                with slim.arg_scope([slim.conv2d], stride=1, padding='SAME'):
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv1,
                                      [3, 3], scope='conv1')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool1')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv2,
                                      [3, 3], scope='conv2')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool2')
                    print(net)

                remSz = int(self.state_dim / 2**2)
                net = tf.concat(
                    [tf.reshape(net, [-1, remSz*remSz*self.Hconv2],
                                name='flatten'),
                     actions], 1)

                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'decay': self.bnDecay,
                                        'scale': True}):
                    for i in range(len(self.HFC)):
                        net = slim.fully_connected(net, self.HFC[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.dropout,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)
                # net = tf.sigmoid(net)

                if not isTargetNN:
                    self.weight_summaries += [tf.summary.histogram('output', net)]

        return images, actions, net

    def defineNNSmall(self, isTargetNN=False):
        with tf.variable_scope('inf'):
            images = tf.placeholder(
                tf.float32,
                shape=[None, 1],
                name='input')

            # net = tf.div(images, 100.0)
            net = images
            # images = tf.placeholder(
            #     tf.int32,
            #     shape=[None, 1],
            #     name='input')
            # net = tf.minimum(99, images)
            # print(net)
            # net = tf.one_hot(net, 100)
            # print(net)
            # net = tf.to_float(net)
            # print(net)
            # net = tf.reshape(net, [-1, 100])
            actions = tf.placeholder(tf.float32,
                                     shape=[None, self.actions_dim],
                                     name='ActorActions')

            net = tf.concat([net, actions], 1)

            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=
                    tf.zeros_initializer(),
                    # tf.contrib.layers.variance_scaling_initializer(
                    #     factor=1.0, mode='FAN_AVG', uniform=True),
                normalizer_fn=self.batchnorm,
                    normalizer_params={
                        'fused': True,
                        'is_training': self.isTraining,
                        'updates_collections': None,
                        'decay': self.bnDecay,
                        'scale': True}):
                for i in range(len(self.HFCSmall)):
                    net = slim.fully_connected(net, self.HFCSmall[i],
                                               scope='fc' + str(i))
                    if not isTargetNN:
                        self.weight_summaries += [tf.summary.histogram(
                            'fc' + str(i),
                            net)]
                    print(net)
                    if self.dropout:
                        net = slim.dropout(net, self.dropout)
                        print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)
                # net = tf.sigmoid(net)

                if not isTargetNN:
                    self.weight_summaries += [tf.summary.histogram('output', net)]

        return images, actions, net

    def define_update_target_nn_op(self):
        with tf.variable_scope('update'):
            tau = tf.constant(self.tau, name='tau')
            invtau = tf.constant(1.0-self.tau, name='invtau')
            return \
                [self.target_nn_params[i].assign(
                    tf.multiply(self.nn_params[i], tau) +
                    tf.multiply(self.target_nn_params[i], invtau))
                 for i in range(len(self.target_nn_params))]

    def define_loss(self):
        with tf.variable_scope('loss2'):
            self.td_targets_pl = tf.placeholder(tf.float32, [None, 1],
                                                name='tdTargets')

            # lossL2 = tfu.mean_squared_diff(self.td_targets_pl, self.nn)
            self.delta = self.td_targets_pl - self.nn
            # Huber loss
            lossL2 = tf.where(tf.abs(self.delta) < 1.0,
                              0.5 * tf.square(self.delta),
                              tf.abs(self.delta) - 0.5, name='clipped_error')
            lossL2 = tf.reduce_mean(lossL2)
            # lossL2 = slim.losses.mean_squared_error(self.td_targets_pl,
                                                    # self.nn)
            lossL2 = tf.Print(lossL2, [lossL2], "lossL2 ", first_n=10)

            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss',
                                      lossL2)]
            regs = []
            for v in self.nn_params:
                if "w" in v.name:
                    regs.append(tf.nn.l2_loss(v))
            lossReg = tf.add_n(regs) * self.weightDecay
            lossReg = tf.Print(lossReg, [lossReg], "regLoss ", first_n=10)
            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss_reg',
                                      lossReg)]

            loss = lossL2 + lossReg
            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss_with_reg',
                                      loss)]

        return loss

    def defineTraining(self, conv=False):
        with tf.variable_scope('train'):
            learning_rate = self.learning_rate
            if self.params['lr-decay']:
                learning_rate = tf.train.exponential_decay(
                    self.learning_rate, self.global_step,
                    10000, 0.7, staircase=False)
            momentum = self.momentum
            if self.params['mom-decay']:
                momentum = 1.0 - tf.train.exponential_decay(
                    1.0 - self.momentum, self.global_step,
                    10000, 0.7, staircase=False)
            if self.opti == 'momentum':
                optimizer = tf.train.MomentumOptimizer(self.learning_rate,
                                                       self.momentum)
            elif self.opti == 'adam':
                optimizer = tf.train.AdamOptimizer(self.learning_rate)
            elif self.opti == 'sgd':
                optimizer = tf.train.GradientDescentOptimizer(
                    self.learning_rate)

            print(optimizer)
            return optimizer.minimize(self.loss_op,
                                      global_step=self.global_step)

    def define_action_grad(self):
        with tf.variable_scope('getActionGradient'):
            # mbs = self.params['miniBatchSize']
            # nn = self.nn
            # nn = tf.Print(nn, [nn], "nn ")
            # out_batch = tf.split(0, mbs, nn)
            # act_batch = tf.split(0, mbs, self.actions_pl)
            # print(out_batch, act_batch)
            # grads = []
            # for i in range(mbs):
            #     g = tf.gradients(out_batch[i], act_batch[i])
            #     print(g)
            #     grads.append(g)
            # return grads

            # tf.gradients returns a list -> [0]
            grads = tf.gradients(self.nn, self.actions_pl)[0]
            print(grads)
            return grads

    def define_grads(self):
        with tf.variable_scope('getGrads'):
            optimizer = tf.train.AdamOptimizer(self.learning_rate, epsilon=0.1)
            return optimizer.compute_gradients(self.loss_op,
                                               var_list=self.nn_params)

    def run_train(self, inputs, actions, targets):
        step = self.sess.run(self.global_step)
        if self.params['smallNN']:
            wSum = 50000
            lSum = 100
        else:
            wSum = 1000
            lSum = 50
        if (step+1) % wSum == 0:
            out, delta, loss, _, summaries = self.sess.run(
                [self.nn,
                 self.delta,
                 self.loss_op,
                 self.train_op,
                 self.weight_summary_op],
                feed_dict={
                    self.input_pl: inputs,
                    self.actions_pl: actions,
                    self.td_targets_pl: targets,
                    self.isTraining: True,
                    self.keep_prob: self.dropout
                })
            self.writer.add_summary(summaries, step)
            self.writer.flush()
        elif (step+1) % lSum == 0:
            out, delta, loss, _, summaries = self.sess.run(
                [self.nn,
                 self.delta,
                 self.loss_op,
                 self.train_op,
                 self.summary_op],
                feed_dict={
                    self.input_pl: inputs,
                    self.actions_pl: actions,
                    self.td_targets_pl: targets,
                    self.isTraining: True,
                    self.keep_prob: self.dropout
                })
            self.writer.add_summary(summaries, step)
            self.writer.flush()
        else:
            out, delta, loss, _ = self.sess.run([self.nn,
                                                 self.delta,
                                                 self.loss_op,
                                                 self.train_op],
                                                feed_dict={
                    self.input_pl: inputs,
                    self.actions_pl: actions,
                    self.td_targets_pl: targets,
                    self.isTraining: True,
                    self.keep_prob: self.dropout
                })
        # print("loss: {}".format(loss))
        if (not self.params['smallNN']) or (step % 100 == 0):
            print("step: {}, loss: {}".format(step, loss))
        return step, out, delta

    def run_predict(self, inputs, action):
        return self.sess.run(self.nn, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: action,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_predict_target(self, inputs, action):
        return self.sess.run(self.target_nn, feed_dict={
            self.target_input_pl: inputs,
            self.target_actions_pl: action,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_get_action_gradients2(self, inputs, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.isTraining: True,
            self.keep_prob: 1.0
        })

    def run_get_action_gradients(self, inputs, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_get_gradients(self, inputs, actions, targets):
        return zip(self.nn_params, self.sess.run(self.grads, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.td_targets_pl: targets,
            self.isTraining: False,
            self.keep_prob: 1.0
        }))

    def run_get_loss(self, inputs, actions, targets):
        return self.sess.run(self.loss_op, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.td_targets_pl: targets,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_update_target_nn(self):
        self.sess.run(self.target_nn_update_op)
