#!/usr/bin/python

import sys
import math
import random
import numpy as np
import scipy.ndimage.filters as spf
from scipy.special import expit
import scipy.signal as sps


def sigmoid(x, factor=1.0):
    "Numerically-stable sigmoid function."
    if x >= 0:
        z = math.exp(-x * factor)
        return 1 / (1 + z)
    else:
        # if x is less than zero then z will be small, denom can't be
        # zero because it's 1+z.
        z = math.exp(x * factor)
        return z / (1 + z)


def printT(s):
    sys.stdout.write(s + '\n')


class STMTip:
    def __init__(self, params, features=None):
        self.params = params
        self.ks = 500
        self.peakMax = params['peakMax']
        self.sigmaMax = params['sigmaMax']
        self.penaltySigma = params['penaltySigma']
        self.radiusMax = params['radiusMax']
        self.penaltyRadius = params['penaltyRadius']

        self.reset(features)

    def act(self, state, voltage, factor=1.0):
        rnd = np.random.rand()
        rnd2 = np.random.rand()
        rnd3 = np.random.rand()

        # score = expit(voltage)
        # prob for improvment
        score = 1.0 - sigmoid(voltage, factor)

        # printT("(features-old, {}".format(self.features))
        # tip kaput
        if rnd < score:
            # printT("(tip improved, {})".format(score))
            if rnd2 > score:
                self.features[0] = max(0, self.features[0])

            if rnd3 < 0.3333334:
                self.features[1] *= score
                self.features[2] *= score
            elif rnd3 < 0.6666666:
                self.features[1] *= score
            else:
                self.features[2] *= score

            self.features[1] = float(int(self.features[1]))
            self.features[2] = float(int(self.features[2]))
        # tip slightly improved
        else:
            # printT("(tip destroyed)")
            if rnd2 > score:
                self.features[0] = min(self.peakMax-1, self.features[0])

            if rnd3 < 0.3333334:
                self.features[1] = \
                    min(self.sigmaMax-1,
                        int(self.features[1] + self.penaltySigma))
                self.features[2] = \
                    min(self.radiusMax-1,
                        self.features[2] + self.penaltyRadius)
            elif rnd3 < 0.6666666:
                self.features[1] = \
                    min(self.sigmaMax-1,
                        int(self.features[1] + self.penaltySigma))
            else:
                self.features[2] = \
                    min(self.radiusMax-1,
                        self.features[2] + self.penaltyRadius)
        # printT("(features-new, {}".format(self.features))
        return False, self.getRes(state), list(self.features)

    def getRes(self, state):
        if self.params['smallNN']:
            return state
        state.shape = (state.shape[1], state.shape[2])
        if not self.params['zoom']:
            kernel = np.zeros((self.ks/10, self.ks/10))
        else:
            kernel = np.zeros((self.ks, self.ks))
        rep = True
        while rep:
            rep = False
            angs = []
            for i in range(self.features[0]+1):
                angs.append(np.random.rand() * 2 * np.pi)
            for i in range(self.features[0]+1):
                for j in range(i+1, self.features[0]+1):
                    if abs(angs[i]-angs[j]) < 2 * np.pi / 10.0:
                        rep = True
            if rep:
                continue
        for j in range(len(angs)):
            radius = pow(self.features[2]+1, 2)
            if not self.params['zoom']:
                radius = radius/10.0
            if radius >= 50:
                printT("radius invalid: {}, {}".format(radius, self.features))

            y = int(np.sin(angs[j]) * radius + kernel.shape[0]/2)
            x = int(np.cos(angs[j]) * radius + kernel.shape[0]/2)
            if j == 0:
                kernel[y, x] = 1.0
            else:
                if kernel[y, x] != 0.0:
                    print("peak same loc")
                kernel[y, x] = 0.334/(len(angs)-1)
            kernel[y, x] = 1.0
        if not self.params['zoom']:
            kernel = spf.gaussian_filter(kernel, (self.features[1]+1)/10.0)
        else:
            kernel = spf.gaussian_filter(kernel, (self.features[1]+1))
        # filteredState = spf.convolve(state, kernel, mode='constant')
        filteredState = sps.fftconvolve(state, kernel, mode='same')
        state.shape = (1, state.shape[0], state.shape[1], 1)
        filteredState.shape = (1,
                               filteredState.shape[0],
                               filteredState.shape[1],
                               1)
        return filteredState

    def reset(self, features=None):
        if features is None:
            # self.features = random.randint(0, 99)
            numPeaks = int(min(max(np.random.exponential(1.5), 1.0),
                               self.peakMax))-1
            sigma = random.randrange(0, self.sigmaMax)
            radius = random.randrange(0, self.radiusMax)
            self.features = [numPeaks, sigma, radius]
        else:
            self.features = features
        self.prevFeatures = self.features
