#include "cnpy.h"
#include <fstream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <numeric>
#include<cstring>
#include <list>
#include <omp.h>
#include <random>
#include <algorithm>
#include <tuple>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <array>
#include <string>
#include <vector>
#include <cmath>


const double pi = std::acos(-1);
int const ks = 500;
// std::array<std::array<float, ks>, ks> kernel;
// std::array<std::array<float, ks>, ks> kernel2;
// std::vector<std::vector<float>> kernel;
// std::vector<std::vector<float>> kernel2;
float const sigmoidFactor = 0.7;
int const numActions = 10;

// float const posReward = 0.5;
// float const negReward = -0.5;
// float const stepReward = -1.5;

float const posReward = -1.0;
float const negReward = -5.0;
float const finReward = 20.0;

int const numIt = 1000;

float const featCnt = 3;
int const peakMax = 3;
int const peakLimit = 1;
int const sigmaMax = 35;
int const sigmaLimit = 4;
int const radiusMax = 11;
int const radiusLimit = 2;

int const penaltySigma = 3;
int const penaltyRadius = 2;

float peakProb = 0.1;
float sigmaProb = 0.45;
float radiusProb = 0.45;

float const gammaV = 0.95;

int const ld = 4;

std::array<std::array<std::array<float, radiusMax>, sigmaMax>, peakMax> values{0};
std::array<std::array<std::array<float, radiusMax>, sigmaMax>, peakMax> valuesB{0};
std::array<std::array<std::array<std::array<float, numActions>, radiusMax>, sigmaMax>, peakMax> qvalues{0};

std::array<std::array<std::array<float, radiusMax>, sigmaMax>, peakMax> stateQuality{0.0f};

std::array<float, numActions> actions {0};
std::array<float, numActions> actionProbs {0};
std::array<std::array<std::array<int, radiusMax>, sigmaMax>, peakMax> maxA{0};

std::array<std::array<std::array<std::array<std::array<std::array<std::array<std::array<float, ld>, numActions>, radiusMax>, sigmaMax>, peakMax>, radiusMax>, sigmaMax>, peakMax> transitions;


std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dis(0, 1);

float sigmoid(float x, float factor)
{
	if (x >= 0) {
		float z = std::exp(-x * factor);
		return 1.0 / (1.0 + z);
	}
	else {
		float z = std::exp(x * factor);
		return z / (1.0 + z);
	}
}

float actProb(float a)
{
	// return 1.0 - (a+1)/(numActions+1);
	return 1.0 - sigmoid(a, sigmoidFactor);
}

int getActForID(int a)
{
	// return a;
	return int(a - ((numActions/2) - 1));
}

void initValues()
{
	for (int i = 0; i < peakMax; i++) {
		for (int j = 0; j < sigmaMax; j++) {
			for (int k = 0; k < radiusMax; k++) {
				if ((i < peakLimit || k < radiusLimit) &&
				    j < sigmaLimit) {
					values[i][j][k] = finReward;
				}
				else {
					values[i][j][k] = std::rand() % 20 - 10;
				// values[i][j][k] = 999.0f;
				}
				// values[0][0][0] = 0.0;
				valuesB[i][j][k] = values[i][j][k];
			}
		}
	}
}

void setActions()
{
	std::cout << "action probs" << std::endl;
	for (int i = 0; i < numActions; i++) {
		actions[i] = getActForID(i);
		actionProbs[i] = actProb(actions[i]);
		std::cout << actionProbs[i] << " ";
	}std::cout << std::endl;
}


float computeVal(int i, int j, int k, std::array<std::array<float, ks>, ks>& kernel, std::array<std::array<float, ks>, ks> & kernel2)
{
	// std::cout << pi << std::endl;
	bool rep = true;
	// std::fill(std::begin(kernel), std::end(kernel), 0);
	// std::array<std::array<float, ks>, ks> kernel;
	// std::array<std::array<float, ks>, ks> kernel2;

	std::memset(std::begin(kernel), 0, ks * ks * sizeof(float));
	std::memset(std::begin(kernel2), 0, ks * ks * sizeof(float));
	// kernel = std::vector<std::vector<float>>(500, std::vector<float>(500, 0));
	// kernel2 = std::vector<std::vector<float>>(500, std::vector<float>(500, 0));
	// std::cout << kernel2.size() << std::endl;
	std::vector<float> angs;
	while (rep) {
		rep = false;
		angs.clear();
		for (int t1 = 0; t1 <= i; t1++) {
			angs.push_back(dis(gen) * 2.0 * pi);
		}
		for (int t1 = 0; t1 <= i; t1++) {
			for (int t2 = t1+1; t2 <= i; t2++) {
				if ((pi - std::abs(std::abs(angs[t1] - angs[t2]) - pi))
				    < 2.0f * pi / 5.0f) {
					rep = true;
				}
			}
		}
	}
	for (int t1 = 0; t1 <= i; t1++) {
		int y = int(std::sin(angs[t1]) * k*k + ks/2);
		int x = int(std::cos(angs[t1]) * k*k + ks/2);
		if (t1 == 0) {
			kernel[y][x] = 1.0;
		}
		else {
			if (kernel[y][x] != 0.0) {
				// kernel[y][x] += 0.3334;
				// std::cout << "peak same loc" << std::endl;
			}
			else {
				kernel[y][x] += 0.3334;
				// kernel[y][x] = 1.0;
				// std::cout << y << " " << x << std::endl;
			}
		}
	}
	// for (int t1 = 0; t1 < ks; t1++) {
	// 	for (int t2 = 0; t2 < ks; t2++) {
	// 		if (kernel[t1][t2] != 0.0f) {
	// 			std::cout << kernel[t1][t2] << " " << t1 << " " << t2 << " ";
	// 		}
	// 	}
	// 	// std::cout << std::endl;
	// }
	cv::GaussianBlur(cv::Mat(500, 500, CV_32FC1, &kernel[0][0]),
	                 cv::Mat(500, 500, CV_32FC1, &kernel2[0][0]), {(j+1)*4+1, (j+1)*4+1}, j+1, j+1);
	// std::cout << "kernel blurred:" << j << std::endl;
	// for (int t1 = 0; t1 < ks; t1++) {
	// 	for (int t2 = 0; t2 < ks; t2++) {
	// 		if (kernel2[t1][t2] != 0.0f) {
	// 			std::cout << kernel2[t1][t2] << " ";
	// 		}
	// 	}
	// 	// std::cout << std::endl;
	// }
	float ym = 0.0f;
	float xm = 0.0f;
	float sm = 0.0f;
	for (int y = 0; y < ks; y++) {
		for (int x = 0; x < ks; x++) {
			ym += float(y+1) * kernel2[y][x];
			xm += float(x+1) * kernel2[y][x];
			sm += kernel2[y][x];
		}
	}
	ym /= sm;
	ym -= 1;
	xm /= sm;
	xm -= 1;

	float dist = 0.0f;
	// std::cout << kernel2[int(ym)][int(xm)] << std::endl;
	for (int y = 0; y < ks; y++) {
		for (int x = 0; x < ks; x++) {
			dist += std::sqrt((std::pow(y - ym, 2) + std::pow(x - xm, 2)))
				* kernel2[y][x]/sm;
		}
	}
	// std::cout << "dist " << dist << std::endl;

	if (std::isnan(dist)) {
		std::cout << "NANNANANNANAN " << i << " " << j << " " << k << " "
		          << dist << " " << ym << " " << xm << " " << sm << std::endl;
	}
	return dist;
}

float computeVals()
{
	for (int i = 0; i < peakMax; i++) {
#pragma omp parallel for
		for (int j = 0; j < sigmaMax; j++) {
			std::array<std::array<float, ks>, ks> kernel;
			std::array<std::array<float, ks>, ks> kernel2;
			for (int k = 0; k < radiusMax; k++) {
				float v = 0.0f;
				int repN = 10;
				for (int rep = 0; rep < repN; rep++) {
					v += computeVal(i, j, k, kernel, kernel2);
				}
				v /= float(repN);
				stateQuality[i][j][k] = v;
			}
		}
	}

}


// float getReward(int i, int j, int k,
//                 int i2, int j2, int k2)
// {
// 	std::array<std::array<float, ks>, ks> kernel;
// 	std::array<std::array<float, ks>, ks> kernel2;
// 	float v1 = computeVal(i, j, k, kernel, kernel2);
// 	float v2 = computeVal(i2, j2, k2, kernel, kernel2);
// 	return v2 < v1 ? posReward : negReward;
// }

void setTransition(int i, int j, int k,
                   int i2, int j2, int k2,
                   int a, float score,
                   float probI, float probS, float probR)
{
	float prob = probI * probS * probR;
	// float rew = 0.0f;
	// rew = getReward(i, j, k, i2, j2, k2);
	float tmp = transitions[i][j][k][i2][j2][k2][a][0];
	transitions[i][j][k][i2][j2][k2][a][0] = tmp + prob;
}


void changeRadius(int i, int j, int k,
                  int i2, int j2,
                  int a, float score,
                  float probI, float probS)
{
	int k2;
	float probR;

	k2 = k;
	probR = score;
	setTransition(i, j, k, i2, j2, k2, a, score, probI, probS, probR);

	k2 = int(k*score);
	probR = score * (1.0 - score);
	setTransition(i, j, k, i2, j2, k2, a, score, probI, probS, probR);

	k2 = std::min(k+penaltyRadius, radiusMax-1);
	probR = (1.0 - score) * (1.0 - score);
	setTransition(i, j, k, i2, j2, k2, a, score, probI, probS, probR);
}

void changeSigma(int i, int j, int k,
                 int i2,
                 int a, float score,
                 float probI)
{
	int j2;
	float probS;

	j2 = j;
	// probS = 0.333334 * score;
	probS = score;
	changeRadius(i, j, k, i2, j2, a, score, probI, probS);

	j2 = int(j*score);
	probS = score * (1.0 - score);
	changeRadius(i, j, k, i2, j2, a, score, probI, probS);

	j2 = std::min(j+penaltySigma, sigmaMax-1);
	probS = (1.0 - score) * (1.0 - score);
	changeRadius(i, j, k, i2, j2, a, score, probI, probS);
}

void changePeak(int i, int j, int k,
                int a, float score)
{
	int i2;
	float probI;

	i2 = i;
	probI = score;
	changeSigma(i, j, k, i2, a, score, probI);

	i2 = std::max(0, i-1);
	probI = score * (1.0 - score);
	changeSigma(i, j, k, i2, a, score, probI);

	i2 = std::min(peakMax-1, i+1);
	probI = (1.0 - score) * (1.0 - score);
	changeSigma(i, j, k, i2, a, score, probI);
}


void precalcTransitions()
{
	// for (int i = 0; i < peakMax; i++) {
	// 	for (int j = 0; j < sigmaMax; j++) {
	// 		for (int k = 0; k < radiusMax; k++) {
	// 			for (int l = 0; l < peakMax; l++) {
	// 				for (int m = 0; m < sigmaMax; m++) {
	// 					for (int n = 0; n < radiusMax; n++) {
	// 						for (int a = 0; a < numActions; a++) {
	// 							transitions[i][j][k][l][m][n][a].push_back({0.0, 0.0});
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	// for (int i = 0; i < peakLimit; i++) {
	// 	for (int j = 0; j < sigmaLimit; j++) {
	// 		for (int k = 0; k < radiusLimit; k++) {
	// 			if (i == 0 && j == 0 && k == 0) {
	// 				continue;
	// 			}

	// 			for (int a = 0; a < numActions; a++) {
	// 				transitions[i][j][k][0][0][0][a].push_back({1.0, finReward});
	// 			}
	// 		}
	// 	}
	// }

	// auto o = transitions[0][0][0][0][0][0][0];
	for (int i = 0; i < peakMax; i++) {
#pragma omp parallel for
		for (int j = 0; j < sigmaMax; j++) {
			std::array<std::array<float, ks>, ks> kernel;
			std::array<std::array<float, ks>, ks> kernel2;

			std::cout << omp_get_num_threads() << " " << std::endl;

			for (int k = 0; k < radiusMax; k++) {
				std::cout << "set transitions reward for "
				          << i << " " << j << " " << k << " "
				          << std::endl;
				float v1;
				if (i == 0 && j == 0 && k == 0) {
					v1 = 0.0f;
				}
				else {
					v1 = 0.0f;
					// int repN = 5;
					// for (int rep = 0; rep < repN; rep++) {
					// 	v1 += computeVal(i, j, k, kernel, kernel2);
					// }
					// v1 /= float(repN);
					// v1 = computeVal(i, j, k, kernel, kernel2);
					v1 = stateQuality[i][j][k];
				}
				for (int l = 0; l < peakMax; l++) {
					for (int m = 0; m < sigmaMax; m++) {
						for (int n = 0; n < radiusMax; n++) {
							// if ((i < peakLimit || k < radiusLimit) &&
							//     j < sigmaLimit) {
							// 	if (i == 0 && j == 0 && k == 0) {
							// 		v1 = 0.0f;
							// 	}
							// 	for (int a = 0; a < numActions; a++) {
							// 		transitions[i][j][k][0][0][0][a][1] = finReward;
							// 		if (ld == 4) {
							// 			transitions[i][j][k][l][m][n][a][2] = v1;
							// 			transitions[i][j][k][l][m][n][a][3] = 0.0f;
							// 		}
							// 	}
							// 	continue;
							// }
							float v2;
							if (l == 0 && m == 0 && n == 0) {
								v2 = 0.0f;
							}
							else {
								v2 = 0.0f;
								// int repN = 5;
								// for (int rep = 0; rep < repN; rep++) {
								// 	v2 += computeVal(l, m, n, kernel, kernel2);
								// }
								// v2 /= float(repN);
								// v2 = computeVal(l, m, n, kernel, kernel2);
								v2 = stateQuality[l][m][n];

							}
							float rew = v2 < v1 ? posReward : negReward;
							if (i < l && j < m && k < n && v2 < v1) {
								std::cout << "wrong order1" << i << " "
								          << j << " " << k << " | "
								          << l << " " << m << " " << n
								          << std::endl;
							}
							if (i > l && j > m && k > n && v2 > v1) {
								std::cout << "wrong order2" << i << " "
								          << j << " " << k << " | "
								          << l << " " << m << " " << n
								          << std::endl;
							}
							// if ((i < peakLimit || k < radiusLimit) &&
							//     j < sigmaLimit) {
							// if (i == 0 && j == 0 && k == 0 &&
							//     l == 0 && m == 0 && n == 0) {
							// 	rew = 0.0;
							// 	rew = finReward;
							// }
							for (int a = 0; a < numActions; a++) {
								if (ld == 2) {
									transitions[i][j][k][l][m][n][a] =
										{0.0, rew};
								}
								else if (ld == 4) {
									transitions[i][j][k][l][m][n][a] =
										{0.0, rew, v1, v2};
								}
								else {
									std::cout << "invalid ld" << std::endl;
								}

							}
						}
					}
				}
			}
		}
	}
	for (int i = 0; i < peakMax; i++) {
		for (int j = 0; j < sigmaMax; j++) {
			for (int k = 0; k < radiusMax; k++) {
				if ((i < peakLimit || k < radiusLimit) &&
				    j < sigmaLimit) {
					// for (int a = 0; a < numActions; a++) {
					// 	transitions[i][j][k][0][0][0][a][0] = 1.0;
					// }
				// if (i < peakLimit &&
				    // j < sigmaLimit &&
				    // k < radiusLimit) {
					// continue;
				}
// #pragma omp parallel for
				for (int a = 0; a < numActions; a++) {
					float score = actionProbs[a];
					std::cout << "set transitions prob for " << i << " " << j
					          << " " << k << " " << a << std::endl;
					changePeak(i, j, k, a, score);
				}
			}
		}
	}
}


void writeTransitions(std::string fn)
{
	unsigned int shape[] = {peakMax, sigmaMax, radiusMax, peakMax, sigmaMax, radiusMax, numActions, 4};
	cnpy::npy_save(fn + std::string(".npy"), (float*)&transitions, shape, 8);
	std::ofstream file(fn, std::ofstream::out);
	for (int i = 0; i < peakMax; i++) {
		for (int j = 0; j < sigmaMax; j++) {
			for (int k = 0; k < radiusMax; k++) {
				for (int a = 0; a < numActions; a++) {
					for (int l = 0; l < peakMax; l++) {
						for (int m = 0; m < sigmaMax; m++) {
							for (int n = 0; n < radiusMax; n++) {
								file << transitions[i][j][k][l][m][n][a][0]
								     << " ";
								file << transitions[i][j][k][l][m][n][a][1]
								     << " ";
								if (ld > 2) {
									file << transitions[i][j][k][l][m][n][a][2]
									     << " ";
									file << transitions[i][j][k][l][m][n][a][3]
									     << " ";
								}
							}
						}
					}
				}
			}
		}
	}
	file.close();
}

void readTransitions(std::string fn, bool check)
{
	cnpy::NpyArray data = cnpy::npy_load(fn + std::string(".npy"));
	for(auto a : data.shape)
	{
		std::cout << a << std::endl;
	}
	float* trans = reinterpret_cast<float*>(data.data);
	bool correct = true;
	for (int i = 0; i < peakMax; i++) {
		int iidx = i * sigmaMax * radiusMax * peakMax * sigmaMax * radiusMax * numActions * ld;
		for (int j = 0; j < sigmaMax; j++) {
			int jidx = j * radiusMax * peakMax * sigmaMax * radiusMax * numActions * ld;
			for (int k = 0; k < radiusMax; k++) {
				int kidx = k * peakMax * sigmaMax * radiusMax * numActions * ld;
				for (int l = 0; l < peakMax; l++) {
					int lidx = l * sigmaMax * radiusMax * numActions * ld;
					for (int m = 0; m < sigmaMax; m++) {
						int midx = m * radiusMax * numActions * ld;
						for (int n = 0; n < radiusMax; n++) {
							int nidx = n * numActions * ld;
							for (int a = 0; a < numActions; a++) {
								int aidx = a * ld;
								int idx = iidx + jidx + kidx + lidx + midx + nidx + aidx;
								if (check && transitions[i][j][k][l][m][n][a][0] != trans[idx]) {
									std::cout << "wrong prob " << transitions[i][j][k][l][m][n][a][0] << " " << trans[idx] << std::endl;
									correct = false;
								}
								transitions[i][j][k][l][m][n][a][0] = trans[idx];
								if (check && transitions[i][j][k][l][m][n][a][1] != trans[idx+1]) {
									std::cout << "wrong rew " << transitions[i][j][k][l][m][n][a][1] << " " << trans[idx+1] << std::endl;
									correct = false;
								}
								transitions[i][j][k][l][m][n][a][1] = trans[idx+1];
								if (ld > 2) {
									transitions[i][j][k][l][m][n][a][2] = trans[idx+2];
									transitions[i][j][k][l][m][n][a][3] = trans[idx+3];
								}
							}
						}
					}
				}
			}
		}
	}
	if (!check) {
		std::cout << "transitions file not checked" << std::endl;
	}
	else if (!correct) {
		std::cout << "something wrong with transitions!" << std::endl;
	}
	else {
		std::cout << "transitions loaded correctly" << std::endl;
	}
	// std::ifstream file(fn, std::ifstream::in);
	// for (int i = 0; i < peakMax; i++) {
	// 	for (int j = 0; j < sigmaMax; j++) {
	// 		for (int k = 0; k < radiusMax; k++) {
	// 			for (int a = 0; a < numActions; a++) {
	// 				for (int l = 0; l < peakMax; l++) {
	// 					for (int m = 0; m < sigmaMax; m++) {
	// 						for (int n = 0; n < radiusMax; n++) {
	// 							float tmp1, tmp2;
	// 							file >> tmp1;
	// 							// if (transitions[i][j][k][l][m][n][a][0] != tmp1) {
	// 							// 	std::cout << "wrong prob" << transitions[i][j][k][l][m][n][a][0] << " " << tmp1 << std::endl;
	// 							// }
	// 							transitions[i][j][k][l][m][n][a][0] = tmp1;
	// 							file >> tmp2;
	// 							// if (transitions[i][j][k][l][m][n][a][1] != tmp2) {
	// 							// 	std::cout << "wrong reward" << transitions[i][j][k][l][m][n][a][1] << " " << tmp2 << std::endl;
	// 							// }
	// 							transitions[i][j][k][l][m][n][a][1] = tmp2;
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }
	// file.close();
}

void valueIteration()
{
	// std::array<float, numActions> tmpValues;
	// omp_set_num_threads(4);
	for (int h = 0; h < numIt; h++) {
		std::cout << "it: " << h << std::endl;
		for (int i = 0; i < peakMax; i++) {
			for (int j = 0; j < sigmaMax; j++) {
#pragma omp parallel for
				for (int k = 0; k < radiusMax; k++) {
					if (i == 0 && j == 0 && k == 0) {
						continue;
					}
					if ((i < peakLimit || k < radiusLimit) &&
					    j < sigmaLimit) {
					// if (i < peakLimit &&
					    // j < sigmaLimit &&
					    // k < radiusLimit) {
						continue;
					}
					// auto tid = omp_get_thread_num();
					// std::cout << "thread id " << tid
					//           << std::flush<< std::endl;
					std::array<float, numActions> tmpValues;
					for (int a = 0; a < numActions; a++) {
						float tmp = 0.0f;
						float p = 0.0f;
						for (int l = 0; l < peakMax; l++) {
							for (int m = 0; m < sigmaMax; m++) {
								for (int n = 0; n < radiusMax; n++) {
									auto const tr = transitions[i][j][k][l][m][n][a];
									if (h % 2 == 0) {
										tmp += tr[0] * (tr[1] + gammaV *
										             valuesB[l][m][n]);
									}
									else {
										tmp += tr[0] * (tr[1] + gammaV *
										             values[l][m][n]);
									}
									p += tr[0];
										// std::cout << tr.size() << " " << i << " " << j << " " << k << " " << l << " " << m << " " << n << " " << a << " | " << tr[0] << " " << tr[1] << std::endl;
									}
								}
							}
						tmpValues[a] = tmp;
						if (std::abs(p-1.0f) > 0.00001) {
							std::cout << "invalid prob"
							          << float(p) << std::endl;
						}
					}
					if (h % 2 == 0) {
						// std::cout << " " << values[i][j][k] << std::endl;
						values[i][j][k] = *std::max_element(
							std::begin(tmpValues),
							std::end(tmpValues));
						// std::cout << values[i][j][k] << std::endl;
					}
					else {
						// std::cout << " " << valuesB[i][j][k] << std::endl;
						valuesB[i][j][k] = *std::max_element(
							std::begin(tmpValues),
							std::end(tmpValues));
						// std::cout << valuesB[i][j][k] << std::endl;
					}
				}
			}
		}
		float diff = 0;
		for (int i = 0; i < peakMax; i++) {
			for (int j = 0; j < sigmaMax; j++) {
				for (int k = 0; k < radiusMax; k++) {
					diff += std::abs(values[i][j][k]-valuesB[i][j][k]);
					// std::cout << diff << std::endl;
				}
			}
		}
		std::cout << " diff " << diff << std::endl;
		if (diff < 0.0000001f) {
			break;
		}
	}
}

void printPolicy()
{
	std::array<float, numActions> tmpValues;
	std::cout << "Policy:" << std::endl;
	for (int i = 0; i < peakMax; i++) {
		for (int j = 0; j < sigmaMax; j++) {
			for (int k = 0; k < radiusMax; k++) {
				for (int a = 0; a < numActions; a++) {
					float tmp = 0.0f;
					float tmp2 = 0.0f;
					float tmp3 = 0.0f;
					for (int l = 0; l < peakMax; l++) {
						for (int m = 0; m < sigmaMax; m++) {
							for (int n = 0; n < radiusMax; n++) {
								auto const tr = transitions[i][j][k][l][m][n][a];
								tmp += tr[0] * (tr[1] + gammaV *
								                values[l][m][n]);
							}
						}
					}
					// std::cout << i << " " << j << " " << k << " " << a << " || " << tmp << " " << tmp2 << " " << tmp3 << " " << (tmp == tmp2) << std::endl;
					tmpValues[a] = tmp;
					qvalues[i][j][k][a] = tmp;
				}
				std::cout << "state ("
				          << i << ", "
				          << j << ", "
				          << k << "), qs: " << std::endl;
				int aID = std::distance(
					std::begin(tmpValues),
					std::max_element(std::begin(tmpValues),
					                 std::end(tmpValues)));
				maxA[i][j][k] = aID;
				for (int h = 0; h < numActions; h++) {
					if (h == aID) {
						std::cout << h << ": " << tmpValues[h]
						          << " <-- " << h << std::endl;
					}
					else {
						std::cout << h << ": " << tmpValues[h]
						          << std::endl;
					}
				}
			}
		}
	}
	float avgVal = 0.0f;
	for (int i = 0; i < peakMax; i++) {
		for (int j = 0; j < sigmaMax; j++) {
			for (int k = 0; k < radiusMax; k++) {
				for (int a = 0; a < numActions; a++) {
					if ((i < peakLimit || k < radiusLimit) &&
					    j < sigmaLimit) {
					// if (i < peakLimit &&
					    // j < sigmaLimit &&
					    // k < radiusLimit) {
						tmpValues[a] = 10.0f;
					}
					else
						tmpValues[a] = qvalues[i][j][k][a];
				}
				int aID = maxA[i][j][k];
				float maxV = *std::max_element(std::begin(tmpValues),
				                               std::end(tmpValues));
				avgVal += maxV;
				std::cout << "state ("
				          << i << ", "
				          << j << ", "
				          << k << "), action: "
				          << aID << " (return: "
				          << maxV << ")"
				          << std::endl;
			}
		}
	}
	std::cout << "average state value: " << avgVal/float(peakMax*sigmaMax*radiusMax) << std::endl;
}

void evaluatePolicy()
{
	int tries = 100000;
	int avgStepsTotal = 0;
	float avgRewardTotal = 0.0f;
	float avgDiscRewardTotal = 0.0f;

	// for (int rep = 0; rep < 5; rep++) {
	for (int is = 0; is < peakMax; is++) {
		for (int js = 0; js < sigmaMax; js++) {
			for (int ks = 0; ks < radiusMax; ks++) {

		int avgSteps = 0;
		float avgReward = 0.0f;
		float avgDiscReward = 0.0f;
		for (int tr = 0; tr < tries; tr++) {
			int step = 0;
			// int i = 1;
			// int j = 25;
			// int k = 6;
			int i = is;
			int j = js;
			int k = ks;
			float rew = 0.0f;
			float discRew = 0.0f;
			while (true) {
				// if (i == 0 and j == 0 and k == 0) {
				// 	std::cout << "final state!" << std::endl;
				// }
				if ((i < peakLimit || k < radiusLimit) &&
				    j < sigmaLimit) {
				// if (i < peakLimit &&
					// j < sigmaLimit &&
					// k < radiusLimit) {
					rew += finReward;
					discRew += std::pow(gammaV, step) * finReward;
					break;
				}
				int a = maxA[i][j][k];
				float rnd = dis(gen);
				float sm = 0.0;
				bool br = false;
				int i2, j2, k2;
				for (int l = 0; l < peakMax; l++) {
					for (int m = 0; m < sigmaMax; m++) {
						for (int n = 0; n < radiusMax; n++) {
							// std::cout << i << " " << j << " " << k
							//           << " " << l << " " << m << " " << n
							//           << " " << a << std::endl;
							sm += transitions[i][j][k][l][m][n][a][0];
							if (sm > rnd) {
								i2 = l;
								j2 = m;
								k2 = n;
								br = true;
								break;
							}
						}
						if (br) break;
					}
					if (br) break;
				}
				rew += transitions[i][j][k][i2][j2][k2][a][1];
				discRew += std::pow(gammaV, step) *
					transitions[i][j][k][i2][j2][k2][a][1];

				i = i2;
				j = j2;
				k = k2;
				step += 1;
			}
			avgSteps += step;
			avgReward += rew;
			avgDiscReward += discRew;
		}
		// std::cout << "average step count over " << tries << ": "
		//           << float(avgSteps) / float(tries) << std::endl;
		// std::cout << "average reward over " << tries << ": "
		//           << float(avgReward) / float(tries) << std::endl;
		// std::cout << "average discounted reward over " << tries << ": "
		//           << float(avgDiscReward) / float(tries) << std::endl;
		std::cout << "average step count over " << tries << " tries for ("
		          << is << ", " << js << "," << ks << "): "
		          << float(avgSteps) / float(tries) << std::endl;
		std::cout << "average reward over " << tries << " tries for ("
		          << is << ", " << js << "," << ks << "): "
		          << float(avgReward) / float(tries) << std::endl;
		std::cout << "average discounted reward over "<< tries << " tries for ("
		          << is << ", " << js << "," << ks << "): "
		          << float(avgDiscReward) / float(tries) << std::endl;

		avgStepsTotal += float(avgSteps) / float(tries);
		avgRewardTotal += float(avgReward) / float(tries);
		avgDiscRewardTotal += float(avgDiscReward) / float(tries);
			}}}
	int nstates = peakMax * sigmaMax * radiusMax;
	std::cout << "\n\ntotal average step count: "
	          << float(avgStepsTotal) / float(nstates) << std::endl;
	std::cout << "total average reward: "
	          << float(avgRewardTotal) / float(nstates) << std::endl;
	std::cout << "total average discounted reward: "
	          << float(avgDiscRewardTotal) / float(nstates) << std::endl;
	// }
}

int main(int argc, char *argv[])
{
	std::srand(std::time(0));
	if (argc < 3) {
		std::cout << "please provide args" << std::endl;
		exit(-1);
	}
	std::string fn = argv[1];
	std::cout << "writing transitions to " <<  fn << std::endl;
	initValues();
	setActions();
	computeVals();
	if (argv[2] == std::string("1")) {
		// std::cout << 1 << std::endl;
		// exit(-1);
		precalcTransitions();
		writeTransitions(fn);
		readTransitions(fn, true);
	}
	else if (argv[2] == std::string("2")){
		// std::cout << 2 << std::endl;
		// exit(-1);
		precalcTransitions();
		writeTransitions(fn);
	}
	else if (argv[2] == std::string("3")){
		// std::cout << 3 << std::endl;
		// exit(-1);
		readTransitions(fn, false);
	}
	valueIteration();
	printPolicy();
	evaluatePolicy();
	return 0;
}
