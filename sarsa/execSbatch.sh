#!/bin/bash

timestamp=$(date +%s)

mkdir -p tempFiles/${timestamp}
cp sarsa.py tempFiles/${timestamp}
cp sarsaQNN.py tempFiles/${timestamp}
cp qTab.py tempFiles/${timestamp}
cp run.sh tempFiles/${timestamp}
cp ../stmTip.py tempFiles/${timestamp}
cp replay_buffer.py tempFiles/${timestamp}
cp ../sumTree.py tempFiles/${timestamp}

echo $timestamp

cd tempFiles/${timestamp}
if [ "x" != "x${1}" ]
then
	./run.sh
else
	sbatch ./run.sh
fi
