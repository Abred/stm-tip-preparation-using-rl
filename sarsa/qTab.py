import numpy as np
import os
import shutil
import sys
import time
import threading

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim

import math

import tfutils


def printT(s):
    sys.stdout.write(s + '\n')


class QTab:
    def __init__(self, out_dir, params,
                 stateCnt=100, actionCnt=10):
        self.out_dir = out_dir
        self.params = params
        self.lock = threading.Lock()
        self.qVals = np.zeros((stateCnt, actionCnt))
        print(self.qVals.shape)
        # for s in range(stateCnt):
        #     for a in range(actionCnt):
        #         self.qVals[s, a] = -99.0

        self.stateCntr = np.zeros((stateCnt, actionCnt))
        self.stateCnt = stateCnt
        self.actionCnt = actionCnt
        self.step = 0
        self.intv = 1
        self.data = np.zeros((1, stateCnt, actionCnt))

    def run_predict(self, state):
        # printT(str(state))
        self.lock.acquire()
        tmp = self.qVals[state.flatten().astype(int)]
        self.lock.release()
        return tmp

    def run_predict_target(self, state):
        # print(state)
        # print(self.qVals[state.astype(int)])
        self.lock.acquire()
        tmp = self.qVals[state.flatten().astype(int)]
        self.lock.release()
        return tmp

    def run_train(self, states, actionIDs, targets):
        # print("train...")
        if (self.step % self.intv) == 0:
            printT("step {} invt {}".format(self.step, self.intv))
            self.lock.acquire()
            self.qVals.shape = (1,
                                self.qVals.shape[0],
                                self.qVals.shape[1])
            self.data = np.append(self.data, self.qVals, axis=0)
            self.qVals.shape = (self.qVals.shape[1],
                                self.qVals.shape[2])
            self.lock.release()
            if self.step < 10:
                self.intv = 1
            elif self.step < 100:
                self.intv = 10
            elif self.step < 1000:
                self.intv = 100
            elif self.step < 10000:
                self.intv = 1000
            else:
            # elif self.step < 100000:
                self.intv = 1000
        self.step += 1
        delta = np.zeros((self.qVals.shape[0], 1))
        out = np.zeros((self.qVals.shape[0], 1))
        mse = 0
        # print(states, actionIDs)
        for b in range(self.params['miniBatchSize']):
            state = int(states[b])
            action = int(actionIDs[b])
            delta[b] = targets[b] - self.qVals[state, action]
            mse += delta[b] * delta[b]
            # self.qVals[state, action] = \
            #     self.qVals[state, action] + \
            #     self.params['learning-rate'] * delta[b]
            alpha = 1.0 / (1 + math.pow(self.stateCntr[state, action], 0.75))
            self.stateCntr[state, action] += 1.0
            # printT("state {} action {} old: {} delta {} targets {}".format(state, action, self.qVals[state, action], delta[b], targets[b]))
            self.lock.acquire()
            self.qVals[state, action] = \
                self.qVals[state, action] + \
                alpha * delta[b]
            out = self.qVals[state, action]
            self.lock.release()
            # printT("state {} action {} new: {}".format(state, action, self.qVals[state, action]))
        loss = mse / float(self.params['miniBatchSize'])
        return self.step, out, delta, loss

    def run_update_target_nn(self):
        return

    def print_stateCnt(self):
        np.save(os.path.join(self.out_dir, "qvals.npy"), self.data)
        for i in range(self.stateCnt):
            for j in range(self.actionCnt):
                printT("state {}, action {}: sampled: {}".format(
                    i, j, self.stateCntr[i, j]))
