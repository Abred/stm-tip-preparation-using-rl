"""
Data structure for implementing experience replay
Author: Patrick Emami

License:
The MIT License (MIT)

Copyright (c) 2016 Patrick E.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import random
from collections import deque

import numpy as np
import pickle
import sys
import scipy.ndimage
import gc

class ReplayBuffer(object):

    def __init__(self, buffer_size, random_seed=None):
        """
        The right side of the deque contains the most recent experiences
        """
        self.buffer_size = buffer_size
        self.count = 0
        self.full = False
        self.buffer = deque()
        # self.buffer = [None] * self.buffer_size
        self.featCount = 3
        random.seed(random_seed)

    def add(self, s, feat, a, r, t, s2, feat2):
        experience = (s, feat, a, r, t, s2, feat2)
        # self.buffer[self.count] = experience
        # self.count += 1
        # if self.count == self.buffer_size:
        #     self.full = True
        #     self.count = 0

        if self.count < self.buffer_size:
            self.buffer.append(experience)
            self.count += 1
        else:
            # self.buffer.popleft()
            self.buffer.append(experience)

    def size(self):
        return self.count
        # if self.full == True:
        #     return self.buffer_size
        # else:
        #     return self.count

    def sample_batch(self, batch_size):
        batch = []

        if self.count < batch_size:
            batch = random.sample(self.buffer, self.count)
        else:
            batch = random.sample(self.buffer, batch_size)
        # if self.full:
        #     pop = random.sample(xrange(self.buffer_size), batch_size)
        # else:
        #     pop = random.sample(xrange(self.count), batch_size)
        # batch = [self.buffer[i] for i in pop]

        s_batch = np.array([_[0] for _ in batch])
        feat_batch = np.reshape(np.array([_[1] for _ in batch]),
                                (batch_size, self.featCount))
        a_batch = np.reshape(np.array([_[2] for _ in batch]), (batch_size, 1))
        r_batch = np.reshape(np.array([_[3] for _ in batch]), (batch_size, 1))
        t_batch = np.reshape(np.array([_[4] for _ in batch]), (batch_size, 1))
        s2_batch = np.array([_[5] for _ in batch])
        feat2_batch = np.reshape(np.array([_[6] for _ in batch]),
                                 (batch_size, self.featCount))
        if s2_batch.shape == (32,):
            print(s_batch.shape, feat_batch.shape, a_batch.shape,
                  t_batch.shape, feat2_batch.shape, s2_batch.shape)
            t = []
            for i in batch:
                t.append(i[5])

            try:
                s2_batch = np.array(t, dtype=np.float32)
            except Exception:
                print(len(t))
                for i in t:
                    print(i.shape, i.dtype)

            # sys.stdout.flush()
        return s_batch, a_batch, r_batch, t_batch, s2_batch, feat_batch, feat2_batch

    def clear(self):
        # self.buffer = [None] * self.buffer_size
        self.deque.clear()
        self.full = False
        self.count = 0

    def dump(self, fn):
        with open(fn, 'w') as f:
            gc.disable()
            pickle.dump(self.buffer, f)
            gc.enable()

    def load(self, fn):
        with open(fn, 'r') as f:
            self.buffer = pickle.load(f)
            self.count = len(self.buffer)
        print("buffer:", self.count)
        i = 0
        while self.buffer[i] is not None:
            i += 1
        self.count = i
        print("buffer:", self.count)
