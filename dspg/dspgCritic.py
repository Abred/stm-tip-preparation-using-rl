import numpy as np
import os
import shutil
import sys
import time

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim

import tfutils


class Critic:
    O = 1

    Hconv1 = 32
    Hconv2 = 64
    HFC = [256, 256]
    HFCVGG = [2048, 2048]

    tau = 0.001
    train_dir = 'data'
    state_dim = 50
    col_channels = 1
    actions_dim = 1

    def __init__(self, sess, out_dir, glStep, params):
        self.sess = sess
        self.summaries = []
        self.out_dir = out_dir
        if params['batchnorm']:
            # self.batchnorm = slim.batch_norm
            self.batchnorm = tfutils.batch_normBUG
        else:
            self.batchnorm = None

        self.dropout = params['dropout']
        self.weightDecay = params['weight-decayCritic']
        self.learning_rate = params['learning-rateCritic']
        self.momentum = params['momentumCritic']
        self.opti = params['optimizerCritic']
        self.bnDecay = params['batchnorm-decay']

        print("dropout critic", self.dropout)

        self.global_step = glStep

        with tf.variable_scope('Critic'):
            self.keep_prob = tf.placeholder(tf.float32)
            self.isTraining = tf.placeholder(tf.bool)

            # Critic Network
            self.setNN()

            self.loss_op = self.define_loss()
            self.train_op = self.defineTraining()

            self.action_grads = self.define_action_grad()
            self.grads = self.define_grads()

            self.summary_op = tf.summary.merge(self.summaries)
            self.writer = tf.summary.FileWriter(out_dir, sess.graph)

    def setNN(self):
        prevTrainVarCount = len(tf.trainable_variables())

        self.input_pl, self.actions_pl, self.nn = self.defineNN()
        self.nn_params = tf.trainable_variables()[prevTrainVarCount:]

        # Target Network
        with tf.variable_scope('target'):
            prevTrainVarCount = len(tf.trainable_variables())
            self.target_input_pl, self.target_actions_pl, self.target_nn =\
                self.defineNN()
            self.target_nn_params = \
                tf.trainable_variables()[prevTrainVarCount:]
            with tf.variable_scope('init'):
                for i in range(len(self.nn_params)):
                    tf.Variable.assign(
                        self.target_nn_params[i],
                        self.nn_params[i].initialized_value())
            self.target_nn_update_op = self.define_update_target_nn_op()

    def defineNN(self):
        with tf.variable_scope('inf'):
            images = tf.placeholder(
                tf.float32,
                shape=[None,
                       self.state_dim,
                       self.state_dim,
                       self.col_channels],
                name='input')
            actions = tf.placeholder(tf.float32,
                                     shape=[None, self.actions_dim],
                                     name='ActorActions')

            net = images
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True)):
                with slim.arg_scope([slim.conv2d], stride=1, padding='SAME'):
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv1,
                                      [3, 3], scope='conv1')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool1')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv2,
                                      [3, 3], scope='conv2')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool2')
                    print(net)

                remSz = int(self.state_dim / 2**2)
                net = tf.concat(
                    1, [tf.reshape(net, [-1, remSz*remSz*self.Hconv2],
                                   name='flatten'),
                        actions])

                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'decay': self.bnDecay,
                                        'scale': True}):
                    for i in range(len(self.HFC)):
                        net = slim.fully_connected(net, self.HFC[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.dropout,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)
                net = tf.sigmoid(net)
                # self.summaries += [tf.summary.histogram('output', net)]

        return images, actions, net

    def define_update_target_nn_op(self):
        with tf.variable_scope('update'):
            tau = tf.constant(self.tau, name='tau')
            invtau = tf.constant(1.0-self.tau, name='invtau')
            return \
                [self.target_nn_params[i].assign(
                    tf.mul(self.nn_params[i], tau) +
                    tf.mul(self.target_nn_params[i], invtau))
                 for i in range(len(self.target_nn_params))]

    def define_loss(self):
        with tf.variable_scope('loss2'):
            self.td_targets_pl = tf.placeholder(tf.float32, [None, 1],
                                                name='tdTargets')

            # lossL2 = tfu.mean_squared_diff(self.td_targets_pl, self.nn)
            lossL2 = slim.losses.mean_squared_error(self.td_targets_pl,
                                                    self.nn)
            lossL2 = tf.Print(lossL2, [lossL2], "lossL2 ", first_n=10)

            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss',
                                      lossL2)]
            regs = []
            for v in self.nn_params:
                if "w" in v.name:
                    regs.append(tf.nn.l2_loss(v))
            lossReg = tf.add_n(regs) * self.weightDecay
            lossReg = tf.Print(lossReg, [lossReg], "regLoss ", first_n=10)
            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss_reg',
                                      lossReg)]

            loss = lossL2 + lossReg
            with tf.name_scope(''):
                self.summaries += [
                    tf.summary.scalar('mean_squared_diff_loss_with_reg',
                                      loss)]

        return loss

    def defineTraining(self, conv=False):
        with tf.variable_scope('train'):
            var_list = None
            if self.opti == 'momentum':
                optimizer = tf.train.MomentumOptimizer(self.learning_rate,
                                                       self.momentum)
            elif self.opti == 'adam':
                optimizer = tf.train.AdamOptimizer(self.learning_rate)
            elif self.opti == 'sgd':
                optimizer = tf.train.GradientDescentOptimizer(
                    self.learning_rate)

            print(optimizer)
            return optimizer.minimize(self.loss_op, var_list=var_list,
                                      global_step=self.global_step)

    def define_action_grad(self):
        with tf.variable_scope('getActionGradient'):
            return tf.gradients(self.nn, self.actions_pl)

    def define_grads(self):
        with tf.variable_scope('getGrads'):
            optimizer = tf.train.AdamOptimizer(self.learning_rate, epsilon=0.1)
            return optimizer.compute_gradients(self.loss_op,
                                               var_list=self.nn_params)

    def run_train(self, inputs, actions, targets):
        step = self.sess.run(self.global_step)
        if (step+1) % 10 == 0:
            out, loss, _, summaries = self.sess.run([self.nn,
                                                     self.loss_op,
                                                     self.train_op,
                                                     self.summary_op],
                                                    feed_dict={
                self.input_pl: inputs,
                self.actions_pl: actions,
                self.td_targets_pl: targets,
                self.isTraining: True,
                self.keep_prob: self.dropout
            })
            self.writer.add_summary(summaries, step)
            self.writer.flush()
        else:
            out, loss, _ = self.sess.run(
                [self.nn, self.loss_op, self.train_op],
                feed_dict={
                    self.input_pl: inputs,
                    self.actions_pl: actions,
                    self.td_targets_pl: targets,
                    self.isTraining: True,
                    self.keep_prob: self.dropout
                })
        # print("loss: {}".format(loss))
        return step, out, loss


    def run_predict(self, inputs, action):
        return self.sess.run(self.nn, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: action,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_predict_target(self, inputs, action):
        return self.sess.run(self.target_nn, feed_dict={
            self.target_input_pl: inputs,
            self.target_actions_pl: action,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_get_action_gradients2(self, inputs, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.isTraining: True,
            self.keep_prob: 1.0
        })

    def run_get_action_gradients(self, inputs, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_get_gradients(self, inputs, actions, targets):
        return zip(self.nn_params, self.sess.run(self.grads, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.td_targets_pl: targets,
            self.isTraining: False,
            self.keep_prob: 1.0
        }))

    def run_get_loss(self, inputs, actions, targets):
        return self.sess.run(self.loss_op, feed_dict={
            self.input_pl: inputs,
            self.actions_pl: actions,
            self.td_targets_pl: targets,
            self.isTraining: False,
            self.keep_prob: 1.0
        })
    def run_update_target_nn(self):
        self.sess.run(self.target_nn_update_op)
