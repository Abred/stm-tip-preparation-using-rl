import parseNNArgs

import pickle
import shutil
import glob
import os
import random
import sys
import time
import json

import numpy as np
import scipy.ndimage

from dspgActor import Actor
from replay_buffer import ReplayBuffer
from dspgCritic import Critic
from stmTip import STMTip
from convNetEval import ConvNet
import tensorflow as tf
from tensorflow.python.framework import ops


class DSPG():
    def __init__(self, sess, params):
        self.params = params
        self.sess = sess

        timestamp = str(int(time.time()))
        self.out_dir = os.path.abspath(os.path.join(
            "/scratch/s7550245/simulateSTM/dspg/runs",
            params['version'], timestamp))
        if params['resume']:
            print("resuming... {}".format(params['resume']))
            self.out_dir = params['resume']

        print("Summaries will be written to: {}\n".format(self.out_dir))
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)
            shutil.copy2(sys.argv[0], os.path.join(self.out_dir, sys.argv[0]))

        if not params['resume']:
            print("new start... {}".format(self.out_dir))
            config = json.dumps(params)
            with open(os.path.join(self.out_dir, "config"), 'w') as f:
                f.write(config)


        self.rewardNet = ConvNet(self.sess, params)
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES)
        for v in variables:
            if "global_step" in v.name:
                self.global_step = v
        self.actor = Actor(self.sess, self.out_dir, self.params)
        self.critic = Critic(self.sess, self.out_dir,
                             self.global_step, self.params)

        self.simulator = STMTip()

        self.imgs = glob.glob("/home/s7550245/simulateSTM/images2/*.npy")

        self.sess.run(tf.assign(self.global_step, 0))
        self.saver = tf.train.Saver()
        if params['resume']:
            self.saver.restore(sess, tf.train.latest_checkpoint(self.out_dir))
            replay.load(os.path.join(self.out_dir, "replayBuffer.pickle"))
            print("Model restored.")


    def run(self):
        episode_reward = tf.Variable(0., name="episodeReward")
        sum1 = tf.summary.scalar("Reward", episode_reward)
        episode_ave_max_q = tf.Variable(0., name='epsideAvgMaxQ')
        sum2 = tf.summary.scalar("Qmax_Value", episode_ave_max_q)
        summary_vars = [episode_reward, episode_ave_max_q]
        summary_ops = tf.summary.merge([sum1, sum2])
        writer = tf.summary.FileWriter(self.out_dir+"/train", self.sess.graph)
        self.sess.run(tf.initialize_all_variables())

        maxEpisodes = 1000
        replayBufferSize = 100
        miniBatchSize = 16
        gamma = 0.9
        replay = ReplayBuffer(replayBufferSize)
        print("using linear Buffer")
        epsilon = 0.1

        for e in range(maxEpisodes):
            rawState = self.reset()
            state = self.simulator.getRes(rawState)
            sigma = self.simulator.sigma
            terminal = False
            ep_reward = 0
            ep_ave_max_q = 0

            step = 0
            while not terminal:
                stateScaled = scipy.ndimage.zoom(state, (1, 0.1, 0.1, 1),
                                                 order=1)
                step += 1
                if np.random.rand() < epsilon:
                    action = np.array([[np.random.rand()]])
                    print("\nNext action (e-greedy): {}".format(action))
                else:
                    action = self.getActions(stateScaled)
                    print("\nNext action: {}".format(action))

                terminal, newState, newSigma = \
                    self.simulator.act(rawState, action[0][0])

                if step == 10:
                    terminal = True

                newStateScaled = scipy.ndimage.zoom(newState, (1, 0.1, 0.1, 1),
                                                    order=1)
                if False:
                    res1, res2 = self.getReward(stateScaled, newStateScaled)
                    reward = res1 - res2
                    print("reward: {} - {} = {}".format(res1, res2, reward))
                else:
                    reward = sigma - newSigma
                    print("reward: {}".format(reward))
                ep_reward += reward

                replay.add(stateScaled, action, reward, terminal,
                           newStateScaled)

                if replay.size() > miniBatchSize:
                    s_batch, a_batch, r_batch, t_batch, ns_batch = \
                        replay.sample_batch(miniBatchSize)

                    qValsNewState = self.predict_target_nn(ns_batch)
                    y_batch = np.zeros((miniBatchSize, 1))
                    for i in range(miniBatchSize):
                        if t_batch[i]:
                            y_batch[i] = r_batch[i]
                        else:
                            y_batch[i] = r_batch[i] + \
                                gamma * qValsNewState[i]

                    qs = self.update(s_batch, a_batch, y_batch)
                    ep_ave_max_q += np.amax(qs)

                    self.update_targets()

                state = newState
                sigma = newSigma
                sys.stdout.flush()

            summary_str = self.sess.run(summary_ops, feed_dict={
                summary_vars[0]: ep_reward,
                summary_vars[1]: ep_ave_max_q / float(step)
            })
            writer.add_summary(summary_str, e)
            writer.flush()
            print('| Reward: {}, | Episode {}, | Qmax: {}\n\n'.
                  format(ep_reward, e,
                         ep_ave_max_q / float(step)))
            if ((e+1) % 50) == 0:
                save_path = self.saver.save(self.sess,
                                            self.out_dir + "/model.ckpt",
                                            global_step=self.global_step)
                replay.dump(os.path.join(self.out_dir, "replayBuffer.pickle"))
                print("Model saved in file: {}".format(save_path))
            sys.stdout.flush()

    def getActions(self, state):
        a = self.actor.run_predict(state)
        return a

    def getReward(self, state, newState):
        r = self.rewardNet.runPrediction(state, newState)
        return r

    def update(self, states, actions, targets):
        step, out, loss = self.critic.run_train(states, actions, targets)
        print("step: {}, loss: {}".format(step, loss))
        ac = self.actor.run_predict(states)
        a_grad = self.critic.run_get_action_gradients(states, ac)
        self.actor.run_train(states, a_grad[0], step)
        return out

    def update_targets(self):
        self.critic.run_update_target_nn()
        self.actor.run_update_target_nn()

    def predict_target_nn(self, state):
        a = self.actor.run_predict_target(state)
        return self.critic.run_predict_target(state, a)

    def predict_nn(self, state):
        a = self.actor.run_predict(state)
        return self.critic.run_predict(state, a)

    def reset(self):
        c = random.randint(0, len(self.imgs)-1)
        img = np.load(self.imgs[c])
        print("\nLoading image {}".format(self.imgs[c]))
        img.shape = (1, img.shape[0], img.shape[1], 1)
        self.simulator.reset()
        return img


params = parseNNArgs.parse(sys.argv[1:])

with tf.Session() as sess:
    rl = DSPG(sess, params)
    rl.run()
