import numpy as np
import os
import shutil
import sys
import time

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim

import math

import tfutils


class Actor:
    O = 1

    Hconv1 = 32
    Hconv2 = 64
    HFC = [256, 256]
    HFCVGG = [2048, 2048]

    tau = 0.001
    train_dir = 'data'
    state_dim = 50
    col_channels = 1
    actions_dim = 1

    def __init__(self, sess, out_dir, params):
        self.sess = sess
        # self.summaries = []
        if params['batchnorm']:
            # self.batchnorm = slim.batch_norm
            self.batchnorm = tfutils.batch_normBUG
        else:
            self.batchnorm = None

        self.dropout = params['dropout']
        self.weightDecay = params['weight-decayActor']
        self.learning_rate = params['learning-rateActor']
        self.momentum = params['momentumActor']
        self.opti = params['optimizerActor']
        self.bnDecay = params['batchnorm-decay']
        self.discrete = params['dspgDiscrete']
        self.numActions = params['numActions']

        print("dropout actor", self.dropout)

        with tf.variable_scope('Actor'):
            self.keep_prob = tf.placeholder(tf.float32)
            self.isTraining = tf.placeholder(tf.bool)

            # Actor Network
            self.setNN()

            self.train_op = self.defineTraining()

            # self.summary_op = tf.summary.merge(self.summaries)
            # self.writer = tf.summary.FileWriter(out_dir, sess.graph)

    def setNN(self):
        prevTrainVarCount = len(tf.trainable_variables())

        self.input_pl, self.nn = self.defineNN()
        self.nn_params = tf.trainable_variables()[prevTrainVarCount:]

        # Target Network
        with tf.variable_scope('target'):
            prevTrainVarCount = len(tf.trainable_variables())
            self.target_input_pl, self.target_nn = \
                self.defineNN()
            self.target_nn_params = \
                tf.trainable_variables()[prevTrainVarCount:]
            with tf.variable_scope('init'):
                for i in range(len(self.nn_params)):
                    tf.Variable.assign(
                        self.target_nn_params[i],
                        self.nn_params[i].initialized_value())
            self.target_nn_update_op = self.define_update_target_nn_op()

    def defineNN(self):
        with tf.variable_scope('inf'):
            images = tf.placeholder(
                tf.float32,
                shape=[None,
                       self.state_dim,
                       self.state_dim,
                       self.col_channels],
                name='input')

            net = images
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True)):
                with slim.arg_scope([slim.conv2d], stride=1, padding='SAME'):
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv1,
                                      [3, 3], scope='conv1')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool1')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv2,
                                      [3, 3], scope='conv2')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool2')
                    print(net)

                remSz = int(self.state_dim / 2**2)
                net = tf.reshape(net, [-1, remSz*remSz*self.Hconv2],
                                 name='flatten')

                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'decay': self.bnDecay,
                                        'scale': True}):
                    for i in range(len(self.HFC)):
                        net = slim.fully_connected(net, self.HFC[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.dropout,
                                               is_training=self.isTraining)
                            print(net)
                if self.discrete:
                    net = slim.fully_connected(net, self.numActions,
                                               activation_fn=None,
                                               scope='out')
                else:
                    net = slim.fully_connected(net, 2 * self.actions_dim,
                                               activation_fn=None,
                                               scope='out')
                print(net)
                # net = tf.sigmoid(net)

                # self.summaries += [tf.summary.histogram('output', net)]

        return images, net

    def define_update_target_nn_op(self):
        with tf.variable_scope('update'):
            tau = tf.constant(self.tau, name='tau')
            invtau = tf.constant(1.0-self.tau, name='invtau')
            return \
                [self.target_nn_params[i].assign(
                    tf.mul(self.nn_params[i], tau) +
                    tf.mul(self.target_nn_params[i], invtau))
                 for i in range(len(self.target_nn_params))]

    def defineTraining(self):
        with tf.variable_scope('train'):
            self.critic_actions_gradient_pl = tf.placeholder(
                tf.float32,
                [None, self.actions_dim],
                name='CriticActionsGradient')

            var_list = self.nn_params

            self.actor_gradients = tf.gradients(
                self.nn,
                self.nn_params,
                # critic grad descent
                # here ascent -> negative
                -self.critic_actions_gradient_pl)

        if self.opti == 'momentum':
            optimizer = tf.train.MomentumOptimizer(self.learning_rate,
                                                   self.momentum)
        elif self.opti == 'adam':
            optimizer = tf.train.AdamOptimizer(self.learning_rate)
        elif self.opti == 'sgd':
            optimizer = tf.train.GradientDescentOptimizer(
                self.learning_rate)

        return optimizer.apply_gradients(zip(self.actor_gradients, var_list))

    def run_train(self, inputs, a_grad, step):
        # if (step+1) % 1 == 0:
        #     _, summaries = self.sess.run([self.train_op,
        #                                   self.summary_op],
        #                                  feed_dict={
        #         self.input_pl: inputs,
        #         self.critic_actions_gradient_pl: a_grad,
        #         self.isTraining: True,
        #         self.keep_prob: self.dropout
        #     })
        #     self.writer.add_summary(summaries, step)
        #     self.writer.flush()
        # else:
            self.sess.run([self.train_op],
                          feed_dict={
                self.input_pl: inputs,
                self.critic_actions_gradient_pl: a_grad,
                self.isTraining: True,
                self.keep_prob: self.dropout
            })

    def run_predict(self, inputs):
        return self.sess.run(self.nn, feed_dict={
            self.input_pl: inputs,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_predict_target(self, inputs):
        return self.sess.run(self.target_nn, feed_dict={
            self.target_input_pl: inputs,
            self.isTraining: False,
            self.keep_prob: 1.0
        })

    def run_update_target_nn(self):
        self.sess.run(self.target_nn_update_op)
