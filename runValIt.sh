#!/bin/bash

#SBATCH --job-name=valIt
##SBATCH -A p_cvldpose
#SBATCH -n 1
##SBATCH -N 1
#SBATCH --time 0-06:00:00
#SBATCH --mem 20G
#SBATCH --mail-type=END,FAIL,TIME_LIMIT_90
#SBATCH --mail-user=s7550245@msx.tu-dresden.de
#SBATCH -o /scratch/s7550245/simulateSTM/valItNet/log.%j
#SBATCH -c 12
###SBATCH -c 6
###SBATCH --gres=gpu:1
###SBATCH --partition=gpu2


# module load eb
# module load tensorflow

source /etc/profile.d/10_modules.sh
module load OpenCV
# ./valIt7noSig transitions11 1 > result11_test1.txt
${1} transitions${2} 1 > result${2}_test1.txt
