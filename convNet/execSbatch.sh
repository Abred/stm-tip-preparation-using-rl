#!/bin/bash

timestamp=$(date +%s)

mkdir -p tempFiles/${timestamp}
cp convNet.py tempFiles/${timestamp}
cp convNetEval.py tempFiles/${timestamp}
cp run.sh tempFiles/${timestamp}

echo $timestamp

cd tempFiles/${timestamp}
if [ "x1" == "x${1}" ]
then
	./run.sh
else
	sbatch $1 ./run.sh
fi
