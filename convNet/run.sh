#!/bin/bash

#SBATCH --job-name=convNet
##SBATCH -A p_cvldpose
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --time 0-06:00:00
#SBATCH --mem 30G
#SBATCH --mail-type=END,FAIL,TIME_LIMIT_90
#SBATCH --mail-user=s7550245@msx.tu-dresden.de
#SBATCH -o /scratch/s7550245/simulateSTM/convNet/log.%j
###SBATCH -c 12
#SBATCH -c 6
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu2


# module load eb
# module load tensorflow


# PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/.local/lib/python2.7/site-packages /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu/ld-2.17.so --library-path /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/bin/python \
PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/1.0.1-Python-2.7.12/lib/python2.7/site-packages/: /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu/ld-2.17.so --library-path /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.12-intel-2016.03-GCC-5.3/bin/python \
		  convNet.py \
		  -i "/home/s7550245/simulateSTM/images3RL2" \
		  --version 4 \
		  --cropSize 50 \
		  --dropout 0.5 \
		  --learning-rate 0.0001 \
		  --miniBatchSize 32 \
		  `#--vgg `\
		  `#--xFirstVggLayers 13 `\
		  `#--stopGrad 8 `\
		  --optimizer sgd \
		  --momentum 0.9 \
		  --numEpochs 2000 \
		  --weight-decay 0.0001 \
		  --penaltySigma 3 \
		  --penaltyRadius 2 \
		  --peakMax 3 \
		  --peakLimit 1 \
		  --sigmaMax 35 \
		  --sigmaLimit 4 \
		  --radiusMax 11 \
		  --radiusLimit 2 \
		  --rewardPos -1 \
		  --rewardNeg -2 \
		  --rewardFinal 10 \
		  `#--transitions "/home/s7550245/simulateSTM/transitions11.npy" `\
		  `#--blob /scratch/s7550245/simulateSTM/convNet/runs/4/8559131_1498835444_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2 `\
		  --transitions "/home/s7550245/simulateSTM/transitions10.npy" \
		  --blob /scratch/s7550245/simulateSTM/convNet/runs/4/8559130_1498835396_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2 \
		  `#--transitions "/home/s7550245/simulateSTM/transitions9.npy" `\
		  `#--blob /scratch/s7550245/simulateSTM/convNet/runs/4/8559127_1498835391_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2 `\
		  `#--classNN /scratch/s7550245/simulateSTM/classConvNet/runs/3/7836376_1496268934_1000_16_noSubImg_noVGG_dropout0.5_nosameL_distorted_Br-0.0_Cntr-0.0_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentumtmpClassImgs `\
		  `#-r /scratch/s7550245/simulateSTM/convNet/runs/3/7864269_1496330383_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0001_lr0.001_mom0.9_optmomentum_images3RL2`
		  `#-r /scratch/s7550245/simulateSTM/convNet/runs/2/7299988_1494605039_2000_16_noSubImg_noVGG_dropout0.5_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.9_optmomentumimages3RL2b `\
		  # --distorted \
		  # --distortBrightnessRelative 0.35 \
		  # --distortContrast 0.2

# --vgg \
# --xFirstVggLayers 7 \
# --stopGrad 7 \
# --distortBrightnessRelative 1.0 \
# --distortContrast 0.1 \
