from keras.applications.vgg16 import preprocess_input
import png
import json
import numpy as np
import os
import shutil
import sys
import time
import random

import parseNNArgs
import loadData
import readFakeImgPairs

from tensorflow.python.framework import ops
import tensorflow as tf
import scipy.ndimage


class ConvNetEval:

    def __init__(self, sess, params, glStep=None):
        try:
            if params['rankNN'] is not None:
                if os.path.isdir(params['rankNN']):
                    in_dir = params['rankNN']
                    model = None
                else:
                    in_dir = os.path.dirname(params['rankNN'])
                    model = params['rankNN']
            else:
                in_dir = params['in_dir']
                model = params['model']
            print("config file: {}".format(os.path.join(in_dir, "config")))
            with open(os.path.join(in_dir, "config"), 'r') as f:
                config = f.read()
                params = json.loads(config)
                params['in_dir'] = in_dir
                params['model'] = model
        except:
            print("config file not found")
        self.sess = sess
        self.useVGG = params['useVGG']
        self.keep_prob = tf.placeholder(tf.float32, shape=(),
                                        name="keep_prob_pl")
        self.isTraining = tf.placeholder(tf.bool, shape=(),
                                         name="isTraining_pl")

        if self.useVGG:
            self.state_dim = 224
        elif params['state_dim'] is None:
            self.state_dim = params['cropSz']
        else:
            self.state_dim = params['state_dim']

        self.img1_pl = tf.placeholder(
            tf.float32, shape=[None, 50, 50, 1], name='input')
        self.img2_pl = tf.placeholder(
            tf.float32, shape=[None, 50, 50, 1], name='input')
        if self.useVGG:
            scope="VGG"
            tmpImg = tf.image.resize_images(self.img1_pl, (224, 224))
            tmpImg = tf.image.grayscale_to_rgb(tmpImg)
            self.saver = tf.train.import_meta_graph(
                tf.train.latest_checkpoint(in_dir) + ".meta",
                import_scope=scope,
                clear_devices=True,
                input_map={'VGG_1/deflatten': tmpImg})
            self.nnp1 = sess.graph.get_tensor_by_name(
                scope+"/VGG_2/top/out/BiasAdd:0")
        else:
            scope="convNet"
            self.saver = tf.train.import_meta_graph(
                tf.train.latest_checkpoint(in_dir) + ".meta",
                import_scope=scope,
                clear_devices=True,
                input_map={'convNet/inf/deflatteni1': self.img1_pl,
                           'convNet/inf_1/deflatteni1': self.img2_pl})
            self.nnp1 = sess.graph.get_tensor_by_name(
                    scope+"/convNet/inf/out/BiasAdd:0")
            self.nnp2 = sess.graph.get_tensor_by_name(
                    scope+"/convNet/inf_1/out/BiasAdd:0")
        try:
            self.keep_prob = sess.graph.get_tensor_by_name(
                scope+"/keep_prob_pl:0")
        except:
            pass
        try:
            self.isTraining = sess.graph.get_tensor_by_name(
                scope+"/isTraining_pl:0")
        except:
            self.isTraining = sess.graph.get_tensor_by_name(
                scope+"/Placeholder_9:0")

    def runPrediction(self, input1, input2):
        if self.useVGG:
            r1 = self.sess.run([self.nnp1], feed_dict={
                self.img1_pl: input1,
                self.isTraining: False,
                self.keep_prob: 1.0
            })
            r2 = self.sess.run([self.nnp1], feed_dict={
                self.img1_pl: input2,
                self.isTraining: False,
                self.keep_prob: 1.0
            })
            return r1, r2
        else:
            return self.sess.run([self.nnp1, self.nnp2], feed_dict={
                self.img1_pl: input1,
                self.img2_pl: input2,
                self.isTraining: False,
                self.keep_prob: 1.0
            })

if __name__ == "__main__":
    params = parseNNArgs.parse(sys.argv[1:])

    if os.path.isfile(os.path.join(params['in_dir'], "config")):
        with open(os.path.join(params['in_dir'], "config"), 'r') as f:
            tmp = params['in_dir']
            tmp2 = params['validationData']
            tmp3 = params['loadBlob']
            model = params['model']
            config = f.read()
            params =  json.loads(config)
            params['in_dir'] = tmp
            params['validationData'] = tmp2
            params['loadBlob'] = tmp3
            params['model'] = model

    timestamp = str(int(time.time()))
    jobid = os.environ['SLURM_JOBID']
    out_dir = os.path.abspath(os.path.join(
        "/scratch/s7550245/simulateSTM/convNet/runsEval",
        params['version'], jobid + "_" + timestamp))

    print("usevgg", params['useVGG'])
    if params['useVGG']:
        out_dir += "_" + "VGG" + str(params['top'])
        if params['stopGrad']:
            out_dir += "-stopGrad" + str(params['stopGrad'])
    else:
        out_dir += "_" + "noVGG"

    print("dropout", params['dropout'])
    if params['dropout']:
        out_dir += "_" + "dropout" + str(params['dropout'])
    else:
        out_dir += "_" + "noDropout"

    print("batchnorm", params['batchnorm'])
    if params['batchnorm']:
        out_dir += "_" + "batchnorm-" + str(params['batchnorm-decay'])
    else:
        out_dir += "_" + "noBatchnorm"

    print("cropSz", params['cropSz'])
    if params['cropSz']:
        out_dir += "_" + "cropSz" + str(params['cropSz'])

    print("reading data from: ", params['in_dir'])
    out_dir += params['in_dir'].split("/")[-1].split("_")[0]
    # out_dir += params['in_dir'].split("/")[-1]

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.copy2(sys.argv[0], os.path.join(out_dir, sys.argv[0]))

    config = json.dumps(params)
    with open(os.path.join(out_dir, "config"), 'w') as f:
        f.write(config)

    global_step = tf.Variable(1, name='global_stepRank',
                              trainable=False)

    sess = tf.Session()
    nn = ConvNetEval(sess, params, glStep=global_step)

    # nn.saver.restore(sess,
    #                  tf.train.latest_checkpoint(params['in_dir']))
    if params['model'] is not None and not os.path.isdir(params['model']):
        print("restoring ... {}".format(params['model']))
        nn.saver.restore(
            sess,
            params['model'])
    elif params['model'] is not None:
        print("restoring ... {}".format(tf.train.latest_checkpoint(params['model'])))
        nn.saver.restore(
            sess,
            tf.train.latest_checkpoint(params['model']))
    else:
        print("restoring ... {}".format(tf.train.latest_checkpoint(params['in_dir'])))
        nn.saver.restore(
            sess,
            tf.train.latest_checkpoint(params['in_dir']))

    if params['loadBlob'] is not None:
        img1_train, img2_train, l1_train, l2_train, f1_train, f2_train, \
        img1_test, img2_test, l1_test, l2_test, f1_test, f2_test = \
            loadData.restorePairs(params['loadBlob'])
        imgs1 = np.concatenate((img1_train, img1_test))
        imgs2 = np.concatenate((img1_train, img2_test))
        l1 = np.concatenate((l1_train, l1_test))
        l2 = np.concatenate((l2_train, l2_test))
        f1 = np.concatenate((f1_train, f1_test))
        f2 = np.concatenate((f2_train, f2_test))
    else:
        imgs1, imgs2, l1, l2, f1, f2 = readFakeImgPairs.buildValPairs(params)


    if params['useVGG']:
        imgs1 = scipy.ndimage.zoom(imgs1,
                                   (1,
                                    224.0/imgs1.shape[1],
                                    224.0/imgs1.shape[2],
                                    1),
                                   order=1)
        imgs2 = scipy.ndimage.zoom(imgs2,
                                   (1,
                                    224.0/imgs2.shape[1],
                                    224.0/imgs2.shape[2],
                                    1),
                                   order=1)
    print("using vgg: {}".format(params['useVGG']))
    channels = 1
    if params['useVGG']:
        channels = 3
        imgs1 = preprocess_input(np.repeat(imgs1, 3, axis=3))
        imgs2 = preprocess_input(np.repeat(imgs2, 3, axis=3))

    random.seed(params['seed'])
    seed = random.randint(0, 2**31)
    print("random_state: {}".format(seed))

    # res1, res2 = nn.runPrediction(imgs1, imgs2)
    transitions = np.load(params['transitions'])
    cor = 0
    total = len(imgs1)
    print(imgs1.shape, imgs2.shape, l1.shape, l2.shape, f1.shape, f2.shape)
    # for i in range(total):
    for idx in range(int(total/128)+1):
        bidx1 = idx * 128
        bidx2 = (idx+1) * 128
        res1, res2 = nn.runPrediction(imgs1[bidx1:bidx2,...],
                                      imgs2[bidx1:bidx2,...])
        j = 0
        for i in range(bidx1, bidx2):
            if i >= total:
                break
            print(res1[j], res2[j])
            if (l1[i] < l2[j] and res1[j] < res2[j]) or \
               (l1[i] > l2[j] and res1[j] > res2[j]):

            # if res1[j] > res2[j]:
                cor += 1
            else:
                print(res1[j] > res2[j], res1[j], res2[j], f1[i], f2[i])
                print(transitions[f1[i][0], f1[i][1], f1[i][2],
                                  f2[i][0], f2[i][1], f2[i][2], 0],
                      transitions[f2[i][0], f2[i][1], f2[i][2],
                                  f1[i][0], f1[i][1], f1[i][2], 0])
            j += 1
    print("Accuracy: {} (correct: {}, total: {})".format(
        float(cor)/float(total), cor, total))
