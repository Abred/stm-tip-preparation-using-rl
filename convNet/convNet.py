from keras.applications.vgg16 import preprocess_input
import json
import numpy as np
import os
import shutil
import sys
import time

import preload
import defineVGG
import parseNNArgs
import loadData
import rankNN
import runner

from tensorflow.python.framework import ops
import tensorflow as tf
import tensorflow.contrib.slim as slim


class ConvNet(rankNN.RankNN):
    Hconv1 = 32
    Hconv2 = 48
    Hconv3 = 48
    HFC = [2048, 2048]
    HFCVGG = [2048, 2048]

    learning_rate = 0.01
    momentum = 0.9

    def __init__(self, sess, out_dir, glStep,
                 trainImages1, trainImages2,
                 trainLabels1, trainLabels2,
                 testImages1, testImages2,
                 testLabels1, testLabels2,
                 params, doPreload=True):
        super(ConvNet, self).__init__(sess, glStep, params)
        self.top = params['top']
        self.bnDecay = params['batchnorm-decay']

        if self.useVGG:
            self.state_dim = 224
        elif params['state_dim'] is None:
            self.state_dim = params['cropSz']
        else:
            self.state_dim = params['state_dim']

        if self.useVGG:
            varTmp = tf.get_variable("tmp", shape=[1,1])
            # with tf.variable_scope("VGG") as scope:
                # model = VGG16(weights='imagenet', include_top=False)
            self.vggsaver = tf.train.import_meta_graph(
                    '/home/s7550245/convNet/vgg-model.meta',
                    import_scope="VGG",
                    clear_devices=True)

            _VARSTORE_KEY = ("__variable_store",)
            varstore = ops.get_collection(_VARSTORE_KEY)[0]
            variables = tf.get_collection(
                ops.GraphKeys.GLOBAL_VARIABLES,
                scope="VGG")

            for v in variables:
                varstore._vars[v.name.split(":")[0]] = v

            with tf.variable_scope("VGG") as scope:
                scope.reuse_variables()
                self.nnp1Train = self.defineNNVGG(trainImages1)
                self.nnp2Train = self.defineNNVGG(trainImages2)
                self.nnp1Test = self.defineNNVGG(testImages1)
                self.nnp2Test = self.defineNNVGG(testImages2)
                self.nnp1Train = tf.stop_gradient(self.nnp1Train)
                self.nnp2Train = tf.stop_gradient(self.nnp2Train)

            if self.top is not None:
                with tf.variable_scope("VGG") as scope:
                    self.nnp1Train = self.defineNNVGGTop(self.nnp1Train)
                    self.nn_params = tf.trainable_variables()
                    scope.reuse_variables()
                    self.nnp2Train = self.defineNNVGGTop(self.nnp2Train)
                    self.nnp1Test = self.defineNNVGGTop(self.nnp1Test)
                    self.nnp2Test = self.defineNNVGGTop(self.nnp2Test)
            self.l1Train = trainLabels1
            self.l2Train = trainLabels2
            self.defineLoss(trainLabels1, trainLabels2)
            self.defineTraining()
            self.l1Test = testLabels1
            self.l2Test = testLabels2

        else:
            if doPreload:
                # if params['classNN'] is not None:
                #     sn = "classConvNet"
                # else:
                sn = "convNet"
                with tf.variable_scope(sn) as scope:
                    # scope.reuse_variables()
                    self.nnp1Train = self.defineNN(trainImages1)
                    self.nn_params = tf.trainable_variables()
                    scope.reuse_variables()
                    self.nnp2Train = self.defineNN(trainImages2)
                    self.nnp1Test = self.defineNN(testImages1)
                    self.nnp2Test = self.defineNN(testImages2)

                self.l1Train = trainLabels1
                self.l2Train = trainLabels2
                self.defineLoss(trainLabels1, trainLabels2)
                self.defineTraining()
                self.l1Test = testLabels1
                self.l2Test = testLabels2

        _VARSTORE_KEY = ("__variable_store",)
        varstore = ops.get_collection(_VARSTORE_KEY)[0]
        variables = tf.get_collection(
            ops.GraphKeys.GLOBAL_VARIABLES,
            scope="inf")

        for v in variables:
            print(v.name)
            if v.name.endswith("weights:0") or \
               v.name.endswith("biases:0"):
                s = []
                var = v
                mean = tf.reduce_mean(var)
                s.append(tf.summary.scalar(v.name+'mean', mean))
                with tf.name_scope('stddev'):
                    stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
                s.append(tf.summary.scalar(v.name+'stddev', stddev))
                s.append(tf.summary.scalar(v.name+'max', tf.reduce_max(var)))
                s.append(tf.summary.scalar(v.name+'min', tf.reduce_min(var)))
                s.append(tf.summary.histogram(v.name+'histogram', var))

                self.weight_summaries += s

        print(set(self.nn_params).symmetric_difference(set(tf.trainable_variables())))
        self.summary_op = tf.summary.merge(self.summaries)
        self.weight_summary_op = tf.summary.merge(self.weight_summaries)

        if params['classNN'] is not None:
            vd = {}
            _VARSTORE_KEY = ("__variable_store",)
            varstore = ops.get_collection(_VARSTORE_KEY)[0]
            variables = tf.get_collection(
                ops.GraphKeys.GLOBAL_VARIABLES,
                scope="convNet")
            for v in variables:
                print(v.name)
                vn = v.name.split(":")[0]
                vn = vn.replace("convNet", "classConvNet")
                print(vn)
                if "fc" not in vn:
                    vd[vn] = v
            self.saver = tf.train.Saver(vd)

    def defineNNVGG(self, images):
        return defineVGG.defineNNVGG(images, self.state_dim, self.top)

    def defineNNVGGTop(self, bottom):
        with tf.variable_scope('top') as scope:
            print(bottom)
            if self.top == 1:
                numPool = 0
                self.HFCVGG = [256, 256]
            if self.top <= 2:
                numPool = 1
                H = 64
                self.HFCVGG = [256, 256]
            elif self.top <= 4:
                numPool = 2
                H = 128
                self.HFCVGG = [512, 512]
            elif self.top <= 7:
                numPool = 3
                H = 256
                self.HFCVGG = [1024, 1024]
            elif self.top <= 10:
                numPool = 4
                H = 512
                self.HFCVGG = [2048, 2048]
            elif self.top <= 13:
                numPool = 5
                H = 512
                self.HFCVGG = [2048, 2048]
            # remSz = int(self.state_dim / 2**numPool)
            net = slim.flatten(bottom)
            # net = tf.reshape(bottom, [-1, remSz*remSz*H],
            #                  name='flatten')

            with slim.arg_scope(
                [slim.fully_connected],
                activation_fn=tf.nn.relu,
                weights_initializer=
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=1.0/255.0, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True)):
                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'scale': True}):
                    for i in range(len(self.HFCVGG)):
                        net = slim.fully_connected(net, self.HFCVGG[i],
                                                   scope='fc' + str(i))
                        net = tf.Print(net, [net], "fc" + str(i), summarize=1000,
                                       first_n=10)

                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.keep_prob,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)
                net = tf.Print(net, [net], "output:", summarize=1000,
                               first_n=10)
                print(net)
            self.weight_summaries += [tf.summary.histogram('output', net)]
            return net

    def defineNN(self, images):
        with tf.variable_scope('inf') as scope:
            net = tf.reshape(images, [-1,
                                      self.state_dim,
                                      self.state_dim,
                                      1], name='deflatteni1')
            fact = 1.0
            # if not params['classNN']:
            net = (net - 127.0) / 255.0
            # fact = 1.0/255.0
            with slim.arg_scope(
                [slim.fully_connected, slim.conv2d],
                activation_fn=tf.nn.relu,
                weights_initializer=\
                    tf.contrib.layers.variance_scaling_initializer(
                        factor=fact, mode='FAN_AVG', uniform=True),
                weights_regularizer=slim.l2_regularizer(self.weightDecay),
                biases_initializer=tf.contrib.layers.xavier_initializer(
                    uniform=True)):
                with slim.arg_scope([slim.conv2d], stride=1, padding='SAME'):
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv1,
                                      [3, 3], scope='conv1')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool1')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv2,
                                      [3, 3], scope='conv2')
                    print(net)
                    net = slim.max_pool2d(net, [2, 2], scope='pool2')
                    print(net)
                    net = slim.repeat(net, 3, slim.conv2d, self.Hconv3,
                                      [3, 3], scope='conv3')
                    print(net)

                # remSz = int(self.state_dim / 2**2)
                net = slim.flatten(net)
                # net = tf.reshape(net, [-1, remSz*remSz*self.Hconv2],
                #                  name='flatten')

                with slim.arg_scope([slim.fully_connected],
                                    normalizer_fn=self.batchnorm,
                                    normalizer_params={
                                        'fused': True,
                                        'is_training': self.isTraining,
                                        'updates_collections': None,
                                        'decay': self.bnDecay,
                                        'scale': True}):
                    for i in range(len(self.HFC)):
                        net = slim.fully_connected(net, self.HFC[i],
                                                   scope='fc' + str(i))
                        print(net)
                        if self.dropout:
                            net = slim.dropout(net,
                                               keep_prob=self.keep_prob,
                                               is_training=self.isTraining)
                            print(net)
                net = slim.fully_connected(net, 1, activation_fn=None,
                                           scope='out')
                print(net)
                net = tf.Print(net, [net], "out ", first_n=100, summarize=20)
                print(net)

                self.weight_summaries += [tf.summary.histogram('output', net)]

        return net


if __name__ == "__main__":
    params = parseNNArgs.parse(sys.argv[1:])

    if os.environ['SLURM_JOB_NAME'] == 'zsh':
        params['version'] = 'tmp'
    print(params, params['version'])
    timestamp = str(int(time.time()))
    jobid = os.environ['SLURM_JOBID']
    out_dir = os.path.abspath(os.path.join(
        "/scratch/s7550245/simulateSTM/convNet/runs",
        params['version'], jobid + "_" + timestamp))

    print("Number of epochs: ", params['numEpochs'])
    out_dir += "_" + str(params['numEpochs'])

    print("miniBatchSize: ", params['miniBatchSize'])
    out_dir += "_" + str(params['miniBatchSize'])

    print("use subimage: ", params['subImages'])
    if params['subImages']:
        out_dir += "_" + "subImg"
    else:
        out_dir += "_" + "noSubImg"

    print("usevgg", params['useVGG'])
    if params['useVGG']:
        out_dir += "_" + "VGG" + str(params['top'])
        if params['stopGrad']:
            out_dir += "-stopGrad" + str(params['stopGrad'])
    else:
        out_dir += "_" + "noVGG"

    print("dropout", params['dropout'])
    if params['dropout']:
        out_dir += "_" + "dropout" + str(params['dropout'])
    else:
        out_dir += "_" + "noDropout"

    print("sameL", params['sameL'])
    if params['sameL']:
        out_dir += "_" + "sameL"
    else:
        out_dir += "_" + "nosameL"

    if params['distortBrightnessRelative'] or params['distortContrast']:
        params['distorted'] = True
    print("distorted", params['distorted'])
    if params['distorted']:
        delta = params['distortBrightnessRelative']
        factor = params['distortContrast']
        out_dir += "_" + "distorted" + "_Br-" + str(delta) + "_Cntr-" + str(factor)
    else:
        out_dir += "_" + "noDistorted"

    print("batchnorm", params['batchnorm'])
    if params['batchnorm']:
        out_dir += "_" + "batchnorm-" + str(params['batchnorm-decay'])
    else:
        out_dir += "_" + "noBatchnorm"

    print("cropSz", params['cropSz'])
    if params['cropSz']:
        out_dir += "_" + "cropSz" + str(params['cropSz'])

    print("data normalised by: ", params['suffix'])
    out_dir += "_suffix-" + params['suffix']

    print("use whitened images", params['white'])
    if params['white']:
        out_dir += "_" + "white"
    else:
        out_dir += "_" + "noWhite"

    print("weight decay", params['weight-decay'])
    out_dir += "_wd" + str(params['weight-decay'])

    print("learning rate", params['learning-rate'])
    out_dir += "_lr" + str(params['learning-rate'])

    print("momentum", params['momentum'])
    out_dir += "_mom" + str(params['momentum'])

    print("optimizer", params['optimizer'])
    out_dir += "_opt" + params['optimizer']

    print("reading data from: ", params['in_dir'])
    out_dir += "_" + params['in_dir'].split("/")[-1]

    if params['resume']:
        print("resuming... {}".format(params['resume']))
        out_dir = params['resume']

    print("Summaries will be written to: {}\n".format(out_dir))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.copy2(sys.argv[0], os.path.join(out_dir, sys.argv[0]))
    shutil.copy2("convNetEval.py",
                 os.path.join(out_dir, "convNetEval.py"))

    if not params['resume']:
        print("new start... {}".format(out_dir))
        config = json.dumps(params)
        with open(os.path.join(out_dir, "config"), 'w') as f:
            f.write(config)

    params['out_dir'] = out_dir
    if os.environ['SLURM_JOB_NAME'] != 'zsh':
        sys.stdout.flush()
        sys.stdout = open(os.path.join(out_dir, "log"), 'w')

    # with tf.device("/gpu:0"):
    global_step = tf.Variable(1, name='global_stepRank',
                              trainable=False)

    sess = tf.Session()
    # sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    if params['loadBlob'] is not None:
        img1_train, img2_train, l1_train, l2_train, f1_train, f2_train, \
        img1_test, img2_test, l1_test, l2_test, f1_test, f2_test = \
            loadData.restorePairs(params['loadBlob'])
    elif params['resume']:
        img1_train, img2_train, l1_train, l2_train, \
        img1_test, img2_test, l1_test, l2_test = loadData.restorePairs(out_dir)
    else:
        img1_train, img2_train, l1_train, l2_train, f1_train, f2_train, \
        img1_test, img2_test, l1_test, l2_test, f1_test, f2_test = \
            loadData.loadPairs(out_dir, params, rawImg=True, fake=True)


    print("using vgg: {}".format(params['useVGG']))
    channels = 1
    if params['useVGG']:
        channels = 3
        img1_train = preprocess_input(np.repeat(img1_train, 3, axis=3))
        img2_train = preprocess_input(np.repeat(img2_train, 3, axis=3))
        img1_test = preprocess_input(np.repeat(img1_test, 3, axis=3))
        img2_test = preprocess_input(np.repeat(img2_test, 3, axis=3))

    print(img1_train.shape, img2_train.shape, l1_train.shape, l2_train.shape)
    print(img1_test.shape, img2_test.shape, l1_test.shape, l2_test.shape)
    print(img1_train.dtype, img1_test.dtype)
    print(img1_train.min(), img1_train.max())
    print(img2_train.min(), img2_train.max())
    print(img1_test.min(), img1_test.max())
    print(img2_test.min(), img2_test.max())


    pl = preload.PreloaderPairwise(img1_train, img2_train,
                                   l1_train, l2_train,
                                   img1_test, img2_test, l1_test, l2_test)
    images1_train, images2_train, labels1_train, labels2_train, \
        images1_test, images2_test, labels1_test, labels2_test = \
        pl.setupPreload(params, rawImg=True)

    rn = ConvNet(sess, out_dir, global_step,
                 images1_train, images2_train,
                 labels1_train, labels2_train,
                 images1_test, images2_test,
                 labels1_test, labels2_test,
                 params)

    pl.initPreload(sess)

    trainImgCnt = img1_train.shape[0]
    testImgCnt = img1_test.shape[0]

    op = runner.Runner(sess, global_step, params, conv=True)

    init_op = tf.group(tf.global_variables_initializer(),
                       tf.local_variables_initializer())
    sess.run(init_op)

    op.initPairwise(out_dir, rn, trainImgCnt, testImgCnt)

    rn.setSess(sess, out_dir)
    op.runPairwiseQueue()
