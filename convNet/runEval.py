#!/bin/bash

#SBATCH --job-name=eval-convNet
#SBATCH -A p_cvldpose
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --time 0-1:00:00
#SBATCH --mem 30G
#SBATCH --mail-type=END,FAIL,TIME_LIMIT_90
#SBATCH --mail-user=s7550245@msx.tu-dresden.de
#SBATCH -o /scratch/s7550245/convNet/log.%j
###SBATCH -c 24
#SBATCH -c 6
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu2


# module load eb
# module load tensorflow

# PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/.local/lib/python2.7/site-packages /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu/ld-2.17.so --library-path /sw/taurus/eb/tensorflow/0.8.0/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/bin/python \
PYTHONPATH=/home/s7550245/pyutil:/home/s7550245/1.0.1-Python-2.7.12/lib/python2.7/site-packages/: /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu/ld-2.17.so --library-path /home/s7550245/1.0.1-Python-2.7.12/lib/x86_64-linux-gnu:/sw/taurus/eb/cuDNN/5.1/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/eb/Python/2.7.11-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/GMP/6.1.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tk/8.6.4-intel-2016.03-GCC-5.3-no-X11/lib:/sw/taurus/eb/SQLite/3.9.2-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/Tcl/8.6.4-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/libreadline/6.3-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/ncurses/6.0-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/zlib/1.2.8-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/bzip2/1.0.6-intel-2016.03-GCC-5.3/lib:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/mkl/lib/intel64:/sw/taurus/eb/imkl/11.3.3.210-iimpi-2016.03-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/impi/5.1.3.181-iccifort-2016.3.210-GCC-5.3.0-2.26/lib64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/mpi/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/ifort/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/compilers_and_libraries_2016.3.210/linux/compiler/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib/intel64:/sw/taurus/eb/icc/2016.3.210-GCC-5.3.0-2.26/lib:/sw/taurus/eb/binutils/2.26-GCCcore-5.3.0/lib:/sw/taurus/eb/GCCcore/5.3.0/lib/gcc/x86_64-unknown-linux-gnu/5.3.0:/sw/taurus/eb/GCCcore/5.3.0/lib64:/sw/taurus/eb/GCCcore/5.3.0/lib:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/lib64:/sw/taurus/libraries/cuda/8.0.44/extras/CUPTI/lib64 /sw/taurus/eb/Python/2.7.12-intel-2016.03-GCC-5.3/bin/python \
		  convNetEval.py \
		  --validationData /home/s7550245/simulateSTM/images3RL2Val \
		  -i $1 \
		  --model $2 \
		  `#--vgg `\
		  `#--xFirstVggLayers 7 `\
		  `#-i /scratch/s7550245/simulateSTM/convNet/runs/3/7688440_1495628759_2000_16_noSubImg_VGG7_noDropout_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.9_optmomentum_images3RL2 `\
		  --transitions "/home/s7550245/simulateSTM/transitions8.npy" \
		  `#--blob /scratch/s7550245/simulateSTM/convNet/runs/3/7688440_1495628759_2000_16_noSubImg_VGG7_noDropout_nosameL_noDistorted_noBatchnorm_cropSz50_suffix-Norm_noWhite_wd0.0002_lr0.0001_mom0.9_optmomentum_images3RL2 `\
