import shutil
import random
import scipy.ndimage
import scipy.ndimage.filters as spf
from scipy.special import expit
import scipy.signal as sps
import glob
import tensorflow as tf
import parseNNArgs
import sys
import os
import numpy as np

params = parseNNArgs.parse(sys.argv[1:])

out_dir = os.path.join("/scratch/s7550245/simulateSTM/valItNet",
                       os.environ['SLURM_JOBID'])
os.makedirs(out_dir)

if params['rankNN']:
    sys.path.insert(1, params['rankNN'])
print(sys.path)
from convNetEval import ConvNet

if params['classNN']:
    sys.path.insert(1, params['classNN'])
print(sys.path)
from classConvNetEval import ClassConvNet

sess = tf.Session()
rewardRankNet = ConvNet(sess, params)
rewardRankNet.saver.restore(
    sess,
    tf.train.latest_checkpoint(params['rankNN']))
rewardClassNet = ClassConvNet(sess, params)
rewardClassNet.saver.restore(
    sess,
    tf.train.latest_checkpoint(params['classNN']))

peakMax = params['peakMax']
peakLimit = params['peakLimit']
sigmaMax = params['sigmaMax']
sigmaLimit = params['sigmaLimit']
radiusMax = params['radiusMax']
radiusLimit = params['radiusLimit']

finReward = params['rewardFinal']
posReward = params['rewardPos']
negReward = params['rewardNeg']

gamma = params['gamma']

ks = 500

numActions = params['numActions']
numIt = 1000

def initStateValues():
    for i in range(peakMax):
        for j in range(sigmaMax):
            for k in range(radiusMax):
                values[i, j, k] = np.random.rand()*20.0-10.0
                if (i < peakLimit or k < radiusLimit) and j < sigmaLimit:
                    values[i, j, k] = finReward
                    # print(values[i, j, k])
                valuesB[i, j, k] = values[i, j, k]


def transformState(state, peaks, sigma, radius):
    # print(state.shape)
    if state.shape[0] == 1:
        state.shape = (state.shape[1], state.shape[2])
    if len(state.shape) > 2:
        state.shape = (state.shape[0], state.shape[1])
    if not params['zoom']:
        kernel = np.zeros((ks/10, ks/10))
    else:
        kernel = np.zeros((ks, ks))
    rep = True
    while rep:
        rep = False
        angs = []
        for i in range(peaks+1):
            angs.append(np.random.rand() * 2 * np.pi)
        for i in range(peaks+1):
            for j in range(i+1, peaks+1):
                if (np.pi - abs(abs(angs[i] - angs[j]) - np.pi)) < 2 * np.pi / 5.0:
                    rep = True
        if rep:
            continue
    for j in range(len(angs)):
        r = pow(radius, 2)
        if not params['zoom']:
            r = r/10.0

        y = int(np.sin(angs[j]) * r + kernel.shape[0]/2)
        x = int(np.cos(angs[j]) * r + kernel.shape[0]/2)
        # print(x, y)
        if j == 0:
            kernel[y, x] = 1.0
        else:
            if kernel[y, x] != 0.0:
                print("peak same loc, {}".format((peaks, sigma, radius)))
            else:
                # kernel[y, x] += 0.334/(len(angs)-1)
                kernel[y, x] += 0.334
                # kernel[y, x] = 1.0
    if not params['zoom']:
        kernel = spf.gaussian_filter(kernel, (sigma+1)/10.0)
    else:
        kernel = spf.gaussian_filter(kernel, (sigma+1))
        # filteredState = spf.convolve(state, kernel, mode='constant')
    filteredState = sps.fftconvolve(state, kernel, mode='same')
    filteredState = filteredState/filteredState.max()*255.0
    state.shape = (1, state.shape[0], state.shape[1], 1)
    filteredState.shape = (1,
                           filteredState.shape[0],
                           filteredState.shape[1],
                           1)
    filteredState = scipy.ndimage.zoom(filteredState,
                                         (1, 0.1, 0.1, 1),
                                         order=1)
    return filteredState


def createState(p, s, r):
    img = np.load(imgs[random.randrange(0, len(imgs))])
    # print(img.min(), img.max())
    return transformState(img, p, s, r)


def createStates():
    for i in range(peakMax):
        for j in range(sigmaMax):
            for k in range(radiusMax):
                print("creating state: {}".format((i, j, k)))
                states[i, j, k] = createState(i, j, k)


def getRewardRankRes(state, nextState):
    res1, res2 = rewardRankNet.runPrediction(state, nextState)
    if res2 > res1:
        return posReward
    else:
        return negReward


def getRewardClassRes(state):
    res = rewardClassNet.runPrediction(state)
    # print(res)
    if res < 0.5:
        return False
    else:
        return True


def valueIteration():
    tmpValues = np.zeros((numActions, 1), dtype=np.float32)

    for h in range(numIt):
        for i in range(peakMax):
            for j in range(sigmaMax):
                for k in range(radiusMax):
                    print(i, j, k)
                    sys.stdout.flush()
                    if not useNet:
                        if (i < peakLimit or k < radiusLimit) and \
                           j < sigmaLimit:
                            continue
                    else:
                        # state = createState(i, j, k)
                        state = states[i, j, k]
                        r = False
                        if (i < peakLimit or k < radiusLimit) and \
                           j < sigmaLimit:
                            rt = True
                        else:
                            rt = False
                        if getRewardClassRes(state):
                            r = finReward
                            if h % 2 == 0:
                                values[i, j, k] = r
                            else:
                                valuesB[i, j, k] = r
                        # print(i, j, k, r, rt)
                        if r == finReward:
                            continue
                    for a in range(numActions):
                        tmp = 0.0
                        p = 0.0
                        for l in range(peakMax):
                            for m in range(sigmaMax):
                                for n in range(radiusMax):
                                    t = transitions[i, j, k, l, m, n, a]
                                    pr = t[0]
                                    if pr == 0.0:
                                        continue
                                    r = t[1]
                                    rt = r
                                    if useNet:
                                        # # state = createState(i, j, k)
                                        # nextState = createState(l, m, n)
                                        nextState = states[l, m, n]
                                        r = getRewardRankRes(state, nextState)
                                    if h % 2 == 0:
                                        tmp2 = pr * (r + gamma *
                                                     valuesB[l, m, n])
                                    else:
                                        tmp2 = pr * (r + gamma *
                                                     values[l, m, n])
                                    # print(i, j, k, l, m, n, a, tmp2, r, rt)
                                    tmp += tmp2
                                    p += t[0]
                        tmpValues[a] = tmp
                        if abs(p-1.0) > 0.0001:
                            print("invalid prob {}".format(p))
                        # print(max(tmpValues))
                    if h % 2 == 0:
                        values[i, j, k] = max(tmpValues)
                    else:
                        valuesB[i, j, k] = max(tmpValues)
            np.save(os.path.join(out_dir, "values" + str(h) + ".npy"),
                    values)
            shutil.copy2(os.path.join(out_dir, "values" + str(h) + ".npy"),
                         os.path.join(out_dir, "values.npy"))
            np.save(os.path.join(out_dir, "valuesB" + str(h) + ".npy"),
                    valuesB)
            shutil.copy2(os.path.join(out_dir, "valuesB" + str(h) + ".npy"),
                         os.path.join(out_dir, "valuesB.npy"))
        diff = np.sum(np.abs(values-valuesB))
        print("It: {}, diff: {}".format(h, diff))
        if diff < 0.1:
            break


def printPolicy():
    tmpValues = np.zeros((numActions, 1), dtype=np.float32)
    print("Policy:")
    for i in range(peakMax):
        for j in range(sigmaMax):
            for k in range(radiusMax):
                print(i, j, k)
                sys.stdout.flush()
                if useNet:
                    state = states[i, j, k]
                    r = False
                    if getRewardClassRes(state):
                        r = finReward
                    # print(i, j, k, r, rt)
                    if r == finReward:
                        print("state {}, qs:".format((i, j, k)))
                        print("{}: {} <-- {}".format(0, values[i, j, k], 0))
                        for a in range(1, numActions):
                            print("{}: {}".format(0, values[i, j, k]))
                            qvalues[i, j, k, a] = values[i, j, k]
                        continue
                for a in range(numActions):
                    tmp = 0.0
                    for l in range(peakMax):
                        for m in range(sigmaMax):
                            for n in range(radiusMax):
                                t = transitions[i, j, k, l, m, n, a]
                                pr = t[0]
                                if pr == 0.0:
                                    continue
                                r = t[1]
                                rt = r
                                if useNet:
                                    # # state = createState(i, j, k)
                                    # nextState = createState(l, m, n)
                                    nextState = states[l, m, n]
                                    r = getRewardRankRes(state, nextState)
                                tmp += pr * (r + gamma * values[l, m, n])
                    tmpValues[a] = tmp
                    qvalues[i, j, k, a] = tmp
                print("state {}, qs:".format((i, j, k)))
                for a in range(len(tmpValues)):
                    aID = np.argmax(tmpValues)
                    if a == aID:
                        print("{}: {} <-- {}".format(a, tmpValues[a], a))
                    else:
                        print("{}: {}".format(a, tmpValues[a]))

    avgVal = 0.0
    for i in range(peakMax):
        for j in range(sigmaMax):
            for k in range(radiusMax):
                aID = np.argmax(qvalues[i, j, k])
                # maxA[i, j, k] = aID
                avgVal += np.max(qvalues[i, j, k])
                print("state: {}, "
                      "action: {} "
                      "(return: {:06.3f},".format(
                          (i, j, k),
                          aID,
                          np.max(qvalues[i, j, k])))
    print("average state value: {}".format(
        avgVal/float(peakMax*sigmaMax*radiusMax)))


if __name__ == "__main__":
    useNet = True
    if params['resume']:
        values = np.load(os.path.join(params['resume'], "values.npy"))
        valuesB = np.load(os.path.join(params['resume'], "valuesB.npy"))
        states = np.load(os.path.join(params['resume'], "states.npy"))
    else:
        values = np.zeros((peakMax, sigmaMax, radiusMax), dtype=np.float32)
        valuesB = np.zeros((peakMax, sigmaMax, radiusMax), dtype=np.float32)
        initStateValues()

        imgs = glob.glob(os.path.join(params['images'], "*.npy"))
        states = np.zeros((peakMax, sigmaMax, radiusMax, 1, 50, 50, 1),
                          dtype=np.float32)
        createStates()
        np.save(os.path.join(out_dir, "states.npy"), states)

    qvalues = np.zeros((peakMax, sigmaMax, radiusMax, numActions),
                       dtype=np.float32)

    transitions = np.load(params['transitions'])
    valueIteration()

    printPolicy()
